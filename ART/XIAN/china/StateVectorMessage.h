#ifndef STATE_VECTOR_MESSAGE_H
#define STATE_VECTOR_MESSAGE_H 

#include "serialization.h"
#include "stddef.h"

// TODO: не оч оптимально
#ifndef STATE_VECTOR_SIZE
#define STATE_VECTOR_SIZE   16
#endif

// упакованная структура
#ifdef QT_SIDE
    #pragma pack(1)
#endif
struct StateVectorMessage {
    uint8_t yaw;
    std::uint16_t ultrasonicTimes[STATE_VECTOR_SIZE];
}
#ifdef QT_SIDE
;
    #pragma pack()
#else
    __attribute__( (packed, aligned(1)) ) 
;
#endif


std::uint8_t * serialize_StateVectorMessage(std::uint8_t *buffer, const StateVectorMessage &message);
std::uint8_t * deserialize_StateVectorMessage(std::uint8_t *buffer, StateVectorMessage &message);

std::uint8_t packStateVectorMessage(std::uint8_t *buffer, const StateVectorMessage &message);
std::uint8_t * unpackStateVectorMessage(std::uint8_t *buffer, std::uint16_t bufferSize, std::uint16_t &bytesRead, StateVectorMessage &message);

#endif