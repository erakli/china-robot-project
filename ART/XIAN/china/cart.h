#ifndef CART_H
#define CART_H

#define  size___ 22

#define	SIZE__		16	//������ �������������� ������� ���������
#define EPCOUNT__	1	//���������� ����������� ����
#define TTLSIZE__	SIZE__*EPCOUNT__	//�������� ������ ��
#define	OUTSIZE__	1	//������ ������� �������

#include <QObject>
#include <matrixunit.h>
#include <qvector.h>
#include <qfile.h>
#include <qstring.h>
#include <qstringlist.h>
#include "qfiledialog.h"

struct	std_	//������
{
	Vector	inp_;	//������� ������ ����� (��������)
	Vector	out_;	//����� (�������)
	int		c_;		//���-�� ������ ��������, �� ������� ������������ �������

	std_() {inp_.clear(); out_.clear(); c_=0;}	//������ �������

	void addVect(Vector _inp) {		//������� ������
		for (int i=0;i<_inp.size();i++) {
			inp_[i]=(inp_[i]*c_+_inp[i])/(c_+1);
		}
		c_++;
	}
};

//���� ART (����������� ���������)
class cArt : public QObject
{
	Q_OBJECT

public:
	cArt(QObject *parent);
	cArt(double _P, double _T);
	~cArt();

	void	init();								//����
    void	load(QString path);								//�������� ������ �����������
    QString	save();								//��������� ��������� � ����
	void	setT(double _T);
	void	setP(double _P);
	void	setCoefs(Matrix _cfs);				//��������� ������� �-��� ��������
	bool	teachSit(Vector _inp, Vector _sol);	//������� �������� (_inp - ����, _sol - �������)
	Vector	getSol(	 Vector _inp, bool   _byP);	//�������� ������� (_inp - ����, _byP - ��������� �����, ������ �������)

private:
	int				n_;			//���������� ��������
	double			T_,			//����� ���������������� ��� �������� 
					P_;			//����� ���������������� ��� ������
	QVector<std_>	standarts_;	//������ ��������
	Matrix			coefs_;		//������� �-��� ��������
	
};

#endif // CART_H
