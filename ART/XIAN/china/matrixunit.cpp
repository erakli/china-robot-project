#include "matrixunit.h"

Vector::Vector(): BaseVector(0)
{
		
}
//________________________________________________________
Vector::Vector(int n): BaseVector(n)
{
		
}
//________________________________________________________
Vector::Vector(const Vector &v): BaseVector(v)
{
		
}
//________________________________________________________
int		Vector::getSize()
{
	return (int)((*this).size());
}
//________________________________________________________
double	Vector::getMagnitude()
{
		double amnt = 0;
		for (int i = 0; i<=this->getSize() - 1; i++) {amnt += (*this)[i]*(*this)[i];}
		return sqrt(amnt);
}
//________________________________________________________
void	Vector::toNorm()
{
	double norm = this->getMagnitude();
	for (int i = 0; i<=this->getSize() - 1; i++) (*this)[i] = (*this)[i]/norm;
}	
//________________________________________________________
Vector	Vector::operator + (const Vector& b)
{
	Vector temp( std::min<int>(this->getSize(), b.size())  );
	for (int i = 0; i<=this->getSize() - 1; i++) temp[i] = (*this)[i] + b[i];
	return temp;
}
//________________________________________________________
Vector	Vector::operator - (const Vector& b)
{
    Vector temp ( std::min<int>(this->getSize(), b.size()) );
	for (int i = 0; i<=this->getSize() - 1; i++) temp[i] = (*this)[i] - b[i];
	return temp;
}
//________________________________________________________
Vector	Vector::operator * (const double& v)
{
	Vector temp(this->getSize());
	for (int i = 0; i<=this->getSize() - 1; i++) temp[i] = (*this)[i]*v;
	return temp;
}
//________________________________________________________
double	Vector::operator * (const Vector& b)
{
	double res = 0;
	for (int i = 0; i<=this->getSize() - 1; i++) {res += (*this)[i]*b[i];}
	return res;
}
//________________________________________________________
Vector	Vector::operator * (const Matrix& b)
{
	Vector temp(b.getCol());
	for (int j = 0; j<=b.getCol() - 1; j++)
	{
		for (int i = 0; i<=this->getSize() - 1; i++)
		{
			temp[j] += (*this)[i]*b[i][j];
		}
	}
	return temp;
}
//________________________________________________________
Vector	Vector::crossProd  (const Vector& v)
{
	Vector result(3);
	result[0] = (*this)[1]*v[2] - (*this)[2]*v[1];
	result[1] = (*this)[2]*v[0] - (*this)[0]*v[2];
	result[2] = (*this)[0]*v[1] - (*this)[1]*v[0];
	return result;
}
//________________________________________________________
Matrix	Vector::toMatrix()
{
	double	n=(*this).size(), i, j;
	Matrix	res(sqrt(n), sqrt(n));

	for(i=0;i<sqrt(n);i++) {
		for(j=0;j<sqrt(n);j++) {
			res[i][j]=(*this)[i*sqrt(n)+j];
		}
	}
	return res;
}
//########################################################
Matrix::Matrix(): BaseMatrix()
{

}
//________________________________________________________
Matrix::Matrix (int n, int m): BaseMatrix()
{
	this->resize(n, m);
}
//________________________________________________________
Matrix::Matrix(const Matrix& m): BaseMatrix(m)
{
	
}
//________________________________________________________
void		Matrix::resize(int n, int m)
{
	BaseMatrix::resize(n);
	for (int i = 0; i<n; i++)
		(*this)[i].resize(m);
}
//________________________________________________________
int			Matrix::getRow() const
{
	return this->size();
}
//________________________________________________________
int			Matrix::getCol() const
{
	return (this->size() > 0) ? (*this)[0].size() : 0;
}
//________________________________________________________
BaseVector&	Matrix::operator[](int i) 
{
	return  BaseMatrix::operator[](i);
}
//________________________________________________________
const		BaseVector& Matrix::operator[](int i) const
{
	return  BaseMatrix::operator[](i);
}
//________________________________________________________
Matrix		Matrix::operator + (const Matrix& b)
{
	Matrix temp(std::min<int>(this->getRow(), b.getRow()), std::min<int>(this->getCol(), b.getCol()));
	for (int i = 0; i<=temp.getRow() - 1; i++)
	{
		for (int j = 0; j<=temp.getCol() - 1; j++) temp[i][j] = (*this)[i][j] + b[i][j];
	}
	return temp;
}
//________________________________________________________
Matrix		Matrix::operator - (const Matrix& b)
{
	Matrix temp(std::min<int>(this->getRow(), b.getRow()), std::min<int>(this->getCol(), b.getCol()));
	for (int i = 0; i<=this->getRow() - 1; i++)
	{
		for (int j = 0; j<=this->getCol() - 1; j++) temp[i][j] = (*this)[i][j] - b[i][j];
	}
	return temp;
}
//________________________________________________________
Matrix		Matrix::operator * (const double& v)
{
	Matrix temp(this->getRow(), this->getCol());
	for (int i = 0; i<=this->getRow() - 1; i++)
	{
		for (int j = 0; j<=this->getCol() - 1; j++) temp[i][j] = (*this)[i][j]*v;
	}
	return temp;
}
//________________________________________________________
Matrix		Matrix::operator * (const Matrix& b)
{
	Matrix temp(this->getRow(), b.getCol());
       for (int i = 0; i<=temp.getRow() - 1; i++)
       {
			for (int j = 0; j<=temp.getCol() - 1; j++)
            {
				temp[i][j] = 0;
		        for (int k = 0; k<=b.getRow() - 1; k++)
                {
                    temp[i][j] = temp[i][j] + (*this)[i][k]*b[k][j];
                }
            }
        }
		return temp;
}
//________________________________________________________
Matrix&		Matrix::operator = (const Matrix& m)
{
	this->resize(m.getRow(), m.getCol());
	for (int i = 0; i<=m.getRow() - 1; i++)
	{
		for (int j = 0; j<=m.getCol() - 1; j++) (*this)[i][j] = m[i][j];
	}
	return *this;
}
//________________________________________________________
Matrix      Matrix::flip()
{
	Matrix temp(this->getCol(), this->getRow());
	for (int i = 0; i<=this->getCol() - 1; i++)
	{
		for (int j = 0; j<=this->getRow() - 1; j++)
		{
			temp[i][j] = (*this)[j][i];
		}
	}
	return temp;
}
//________________________________________________________
Vector      Matrix::operator * (const Vector& b)
{
	Vector temp(this->getRow());
	for (int i = 0; i<=this->getRow() - 1; i++)
	{
		temp[i] = 0;
		for (int j = 0; j<=this->getCol() - 1; j++)
		{
			temp[i] = temp[i] + (*this)[i][j]*b[j];
		}
	}
	return temp;
}
//________________________________________________________
Matrix      Matrix::inverse()
{
	int i, j, k, n;
	double a;
	n = this->getRow();
	Matrix R(n, n), C(n, n);
	Matrix result(n, n);

	for (k = 0; k<=n - 1; k++)	//�������
	{
		for (i = 0; i<=n - 1; i++)
			{
				R[k][i] = (*this)[k][i];
			}
	}

	for (i = 0; i<=n - 1; i++)	//��������
	{
		for (j = 0; j<=n - 1; j++)
		{
			if (i == j) {C[i][j] = 1.;}
			else        {C[i][j] = 0.;}
		}
	}

	for (k = 0; k<=n - 1; k++)	//"�����"(c)
	{
		for (i = k + 1; i<=n - 1; i++)
		{
			a=R[i][k];
			for (j = k; j<= n - 1; j++)
				R[i][j]=R[i][j]-a*(R[k][j]/R[k][k]);
               for (j = 0; j<= n - 1; j++) 
				C[i][j]=C[i][j]-a*(C[k][j]/R[k][k]);
		}
	}

	for (k = n - 1; k>= 1; k--) 
	{
		for (i = k; i>=1; i--) 
		{
			a=R[i-1][k];
			for (j = k; j>=0; j--)
				 R[i-1][j]=R[i-1][j]-a*(R[k][j]/R[k][k]);
			for (j = 0; j<=n - 1; j++) 
				 C[i-1][j]=C[i-1][j]-a*(C[k][j]/R[k][k]);
		}	
	}


	for (i = 0; i<=n - 1; i++)
	{
		for (j = 0; j<=n - 1; j++)
		{
			C[i][j] = C[i][j]/R[i][i];
		}
	}

	for (i = 0; i<=n - 1; i++)
	{
		for (j = 0; j<=n - 1; j++)
		{
			result[i][j] = C[i][j];
		}
	}

	return result;
		
}
//________________________________________________________
Vector		Matrix::toVector()
{
	Vector res;

	if((*this).getRow()!=(*this).getCol())	return res; //���� �� ����������, �� ������ �� ������ (��, ����...)

	if((*this).getRow()==0||(*this).getCol()==0) return res; //���� ����� �������, �� ����...

	int n=(*this).size(), i, j;

	res.resize(n*n);

	for(i=0;i<n;i++) {
		for(j=0;j<n;j++) {
			res[i*n+j]=(*this)[i][j];
		}
	}

	return res;
}
//________________________________________________________
void		Matrix::toFile(QString file, int prec)
{
	QFile	f(file);
	QString	res, tab="\t", endl="\n";
	int		i, n=getRow(),
			j, m=getCol();
	
	for(i=0;i<n;i++) {
		for(j=0;j<m;j++) {
			res+=QString::number((*this)[i][j], 'f', prec);
			res+=tab;
		}
		if(i!=n-1) res+=endl;
	}

	if(	f.open(QIODevice::WriteOnly)) {
//		f.write(res.toAscii());
		f.close();
	}
}
//########################################################
SymmMatrix::SymmMatrix(): Matrix()
{
	
}
//________________________________________________________
SymmMatrix::SymmMatrix(int n, int m): Matrix(n, m)
{
	
}
//________________________________________________________
SymmMatrix	SymmMatrix::inverse()
{
	int i, j, k, n = this->getRow();
    Matrix L(n, n);
    SymmMatrix  result(n, n);
	double amnt = 0., temp;
		
	for (i = 1; i<=n; i++)
	{
		for (j = 1; j<=i - 1; j++)
		{
			amnt = 0;
			for (k = 1; k<=j - 1; k++) amnt+=L[i-1][k-1]*L[j-1][k-1];
			L[i-1][j-1]=((*this)[i-1][j-1]-amnt)/L[j-1][j-1];
		}
		amnt = (*this)[i-1][i-1];
		for (k = 1; k<=i - 1; k++) amnt -= pow(L[i-1][k-1], 2);
		L[i-1][i-1] = sqrt(amnt);
	}

	for (i = n; i>=1; i--)
	{
		amnt = 0;
		for (k = i+1; k<=n; k++)
		{
			temp=L[k-1][i-1]*result[k-1][i-1];
			amnt += temp;
		}
		result[i-1][i-1]=(1/L[i-1][i-1])*(1/L[i-1][i-1] - amnt);
		for (j = i - 1; j>=1; j--)
		{
			amnt = 0;
			for (k = j + 1; k<=n; k++) amnt = amnt + L[k-1][j-1]*result[k-1][i-1];
			result[i-1][j-1] = -amnt/L[j-1][j-1];
			result[j-1][i-1] = result[i-1][j-1];
		}
	}
	return result;
}
