#include <math.h>
#include <vector>
#include "qstring.h"
#include "qfile.h"

#pragma once 
#ifndef MATRIXINIT_H
#define MATRIXUNIT_H

typedef	std::vector<double>						BaseVector;
typedef std::vector<BaseVector>					BaseMatrix;

class Matrix;

class Vector: public BaseVector  
{
public:

	Vector();
	Vector(int n);
    Vector(const Vector& v);

	int		getSize();
	double	getMagnitude();
	void	toNorm();
    Vector	operator + (const Vector& b);
    Vector	operator - (const Vector& b);
    Vector	operator * (const double& v);
    double	operator * (const Vector& b);
    Vector	operator * (const Matrix& b);
    Vector	crossProd  (const Vector& v);
	Matrix	toMatrix   (			   );
};

class Matrix: protected BaseMatrix
{
public:
			Matrix();
			Matrix (int n, int m);
            Matrix (const Matrix& m);
	
			void		resize(int n, int m);
			int			getRow() const;
			int			getCol() const;
			BaseVector&	operator[] (int i);
const		BaseVector& operator[] (int i) const;
            Matrix		operator + (const Matrix& b);
            Matrix		operator - (const Matrix& b);
            Matrix		operator * (const double& v);
            Matrix		operator * (const Matrix& b);
            Matrix&		operator = (const Matrix& m);
			Matrix		flip();
            Vector		operator * (const Vector& b);
			Matrix		inverse();
			Vector		toVector();
			void		toFile(QString fname, int prec);
};

class SymmMatrix: public Matrix
{
public:
	SymmMatrix();
	SymmMatrix(int n, int m);
    SymmMatrix  inverse();
};

#endif
