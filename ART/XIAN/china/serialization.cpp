#include "serialization.h"

#define BYTE_MAX            256

#ifdef QT_SIDE
using namespace std;
#endif

uint8_t evalCheckSum(uint8_t *buffer, uint16_t bufferSize) {
    uint16_t sum = 0;
    for (uint8_t i = 0; i < bufferSize; i++) {
        sum += buffer[i];
    }

    return (uint8_t)(sum % BYTE_MAX);
}



// uint8_t * serialize_uint8(uint8_t *buffer, uint8_t value) {
//     buffer[0] = value;
//     return (buffer + 1);
// }

// uint8_t * serialize_uint16(uint8_t *buffer, uint16_t value) {
//     // Big-endian
//     buffer[0] = value >> 8;
//     buffer[1] = value;
//     return (buffer + 2);
// }

// uint8_t * serialize_uint32(uint8_t *buffer, uint32_t value) {
//     // Big-endian
//     buffer[0] = value >> 24;
//     buffer[1] = value >> 16;
//     buffer[2] = value >> 8;
//     buffer[3] = value;
//     return (buffer + 4);
// }



// uint8_t * deserialize_uint8(uint8_t *buffer, uint8_t &value) {
//     value = buffer[0];
//     return (buffer + 1);
// }

// uint8_t * deserialize_uint16(uint8_t *buffer, uint16_t &value) {
//     value = (buffer[0] << 8) | buffer[1];
//     return (buffer + 2);
// }

// uint8_t * deserialize_uint32(uint8_t *buffer, uint32_t &value) {
//     value = (buffer[0] << 24) | (buffer[1] << 16) | (buffer[2] << 8) | buffer[3];
//     return (buffer + 4);
// }