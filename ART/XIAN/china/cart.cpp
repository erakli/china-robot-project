#include "cart.h"

cArt::cArt(QObject *parent)
	: QObject(parent)
{
	init();
}
//___________________________________________________
cArt::cArt(double _P, double _T)
{
	init();

	if(_P>=0&&_P<=1)	P_=_P;	else	P_=0.85;
	if(_T>=0&&_T<=1)	T_=_T;	else	T_=0.85;
}
//___________________________________________________
cArt::~cArt()
{

}
//___________________________________________________
void	cArt::init()
{
    P_=T_=0.85;
	standarts_.clear();
	n_=0;

    coefs_.resize(16, 16);

    for(int i = 0; i < 16; ++i)
        coefs_[i][i] = 1;
}
//___________________________________________________
void cArt::load(QString path){
    QFile f(path);//("results.art");
    QStringList slist, slistr;
    int i, j,
    n, m;

    if( f.open(QIODevice::ReadOnly)) {
        slist=QString::fromUtf8(f.readAll()).split("\n");// ::fromAscii(f.readAll()).split("\n");

    n=slist.size();

    if(n<3) return;

    for(i=0;i<n-4;i+=4) {
        std_ std;

    slistr=slist[i].split("\t");

    m=slistr.size()-1;

    for(j=0;j<m;j++) {
        std.inp_.push_back(slistr[j].toDouble());
    }

    slistr=slist[i+1].split("\t");

    m=slistr.size()-1;

    for(j=0;j<m;j++) {
        std.out_.push_back(slistr[j].toDouble());
    }

    slistr=slist[i+2].split("\t");

    m=slistr.size();

    for(j=0;j<m;j++) {
        std.c_=slistr[j].toDouble();
    }

    standarts_.push_back(std);
    }
    }
}
//___________________________________________________
QString	cArt::save()
{
	QFile	f("results.art");
	QString s="";

	int i, j,
		n, m;

	n=standarts_.size();

	for(i=0;i<n;i++) {
		m=standarts_[i].inp_.size();
		for(j=0;j<m;j++) {
			s=s+QString::number(standarts_[i].inp_[j], 'f', 10)+"\t";
		}
		s+="\n";

		m=standarts_[i].out_.size();
		for(j=0;j<m;j++) {
			s=s+QString::number(standarts_[i].out_[j], 'f', 10)+"\t";
		}
		s+="\n";

		s=s+QString::number(standarts_[i].c_, 'f', 10)+"\n";

		s+="\n";
	}

    QString path = QFileDialog::getSaveFileName(0, "Save ART", "RESULT", "*.art");
    f.setFileName(path);
	if(	f.open(QIODevice::WriteOnly)) {
        f.write(s.toUtf8());
        return path;
    } else return "save error";
}
//___________________________________________________
void	cArt::setP(double _P)
{
	P_=_P;
}
//___________________________________________________
void	cArt::setT(double _T)
{
	T_=_T;
}
//___________________________________________________
void	cArt::setCoefs(Matrix _cfs)
{
	coefs_=_cfs;
}
//___________________________________________________
bool	cArt::teachSit(Vector _inp, Vector _sol)
{
	int i, n;

	_inp.toNorm();		//���������� �����
    _inp=coefs_*_inp;	//�������� �� �-�� ��������

	n=standarts_.size();	//������� ���������� ��������� ��������

	if(!n)	{ //���� �� ��� ���, �� ������ �����
		n_++;
		n ++;

		std_	std;
				std.inp_=_inp;
				std.out_=_sol;
				std.c_++;

		standarts_.push_back(std);

		return true; //������ ������, �.�. ������ ����� ������
	}
	else {	//���� �� 0, �.�. ��� ���� �������, ���� �������� � ����
		QVector<double>	koh(n);	//������ ������� ��������
		int		maxi= 0;
		double	maxc=-5.0;

		//���� ������ � ������ ��� ��������
		for(i=0;i<n;i++) {
			koh[i]=_inp*standarts_[i].inp_;	

			if(koh[i]>maxc) {
				maxc=koh[i];
				maxi=i;
			}
		}

		//���� ������������ ������� ������, ��� �����, � ����� ������� ��� ����������, ��...
		if(maxc>T_&&standarts_[maxi].out_==_sol) {
			standarts_[maxi].addVect(_inp); //����������� ��������� ������
			return false;	//����� ���������� �� �������
		}
		else { //���� ���-�� �� ��� ������� �� ���, �� ������ ����� �������
			n ++;
			n_++;
			std_	std;
					std.inp_=_inp;
					std.out_=_sol;
					std.c_++;

            standarts_.push_back(std);
			return	true;	//����� �������
		}
	}
}
//___________________________________________________
Vector	cArt::getSol(Vector _inp, bool _byP)
{
    int		i, n;
    n = standarts_.size();
	_inp.toNorm();	//�������� ����
	_inp=coefs_*_inp;	//�������� �� �-�� ��������

	QVector<double>	koh(n);	//������ ������� ��������
	int		maxi= 0;
	double	maxc=-5.0;

	//������ � ������ ������ ��������
	for(i=0;i<n;i++) {
		koh[i]=_inp*standarts_[i].inp_;	
		
		if(koh[i]>maxc) {
			maxc=koh[i];
			maxi=i;
		}
	}

	if(_byP) {	//���� ������ ���� ����� ������, ��
		if(maxc>P_) {	//���� ������������ ������� ������ ������, ��
			return standarts_[maxi].out_;	//��������� ��������������� �������
		}
		else {	//���� ������ ������, �� ��������� ������ ������
			return Vector(standarts_[maxi].out_.size());
		}
	}

	//�� � ���� ���� �� ������, �� ����� ����� �� ���� ������ �...
	return standarts_[maxi].out_;	//������ ��������������� �������
}
