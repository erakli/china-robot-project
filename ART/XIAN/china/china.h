#ifndef CHINA_H
#define CHINA_H

//#include <QtTest/QTest>
#define sleepMs  1000

#include <QtGui>
//#include <QtGui/QMainWindow>
#include "ui_china.h"
#include <QDateTime>
#include <QTextCodec>
#include <QKeyEvent>
#include <QtSerialPort/QSerialPort>
#include "cart.h"
#include "qfile.h"
#include "qfiledialog.h"
#include "qstringlist.h"
#include "qtimer.h"
#include "stdafx.h"
#include <Windows.h>
#include <iostream>
#include <cstdint>
#include <qdebug.h>
#include <string.h>
#include "StateVectorMessage.h"


class china : public QMainWindow
{
	Q_OBJECT

public:
    china(QWidget *parent = 0);
//	china(QWidget *parent = 0, Qt::WFlags flags = 0);
	~china();

private:
	Ui::chinaClass ui;

	//��������
	bool	mode_;		//����� (0 - ��������, 1 - ����������������)
    bool    flagFirstCom; // флаг 1й команды. если он false значит запоминаем предыдущую команду
    double  prevCom;    // предыдущая команда
    int     numSameCom; // счетчик одинаковых команд. если одна и та же команда зациклилась - меняем ее сами!
	cArt*	part_;		//����� ART
	Vector	INP__;		//�������  ������ �  ����
	Vector	SOL__;		//�������� ������ �� ����
    QTimer	tmrRead_;	//������ ��� ������ �� � ���
	HANDLE	hSerial_;	//�����. ����
    QSerialPort serialPort;
    QString     serialPortName;

    void    delay(int ms);
    void	sendSol(QString command);	//��������� �������
    bool    getData();  //������� ���� � ��������
    void    write2serial(); //����� � ������
    bool    analyzeCurPrev(double  curCom); //возвращает true, если команда текущая и предыдущая разные
	//#################

	void keyPressEvent(QKeyEvent *ev);
    //void readPort();
private slots:
	void on_upBtn_clicked();
	void on_uprightBtn_clicked();
	void on_upleftBtn_clicked();
	void on_leftBtn_clicked();
	void on_rightBtn_clicked();
	void on_downBtn_clicked();
	void on_downrightBtn_clicked();
	void on_downleftBtn_clicked();
	void on_stopBtn_clicked();

	void on_trainBtn_clicked();
	void on_totrainBtn_clicked();
	void on_networkBtn_clicked();

	void on_stopTrainBtn_clicked();
	void on_stopToTrainBtn_clicked();
	void on_stopNetworkBtn_clicked();

	void on_loadBtn_clicked();
	void on_saveBtn_clicked();

	//��������
	void on_setTBtn_clicked();
	void on_setPBtn_clicked();
	void on_setCoefsBtn_clicked();

	void slotTmrRead();
	//###############

    void on_pbConnect_clicked();
    void on_pbDisconnect_clicked();

    void on_pbSetCom_clicked();
};

#endif // CHINA_H
