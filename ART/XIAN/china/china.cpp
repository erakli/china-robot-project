#include "china.h"

china::china(QWidget *parent/*, Qt::WFlags flags*/)
    : QMainWindow(parent/*, flags*/)
{
	ui.setupUi(this);

	ui.upBtn		->	setEnabled(false);
	ui.downBtn		->	setEnabled(false);
	ui.leftBtn		->	setEnabled(false);
	ui.rightBtn		->	setEnabled(false);
	ui.stopBtn		->	setEnabled(false);
	ui.upleftBtn	->	setEnabled(false);
	ui.uprightBtn	->	setEnabled(false);
	ui.downleftBtn	->	setEnabled(false);
	ui.downrightBtn	->	setEnabled(false);

	ui.stopTrainBtn		-> setEnabled(false);
	ui.totrainBtn		-> setEnabled(false);
	ui.stopToTrainBtn	-> setEnabled(false);
	ui.networkBtn		-> setEnabled(false);
	ui.stopNetworkBtn	-> setEnabled(false);

	this->setFocus();

	//������ ����� � ���������� �����������
	part_ = new cArt(0);

	ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " ����������� ������� ������ � ������� ������� ��� ����!!!");

	//��������� �������
	INP__.resize(TTLSIZE__);
	SOL__.resize(OUTSIZE__);

    //ifi
    QFile ff("com.txt");

    if(ff.open(QIODevice::ReadOnly)) {
       serialPortName=QString::fromUtf8(ff.readAll());
    }
    qDebug()<<serialPortName;
   // serialPortName = "COM7";
    serialPort.setPortName(serialPortName);
    serialPort.setBaudRate(QSerialPort::Baud115200);

    ui.trainBtn->setText("training");
    ui.stopTrainBtn->setText("stop training");
    ui.networkBtn->setText("network");
    ui.stopNetworkBtn->setText("stop network");
    ui.saveBtn->setText("save");
    ui.loadBtn->setText("load");
    ui.setPBtn->setText("set P");
    ui.setTBtn->setText("set T");
    ui.tabWidget->setTabText(0, "Training");
    ui.tabWidget->setTabText(1, "to raining");
    ui.tabWidget->setTabText(2, "Network");
    ui.tabWidget->setTabText(3, "Settings");
   // while(!serialPort.open(QIODevice::ReadOnly)) {
    //}
    //if(!serialPort.open(QIODevice::ReadWrite)) {
      //  qDebug()<<"не открылся";
    //}
	//�������� ������
	connect(&tmrRead_, SIGNAL(timeout()), SLOT(slotTmrRead()));
    //tmrRead_.start(500);
   // connect(ui.pbConnect, SIGNAL(clicked()), SLOT(on_pbConnect_clicked()));
   // connect(ui.pbDisconnect, SIGNAL(clicked()), SLOT(on_pbDisconnect_clicked()));
    //�� ��������� - ��������
    mode_        = false;
    flagFirstCom = true;
    prevCom      = 0;
}
//_____________________________________________________________
china::~china()
{

}
//_____________________________________________________________
void china::on_upBtn_clicked()
{
	this->setFocus();

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": ACTION \"FORWARD!\"");

    SOL__[0] = 8;

    if(getData()==false) {
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": There is no any connected device");
        return;
    }

	bool res=false;

	if(!mode_)
		res = part_->teachSit(INP__, SOL__);
    if(!res) ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass does not create"	);
    else	 ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass create"	);

    sendSol("8");
}
//_____________________________________________________________
void china::on_uprightBtn_clicked()
{
	this->setFocus();

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": ACTION \"FORWARD RIGHT!\"");

	SOL__[0] = 45;

    if(getData()==false) {
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": There is no any connected device");
        return;
    }

	bool res=false;

	if(!mode_)
		res = part_->teachSit(INP__, SOL__);
    if(!res) ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass does not create"	);
    else	 ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass create  " + QString::number(SOL__[0]));
}
//_____________________________________________________________
void china::on_upleftBtn_clicked()
{
	this->setFocus();

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": ACTION \"FORWARD LEFT!\"");

	SOL__[0] = 315;

    if(getData()==false) {
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": There is no any connected device");
        return;
    }

	bool res=false;

	if(!mode_)
		res = part_->teachSit(INP__, SOL__);
    if(!res) ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass does not create"	);
    else	 ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass create"	);
}
//_____________________________________________________________
void china::on_leftBtn_clicked()
{
	this->setFocus();

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": ACTION \"LEFT!\"");

    SOL__[0] = 4;

    if(getData()==false) {
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": There is no any connected device");
        return;
    }

	bool res=false;

	if(!mode_)
		res = part_->teachSit(INP__, SOL__);
    if(!res) ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass does not create"	);
    else	 ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass create"	);

    sendSol("4");
}
//_____________________________________________________________
void china::on_rightBtn_clicked()
{
    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": ACTION \"RIGHT!\"");

    SOL__[0] = 6;

    if(getData()==false) {
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": There is no any connected device");
        return;
    }

	bool res=false;

	if(!mode_)
		res = part_->teachSit(INP__, SOL__);
    if(!res) ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass does not create"	);
    else	 ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass create"	);

    sendSol("6");
}
//_____________________________________________________________
void china::on_downBtn_clicked()
{
    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": ACTION \"BACK!\"");

    SOL__[0] = 2;

    if(getData()==false) {
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": There is no any connected device");
        return;
    }

	bool res=false;

	if(!mode_)
		res = part_->teachSit(INP__, SOL__);
    if(!res) ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass does not create"	);
    else	 ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass create"	);

    sendSol("2");
}
//_____________________________________________________________
void china::on_downrightBtn_clicked()
{
    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": ACTION \"BACK RIGHT!\"");

	SOL__[0] = 135;

    if(getData()==false) {
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": There is no any connected device");
        return;
    }

	bool res=false;

	if(!mode_)
		res = part_->teachSit(INP__, SOL__);
    if(!res) ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass does not create"	);
    else	 ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass create"	);
}
//_____________________________________________________________
void china::on_downleftBtn_clicked()
{
    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": ACTION \"BACK LEFT!\"");

	SOL__[0] = 225;

    if(getData()==false) {
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": There is no any connected device");
        return;
    }

	bool res=false;

	if(!mode_)
		res = part_->teachSit(INP__, SOL__);
    if(!res) ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass does not create"	);
    else	 ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass create"	);
}
//_____________________________________________________________
void china::on_stopBtn_clicked()
{
    serialPort.write("5");
    return;

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": ACTION \"STOP!\"");

    SOL__[0] = 5;

    if(getData()==false) {
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": There is no any connected device");
        return;
    }

	bool res=false;

	if(!mode_)
		res = part_->teachSit(INP__, SOL__);
    if(!res) ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new ass does not created"	);
    else	 ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " new (_o_) created"	);

 //   if(serialPort.open(QIODevice::WriteOnly)){
        serialPort.write("5");
 //   }
 //   serialPort.close();
}
//_____________________________________________________________
void china::on_trainBtn_clicked()
{
    tmrRead_.stop();

	ui.upBtn		->	setEnabled(true);
	ui.downBtn		->	setEnabled(true);
	ui.leftBtn		->	setEnabled(true);
	ui.rightBtn		->	setEnabled(true);
	ui.stopBtn		->	setEnabled(true);
	ui.upleftBtn	->	setEnabled(true);
	ui.uprightBtn	->	setEnabled(true);
	ui.downleftBtn	->	setEnabled(true);
	ui.downrightBtn	->	setEnabled(true);

	ui.stopTrainBtn ->  setEnabled(true);
	ui.networkBtn	->  setEnabled(false);
	ui.totrainBtn	->  setEnabled(false);

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": Mode \"Training\" activated!");

	this->setFocus();

	mode_=false;
}
//_____________________________________________________________
void china::on_totrainBtn_clicked()
{
	ui.upBtn		->	setEnabled(false);
	ui.downBtn		->	setEnabled(false);
	ui.leftBtn		->	setEnabled(false);
	ui.rightBtn		->	setEnabled(false);
	ui.stopBtn		->	setEnabled(false);
	ui.upleftBtn	->	setEnabled(false);
	ui.uprightBtn	->	setEnabled(false);
	ui.downleftBtn	->	setEnabled(false);
	ui.downrightBtn	->	setEnabled(false);

	ui.networkBtn	  -> setEnabled(false);
	ui.stopToTrainBtn -> setEnabled(true);
	ui.trainBtn		  -> setEnabled(false);
	ui.stopTrainBtn	  -> setEnabled(false);

    ui.totrainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": Mode \"Totraining\" ouliyts!");

	mode_=false;
}
//_____________________________________________________________
void china::on_networkBtn_clicked()
{   
	ui.upBtn		->	setEnabled(false);
	ui.downBtn		->	setEnabled(false);
	ui.leftBtn		->	setEnabled(false);
	ui.rightBtn		->	setEnabled(false);
	ui.stopBtn		->	setEnabled(false);
	ui.upleftBtn	->	setEnabled(false);
	ui.uprightBtn	->	setEnabled(false);
	ui.downleftBtn	->	setEnabled(false);
	ui.downrightBtn	->	setEnabled(false);

	ui.stopNetworkBtn -> setEnabled(true);
	ui.trainBtn		  -> setEnabled(false);
	ui.totrainBtn	  -> setEnabled(false);

    ui.networkLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": Mode \"Network\" activated!");

	mode_=true;

    tmrRead_.start(500);
}
//_____________________________________________________________
void china::on_stopTrainBtn_clicked()
{
	ui.upBtn		->	setEnabled(false);
	ui.downBtn		->	setEnabled(false);
	ui.leftBtn		->	setEnabled(false);
	ui.rightBtn		->	setEnabled(false);
	ui.stopBtn		->	setEnabled(false);
	ui.upleftBtn	->	setEnabled(false);
	ui.uprightBtn	->	setEnabled(false);
	ui.downleftBtn	->	setEnabled(false);
	ui.downrightBtn	->	setEnabled(false);

	ui.totrainBtn -> setEnabled(true);
	ui.networkBtn -> setEnabled(true);
	ui.stopTrainBtn ->setEnabled(false);

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": Mode \"Training\" deactivated!");
}
//_____________________________________________________________
void china::on_stopToTrainBtn_clicked()
{
	ui.networkBtn		-> setEnabled(true);
	ui.trainBtn			-> setEnabled(true);
	ui.stopToTrainBtn	-> setEnabled(false);
    ui.totrainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": Mode \"Totraining\" deactivated!");
}
//_____________________________________________________________
void china::on_stopNetworkBtn_clicked()
{
    ui.trainBtn			-> setEnabled(true );
    ui.totrainBtn		-> setEnabled(true );
	ui.stopNetworkBtn	-> setEnabled(false);

    ui.networkLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": Mode \"Network\" deactivated!");

    mode_=false;

    getData();

    serialPort.write("5");
}
//_____________________________________________________________
void china::keyPressEvent( QKeyEvent *ev )
{
	switch(ev -> key()){
	case Qt::Key_Up:
		if(ev->modifiers() &&	Qt::ShiftModifier){
			this->on_upleftBtn_clicked();
			break;
		}
		if(ev->modifiers() && Qt::AltModifier){
			this->on_uprightBtn_clicked();
			break;
		}
		this->on_upBtn_clicked();
		break;
	case Qt::Key_Down:
		if(ev->modifiers() && Qt::ShiftModifier){
			this->on_downleftBtn_clicked();
			break;
		}
		if(ev->modifiers() && Qt::AltModifier){
			this->on_downrightBtn_clicked();
			break;
		}

		this->on_downBtn_clicked();
		break;
	case Qt::Key_Left:
		this->on_leftBtn_clicked();
		break;
	case Qt::Key_Right:
		this->on_rightBtn_clicked();
		break;
	case Qt::Key_Space:
		this->on_stopBtn_clicked();
		break;
		
	}

}
//_____________________________________________________________
void china::on_loadBtn_clicked()
{
    QString path = QFileDialog::getOpenFileName(0, "Load", "", "*.art");
    part_->load(path);

    ui.networkBtn->setEnabled(true);

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": successefull loaded");
}
//_____________________________________________________________
void china::on_saveBtn_clicked()
{	
    QString path = part_->save();
    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": successefull saved in " + path);
}
//_____________________________________________________________
void china::on_setTBtn_clicked()
{
	double		t = ui.le_T->text().toDouble();
	part_->setT(t);

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": T setted");
}
//_____________________________________________________________
void china::on_setPBtn_clicked()
{
	double		p = ui.le_P->text().toDouble();
	part_->setP(p);

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": P setted");
}
//_____________________________________________________________
void china::on_setCoefsBtn_clicked()
{
	Matrix	c(TTLSIZE__, TTLSIZE__);

	QFile		f("matrix.art");
	QStringList	fl;
	int			i, j, m;

	if(f.open(QIODevice::ReadOnly)) {
        fl = QString::fromUtf8(f.readAll()).split("\t");

//		for(i=0;i<EPCOUNT__;i++) {

//			m=fl.size();

//			for(j=0;j<m;j++) {
//				c[j*i+j][j*i+j]=fl[j].toDouble();
//			}
            for(i=0;i<fl.size();i++) {
                c[i][i]=fl[i].toDouble();
          //  }
		}
	}

    ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + ": weight setted");
}
//_____________________________________________________________
void china::slotTmrRead()
{  
    getData();
}
//_____________________________________________________________
void china::delay(int ms)
{
    QEventLoop loop;
    QTimer::singleShot(ms, &loop, SLOT(quit()));
    loop.exec();
}
//_____________________________________________________________
void china::sendSol(QString command)
{
    if(!mode_)
        return;

    double com = command.toDouble();
    if(analyzeCurPrev(com)){
        serialPort.write("5");
        delay(sleepMs);

        prevCom    = com;
        numSameCom = 0;
    } else {
        numSameCom++;
    }
    
    QString comToRobot = command;

    if(numSameCom >= 6){
        serialPort.write("5");
        delay(sleepMs);
        if(command == "8")
            comToRobot = "2";
        if(command == "2")
            comToRobot = "8";
        if(command == "6")
            comToRobot = "4";
        if(command == "4")
            comToRobot = "6";

        numSameCom = 0;
    }

    //serialPort.write(command.toStdString().c_str());
    serialPort.write(comToRobot.toStdString().c_str());
    ui.networkLog->append(comToRobot);
}
//_____________________________________________________________
bool china::getData()
{
 //   if (!serialPort.open(QIODevice::ReadWrite)) {
 //       qDebug()<<"poshel nah so svoim robotom";
 //       return false;
 //   }

    qDebug()<<"oiluts";
    //    Message message;
        StateVectorMessage message;
        uint16_t bytesRead;
        uint16_t bufferSize;
        uint8_t * ptr;
        QTextStream standardOutput(stdout);
        QByteArray readData = serialPort.readAll();
        while (serialPort.waitForReadyRead(10000)) {
            qDebug()<<readData;
            readData   = readData.append(serialPort.readAll());
            bufferSize = readData.size();
    //        if (bufferSize >= sizeof(Message) * 3) {
            if (bufferSize >= sizeof(StateVectorMessage) * 3) {
                // try to read message
    //            ptr = unpackMessage((std::uint8_t*)readData.data(), bufferSize, bytesRead, message);
                ptr = unpackStateVectorMessage((std::uint8_t*)readData.data(), bufferSize, bytesRead, message);
                if (ptr == NULL) {
                    standardOutput << "Message not found" << endl;
                    qDebug() << readData << endl;
                    if (bytesRead > 0)
                        readData = readData.mid(bytesRead);
                    continue;
                }

//                for (int i = 0; i < SIZE; i++) {
//                    standardOutput << QString::number(message.intVar[i]) << " ";
//                }
//                standardOutput << "; " << QString::number(message.longVar) << endl;

                for (int i = 0; i < STATE_VECTOR_SIZE; i++) {
                    standardOutput << QString::number(message.ultrasonicTimes[i]) << " ";
                    INP__[i]=message.ultrasonicTimes[i];
                }
                standardOutput << "; " << QString::number(message.yaw) << endl;

                if(mode_) {
                    SOL__ = part_->getSol(INP__, 1);
                    //ui.networkLog->append(QString::number(SOL__[0]));
                    //serialPort.write(res);
                    qDebug()<<"PISHU SUDA";

                    sendSol(QString::number(SOL__[0]));
                    /*serialPort.write("5");
                    delay(sleepMs);
                    serialPort.write(QString::number(SOL__[0]).toUtf8().toStdString().c_str());*/
                }

                // �� ��������� ���������, ����� ���������� �������
                if (bufferSize > bytesRead){
                    readData = readData.mid(bytesRead);
                    break;
                }
                else
                    readData.clear();
            }
        }

        if (serialPort.error() == QSerialPort::ReadError) {
            standardOutput << QObject::tr("Failed to read from port %1, error: %2").arg(serialPortName).arg(serialPort.errorString()) << endl;
            //return 1;
        } else if (serialPort.error() == QSerialPort::TimeoutError && readData.isEmpty()) {
            standardOutput << QObject::tr("No data was currently available for reading from port %1").arg(serialPortName) << endl;
            //return 0;
        }

        standardOutput << QObject::tr("Data successfully received from port %1").arg(serialPortName) << endl;
        standardOutput << readData << endl;

//        serialPort.close();

        return true;
}
//_____________________________________________________________
void china::write2serial()
{

}
//-------------------------------------------------------------
bool china::analyzeCurPrev(double  curCom)
{
    if(curCom != prevCom)
        return true;
    else
        return false;
}





void china::on_pbConnect_clicked()
{
    if(!serialPort.open(QIODevice::ReadWrite)) {
        qDebug()<<"не открылся";
        ui.trainLog->append("COM port " + serialPortName +  " doesn't opened");
    } else {
        ui.trainLog->append("COM port " + serialPortName +  " opened");
        tmrRead_.start(500);
    }
}

void china::on_pbDisconnect_clicked()
{
     if(serialPort.isOpen()) {
         serialPort.close();
         ui.trainLog->append("COM port " + serialPortName +  " closed");
         tmrRead_.stop();
     }
}

void china::on_pbSetCom_clicked()
{
    QString comName = ui.leCom->text();

    if(!serialPort.isOpen()) {
        serialPortName = comName;
        serialPort.setPortName(serialPortName);
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " New port name: " + serialPortName);
    }
    else
        ui.trainLog->append(QDateTime::currentDateTime().toString("hh:mm:ss") + " Close port to change portname! ");

}
