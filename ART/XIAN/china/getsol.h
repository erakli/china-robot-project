#ifndef GETSOL_H
#define GETSOL_H

#include <QDialog>
#include "ui_getsol.h"

class getSol : public QDialog
{
	Q_OBJECT

public:
	getSol(QWidget *parent = 0);
	~getSol();

private:
	Ui::getSol ui;
};

#endif // GETSOL_H
