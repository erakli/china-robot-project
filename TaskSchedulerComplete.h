#ifndef TASK_SCHEDULER_H
#define TASK_SCHEDULER_H 

// Здесь мы делаем полную инициализацию библиотеки TaskScheduler

#define _TASK_PRIORITY
#define _TASK_STATUS_REQUEST
#define _TASK_LTS_POINTER       // Compile with support for Local Task Storage pointer
#include <TaskScheduler.h> 

#endif