#ifndef DEBUG_H
#define DEGUG_H



//#include "MacroFunctions.h"

// #define DEBUG 
// #define TRACING 

#include <Arduino.h>  // Serial
#include <stdio.h> // sprintf

#define ARG_BUFFER_SIZE    255
extern char argBuffer[ARG_BUFFER_SIZE];
extern uint16_t bufPos;


#ifdef DEBUG

// int sprintf ( char * str, const char * format, ... );
// On success, the total number of characters written is returned.

    #define DEBUG_PORT		Serial

    //DPRINT is a macro, debug print
    #define DPRINT(...)    if (DEBUG_PORT) DEBUG_PORT.print(__VA_ARGS__)

    //DPRINTLN is a macro, debug print with new line
    #define DPRINTLN(...)  if (DEBUG_PORT) DEBUG_PORT.println(__VA_ARGS__) 


//    #define BUFFER_PRINT(a) \
//        do { \
//            bufPos += sprintf(argBuffer + bufPos, #a": %i; ", a); \
//        } while (false);	// здесь нужна точка с запятой
//
//    // печать аргументов
//    #define ARG_DPRINT(...) \
//        do { \
//            bufPos = 0; \
//            MAP(BUFFER_PRINT, __VA_ARGS__); \
//            DPRINTLN(argBuffer); \
//        } while (false) // а здесь нет

#ifdef TRACING
    extern uint8_t depthLevel;

    // печатаем отступ
    #define PRINT_IDENT(num) \
        do { \
            memset(argBuffer, '\t', num); \ 
            argBuffer[num] = '\0'; \
            DPRINT(argBuffer); \
        } while (false)

    //depthLevel + strlen(argBufferNew)
    // делаем отступ. на месте звёздочки n пробелов
    #define VALUE_PRINT(PRINT, fmt, val) \
        do { \
            PRINT_IDENT(depthLevel); \
            sprintf(argBuffer, fmt, val); \ 
            PRINT(argBuffer); \
        } while (false)

#else // TRACING
    // печать одного значения
    #define VALUE_PRINT(PRINT, fmt, val) \
        do { \
            sprintf(argBuffer, fmt, val); \
            PRINT(argBuffer); \
        } while (false)
#endif  // TRACING

    #define VALUE_DPRINT(val)     VALUE_PRINT(DPRINT, #val" = %i", val)
    #define VALUE_DPRINTLN(val)   VALUE_PRINT(DPRINTLN, #val" = %i", val)

#else // DEBUG

    #define DPRINT(...)     //now defines a blank line
    #define DPRINTLN(...)   //now defines a blank line

    #define ARG_DPRINT(...)

    #define VALUE_DPRINT(...) 
    #define VALUE_DPRINTLN(...) 

#endif // DEBUG



#if defined(DEBUG) && defined(TRACING)

    extern const uint8_t NewLine_None;
    extern const uint8_t NewLine_Before;
    extern const uint8_t NewLine_After;
    extern const uint8_t NewLine_Both;

    #define DEPTH_IDENT 1
    #define TRACE_DELAY   250

    // функции для трассирования вызовов функций
    #define TRACE(in_or_out, func_num, new_line, wait, inc_depth_level) \
        do { \
            if (new_line == NewLine_Before || new_line == NewLine_Both) DPRINTLN(" "); \
            if (in_or_out == true) { \
                PRINT_IDENT(depthLevel); \
                DPRINT("->"); \
            } else { \
                PRINT_IDENT(depthLevel - 1); \
                DPRINT("<-"); \
            } \
            DPRINTLN(func_num); \
            if (new_line == NewLine_After || new_line == NewLine_Both) DPRINTLN(" "); \
            if (inc_depth_level) depthLevel += DEPTH_IDENT; else depthLevel -= DEPTH_IDENT; \
            if (wait == true) delay(TRACE_DELAY); \
        } while (false)

    #define TRACEIN(func_num)	TRACE(true, func_num, NewLine_Before, true, true)
    #define TRACEOUT(func_num)	TRACE(false, func_num, NewLine_After, true, false)

    #define TRACE_ARG(arg)		VALUE_PRINT(DPRINTLN, " arg("#arg" = %i)", arg)

#else // defined(DEBUG) && defined(TRACING)

    #define TRACEIN(...)
    #define TRACEOUT(...)
        
    #define TRACE_ARG(...)

#endif // defined(DEBUG) && defined(TRACING)



#endif
