#ifndef ARRAY_FUNCTIONS_H
#define ARRAY_FUNCTIONS_H

#include "debug.h"
#include <Arduino.h> // Serial

#define VALUE_PRINT "%u,"

template <typename T>
void PrintArray(const T & arr, uint8_t count, const char * name = NULL) {
    if (name != NULL) {
        // печатаем имя
        bufPos = sprintf(argBuffer, "%s: ", name);
    } else {
        // bufPos = sprintf(argBuffer, "array: ");
        bufPos = 0;
    }

    for (uint8_t i = 0; i < count; i++) {
        bufPos += sprintf(argBuffer + bufPos, VALUE_PRINT, arr[i]);
    }
    
    Serial.print(argBuffer);
}


#ifndef US_ROUNDTRIP_CM
#define US_ROUNDTRIP_CM 57
#endif

template <typename T>
void DisplayStateVector(const T & arr, uint8_t count, 
    bool convert_to_cm = false, const char * name = NULL) 
{
    Serial.print(millis());
    if (name != NULL) {
        Serial.print(", ");
        Serial.print(name);
    }
    Serial.print(":     ");

    unsigned long cm = 0;

    for (uint8_t j = 0; j < count; j++) {
        cm = arr[j];
        if (convert_to_cm)
            cm /= US_ROUNDTRIP_CM;

        Serial.print(cm);
        Serial.print(", ");
    }

    Serial.println();
}


// #define OUTPUT_READABLE

template <typename T>
void DisplayStateVector(const T & arr, uint8_t first_count, uint8_t second_count,
    bool convert_to_cm = false, const char * name = NULL) 
{
    Serial.print(millis());
    if (name != NULL) {
        Serial.print(", ");
        Serial.print(name);
    }
    Serial.print(":     ");

    unsigned long cm = 0;

    for (uint8_t i = 0; i < first_count; i++) {
#ifdef OUTPUT_READABLE
        Serial.print("      s");
        Serial.print(i);
        Serial.print(": ");
#endif
        for (uint8_t j = 0; j < second_count; j++) {
            cm = arr[i * second_count + j];
            if (convert_to_cm)
                cm /= US_ROUNDTRIP_CM;

            Serial.print(cm);
            Serial.print(", ");
        }
    }

    Serial.println();
}


#endif
