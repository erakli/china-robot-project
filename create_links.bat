set lib_path="%USERPROFILE%\Documents\Arduino\libraries"
mklink /J "%lib_path%\ChinaRobotProject" "%cd%"

mklink /J "%lib_path%\NewPingMega" "%cd%\libraries\NewPingMega"
mklink /J "%lib_path%\FastUltrasonic" "%cd%\libraries\FastUltrasonic"

mklink /J "%lib_path%\I2Cdev" "%cd%\libraries\i2cdevlib\Arduino\I2Cdev"
mklink /J "%lib_path%\MPU6050" "%cd%\libraries\i2cdevlib\Arduino\MPU6050"
mklink /J "%lib_path%\HMC5883L" "%cd%\libraries\i2cdevlib\Arduino\HMC5883L"
mklink /J "%lib_path%\pid_v1" "%cd%\libraries\pid_v1"
mklink /J "%lib_path%\DirectIO" "%cd%\libraries\DirectIO"
mklink /J "%lib_path%\EnableInterrupt" "%cd%\libraries\EnableInterrupt"
mklink /J "%lib_path%\TaskScheduler" "%cd%\libraries\TaskScheduler"

mklink /J "%lib_path%\SensorsManager" "%cd%\Sensors\Ultrasonic\SensorsManager"
mklink /J "%lib_path%\Serialization" "%cd%\Communication\Serialization"
mklink /J "%lib_path%\DrivingModule" "%cd%\Driving\DrivingModule"

mklink /J "%lib_path%\Sensors" "%cd%\Sensors"

mklink /J "%lib_path%\UltrasonicManager" "%cd%\Scheduler\UltrasonicManager"
mklink /J "%lib_path%\CommunicationManager" "%cd%\Scheduler\CommunicationManager"
mklink /J "%lib_path%\ImuManager" "%cd%\Scheduler\ImuManager"
mklink /J "%lib_path%\DriveManager" "%cd%\Scheduler\DriveManager"