#ifndef DRIVINGMODULE_h
#define DRIVINGMODULE_h

#include "pins.h"
#include <Arduino.h>
#include "pid_v1.h"

void setCommand(char command, int V); // управление двигателями.

void CheckFunction(float yaw);

void InicializeDrivingModule();

void setDirection(char com,float yaw);

#endif
