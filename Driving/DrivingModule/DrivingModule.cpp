#include "DrivingModule.h"
//#define OUTPUT_PRINT
#define BIG_BOY
//#define BABY


double Setpoint_Local, Input, Output;
#ifdef BABY
double Kp = 18, Ki = 0, Kd = 0.5;
int Speed = 60;
#endif

#ifdef BIG_BOY
int Speed = 80;
double Kp = 25, Ki = 0, Kd = 0.5;
#endif
PID myPID(&Input, &Output, &Setpoint_Local, Kp, Ki, Kd, DIRECT);

char command_Local;
bool stopped;
bool flag; // для обнаружения команд вперед/назад.

void setDirection(char com,float yaw)
{
  
  switch (com) 
        {
            case '7':
                Setpoint_Local = yaw - 45.0;
                flag = false;
            break;

            case '9':
                Setpoint_Local = yaw + 45.0;
                flag = false;
            break;

            case '4':
                Setpoint_Local = yaw - 90.0;
                flag = false;
            break;

            case '6':
                Setpoint_Local = yaw + 90.0;
                flag = false;
            break;

            case '5':
                Setpoint_Local = yaw;
                flag = false;
                setCommand('S',0);
            break;
            
            case '8':
                setCommand('F',Speed);
                flag = true;
            break;

            case '2':
                setCommand('B',Speed);
                flag = true;
            break;
            

        }
}

void InicializeDrivingModule()
{
    Input = 0.0;
    Setpoint_Local = 0.0;

    myPID.SetOutputLimits(-255, 255);

    //turn the PID on
    myPID.SetMode(AUTOMATIC);


    stopped = false;
}

void CheckFunction(float yaw)
{
    if (Setpoint_Local >= 180.0)
    Setpoint_Local -= 360.0;
    else if (Setpoint_Local < -180.0)
    Setpoint_Local += 360.0;

    Input = yaw;
    myPID.Compute(true);

   if(!flag)
   {
    if(abs(yaw - Setpoint_Local) <= 3.0)
    {
        if (stopped == false) {
#ifdef OUTPUT_PRINT
            Serial11.print("setcommand_Local('S', Output)");
            Serial1.println();

            Serial1.println();
            Serial1.println();
#endif
            // TODO: commented
            setCommand('S', 0);
            stopped = true;
        }
    } 
    else
    {
        if (abs(Output) > 40) {

            command_Local = (Output < 0) ? 'L' : 'R';

#ifdef OUTPUT_PRINT
            Serial1.print("Output = ");
            Serial1.println(Output);
  
            Serial1.println();
            Serial1.println();
#endif

            // TODO: commented
            setCommand(command_Local, abs(Output));
            stopped = false;
        }
    }
   }
}


void setCommand(char command_Local, int V) // управление двигателями.
{
    analogWrite (PWM_RIGHT, LOW);
    analogWrite (PWM_LEFT, LOW); 

    delayMicroseconds(250);

    switch (command_Local) 
    {
        case 'B': //вперед             
            digitalWrite(DIR_RIGHT,HIGH);
            digitalWrite(DIR_LEFT,HIGH);
            #ifdef OUTPUT_PRINT
            Serial1.println("Back");
            #endif
        break;
      
        case 'F': //назад
            digitalWrite(DIR_RIGHT,LOW);
            digitalWrite(DIR_LEFT,LOW); 
            #ifdef OUTPUT_PRINT
            Serial1.println("Forward");
            #endif
        break;

        case 'L': // влево
            digitalWrite(DIR_RIGHT,HIGH);
            digitalWrite(DIR_LEFT,LOW); 
            #ifdef OUTPUT_PRINT
            Serial1.println("Left");
            #endif
        break;

        case 'R':// вправо
            digitalWrite(DIR_RIGHT,LOW);
            digitalWrite(DIR_LEFT,HIGH);
            #ifdef OUTPUT_PRINT
            Serial1.println("Right");
            #endif
        break;

        case 'S': // stop
          #ifdef OUTPUT_PRINT
          Serial1.println("Stop");
          #endif
        return;
        
    } // end switch

    if (    command_Local == 'F' ||
            command_Local == 'B' || 
            command_Local == 'L' || 
            command_Local == 'R')
    {
        analogWrite (PWM_RIGHT, V);
        analogWrite (PWM_LEFT, V);
    }
}
