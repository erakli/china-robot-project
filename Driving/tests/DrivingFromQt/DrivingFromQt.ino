// при питании DAGU и ARDUINO от разных источников не забываем
// объединять земли

int pwm2 = 9;
int pwm3 = 10;
int dir2 = 29; //правые (1 и 3)
int dir3 = 28; //левые (2 и 4)

int V = 150; //скорость вращения двигателей

//#define Serial  Serial1

void setCommand(char command, int V) // управление двигателями.
{
    analogWrite (pwm2, LOW);
    analogWrite (pwm3, LOW); 

    delayMicroseconds(250);

    switch (command) 
    {
        case '8': //вперед             
            digitalWrite(dir2,HIGH);
            digitalWrite(dir3,HIGH);
            Serial.println("Forward");
        break;
      
        case '2': //назад
            digitalWrite(dir2,LOW);
            digitalWrite(dir3,LOW); 
            Serial.println("Back");
        break;

        case '4': // влево
            digitalWrite(dir2,HIGH);
            digitalWrite(dir3,LOW); 
            Serial.println("Left");
        break;

        case '6':// вправо
            digitalWrite(dir2,LOW);
            digitalWrite(dir3,HIGH);
            Serial.println("Right");
        break;

        case '5': // stop
          Serial.println("Stop");
        return;
        
    } // end switch

    if (command == '8' || command == '2' || command == '4' || command == '6') {
        analogWrite (pwm2, V);
        analogWrite (pwm3, V);
    }
}


void setup() 
{
  Serial.begin(9600);
  pinMode(dir2,OUTPUT);
  pinMode(dir3,OUTPUT);
}

void loop() {

  if(Serial.available())
    {
      int value = Serial.read();
      setCommand(value, V);
    }
  
  
}
