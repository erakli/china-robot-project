int dir2 = 32; //правые (1 и 3)
int pwm2 = 2;
int dir3 = 34; //левые (2 и 4)
int pwm3 = 7;

int V = 100; //скорость вращения двигателей


void setCommand(int value)
{
  if(value == '8') //вперед
  {
     analogWrite (pwm2, LOW);
     analogWrite (pwm3, LOW);       
     delay(10);        
     digitalWrite(dir2,HIGH);
     digitalWrite(dir3,HIGH);
     analogWrite (pwm2, V);
     analogWrite (pwm3, V);
     Serial.println("Forward");
  }

  if(value == '2') //назад
  {
     analogWrite (pwm2, LOW);
     analogWrite (pwm3, LOW);
     delay(10);
     digitalWrite(dir2,LOW);
     digitalWrite(dir3,LOW);
     analogWrite (pwm2, V);
     analogWrite (pwm3, V);  
     Serial.println("Back");
  }

    if(value == '4') //влево 
  {
     analogWrite (pwm2, LOW);
     analogWrite (pwm3, LOW);        
     delay(10);
     digitalWrite(dir2,LOW);
     digitalWrite(dir3,HIGH);
     analogWrite (pwm2, V);
     analogWrite (pwm3, V);  
     Serial.println("Left");
  }

    if(value == '6')//вправо
  {
     analogWrite (pwm3, LOW);
     delay(10);
     digitalWrite(dir2,HIGH);
     digitalWrite(dir3,LOW); 
     analogWrite (pwm2, V);
     analogWrite (pwm3, V);
     Serial.println("Right");
  }

      if(value == '5')// стоп
  {
     analogWrite (pwm2, LOW);
     analogWrite (pwm3, LOW); 
     Serial.println("Stop");
  }
}


void setup() 
{
  Serial.begin(9600);
  pinMode(dir2,OUTPUT);
  pinMode(dir3,OUTPUT);
}

void loop() {

  if(Serial.available())
    {
      int value = Serial.read();
      setCommand(value);
    }
  
  
}
