#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QObject>
#include <QDebug>

QSerialPort *serial;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    serial = new QSerialPort(this);
    serialBuffer = "";

    bool serial_is_available = false;
    QString arduino_uno_port_name;

    serial_is_available = true;

    if(serial_is_available)
    {
        serial->setPortName("COM13");
        serial->setBaudRate(QSerialPort::Baud115200);
        serial->setDataBits(QSerialPort::Data8);
        serial->setParity(QSerialPort::NoParity);
        serial->setStopBits(QSerialPort::OneStop);
        serial->setFlowControl(QSerialPort::NoFlowControl);
        serial->open(QSerialPort::ReadWrite);

        QObject::connect(serial,SIGNAL(readyRead()),this,SLOT(readSerial()));
        QObject::connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_clicked()));
        QObject::connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_2_clicked()));
        QObject::connect(ui->pushButton_3,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_3_clicked()));
        QObject::connect(ui->pushButton_4,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_4_clicked()));
        QObject::connect(ui->pushButton_5,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_5_clicked()));
        QObject::connect(ui->pushButton_6,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_6_clicked()));
        QObject::connect(ui->pushButton_7,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_7_clicked()));
        QObject::connect(ui->pushButton_8,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_8_clicked()));
    }
    else
    {
        qDebug() << "Serial is not available";
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readSerial()
{
    serialData = serial->readAll();
    serialBuffer = QString::fromStdString(serialData.toStdString());
    qDebug() << serialBuffer;
//    ui->label->setText(serialBuffer);
//  //  ui->lineEdit->setText(temp);
}

void MainWindow::on_pushButton_clicked()
{
    serial->write("8");
}

void MainWindow::on_pushButton_2_clicked()
{
    serial->write("9");
}

void MainWindow::on_pushButton_3_clicked()
{
    serial->write("'7'");
}

void MainWindow::on_pushButton_4_clicked()
{
    serial->write("4");
}

void MainWindow::on_pushButton_5_clicked()
{
    serial->write("6");
}

void MainWindow::on_pushButton_6_clicked()
{
    serial->write("2");
}

void MainWindow::on_pushButton_7_clicked()
{
    serial->write("5");
}

void MainWindow::on_pushButton_8_clicked()
{
    serial->write("dfdsf");
}
