#include "TaskSchedulerComplete.h"

#include "ImuManager.h"


extern float yaw;



// Schedulers
// gyroManager is a higher priority scheduler
// taskManager is a base scheduler
Scheduler gyroManager;
Scheduler taskManager;

StatusRequest gyroDataReady;


// Callback methods prototypes
void CycleCallback();
void BaseCycleCallback();

Task tCycle(&CycleCallback);

Task tBaseCycle(100, TASK_FOREVER, &BaseCycleCallback);


void PrepareStatus() {
    gyroDataReady.setWaiting();
    tCycle.waitFor(&gyroDataReady);
}


void CycleCallback() {
    Serial.print(millis());
    Serial.print(", yaw = ");
    Serial.println(yaw);

    PrepareStatus();
}

void BaseCycleCallback() {
    Serial.print("> > > BaseCycleCallback, ");
    Serial.print(millis());
    Serial.print(", yaw = ");
    Serial.println(yaw);
}




// ----------------------- MAIN CODE --------------------------------

void setup() {
    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    PrepareStatus();

    // запускаем таски
    InitImuManager(&gyroManager, &gyroDataReady);

    gyroManager.addTask(tCycle);
    taskManager.addTask(tBaseCycle);

    taskManager.setHighPriorityScheduler(&gyroManager);

    tCycle.enable();
    tBaseCycle.enable();
}


void loop() {
    taskManager.execute();
}