#ifndef IMU_MANAGER_H
#define IMU_MANAGER_H 

#include <Arduino.h>

#include "TaskSchedulerComplete.h"


#define GYRO_OFFSET_X   201
#define GYRO_OFFSET_Y   -9
#define GYRO_OFFSET_Z   34

#define ACCEL_OFFSET_Z  1472

#define MPU_SETTING_TIME    15000

// после сигнализации события gyroDataReady, в этой переменной будет лежать 
// актуальное значение угла yaw
extern float yaw;

// чтобы добавить таск измерений к существующему планировщику, нужно вызвать этот метод
// второй аргумент - это событие, о котором мы можем сигнализировать
void InitImuManager(    Scheduler * taskManager, 
                        StatusRequest * gyroDataReady = NULL,
                        StatusRequest * mpuInitializedStatus = NULL);

#endif