#include "ImuManager.h"

// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif


#include <DirectIO.h>

#include "pins.h"
#include "debug.h"


#define MPU6050_FIFO_OFLOW_INT      0x10
#define MPU6050_DATA_RDY_INT        0x02

#define MPU6050_FIFO_SIZE           1024


#define MPU6050_DLPF_MODE           0
#define MPU6050_SAMPLE_RATE_MODE    0


// #define OUTPUT_READABLE_YAWPITCHROLL


// ================================================================
// ===                     MPU VARIABLES                        ===
// ================================================================

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
//MPU6050 mpu(0x69); // <-- use for AD0 high

Input<MPU_INTERRUPT_PIN> pInterrupt;

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer


// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector


// ================================================================
// ===          TASK SCHEDULER VARIABLES                        ===
// ================================================================

Scheduler * pGyroTaskManager;

StatusRequest * pGyroDataReady;
StatusRequest * pMpuInitializedStatus; // сообщаем во внешний мир, что мы выставились

StatusRequest dmpDataReadyStatus;

// интерфейсная переменная
float yaw = 0;


// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

void isrDmpDataReady() {
    dmpDataReadyStatus.signalComplete();
}




// ================================================================
// ===          FUNCTIONS PROTOTYPES                            ===
// ================================================================

// pre-defined functions
void MPU6050_Prepare();
bool MPU6050_CheckDataReady();
bool MPU6050_GetData();

void PrepareForDataReady();


// ================================================================
// ===          TASKS                                           ===
// ================================================================

// Callback methods prototypes
void InitDmpCallback();
void SettingImuCallback();
void DmpDataReadyCallback();


Task tGyro(TASK_IMMEDIATE, TASK_ONCE, &InitDmpCallback);



// ================================================================
// ===          CALLBACKS                                       ===
// ================================================================

unsigned long startTime;

void InitDmpCallback() {
    MPU6050_Prepare();

    // если в результате инициализации MPU всё норм, то подписываемся на событие
    // появления данных в буфере
    if (dmpReady) {
        startTime = millis();

        PrepareForDataReady();
        tGyro.setCallback(&SettingImuCallback);

    } else {
        // TODO: сигнализируем, что у нас DMP не работает
        Serial.println("dmpReady == false");
    }
}


// первое время просто будем считывать данные с БИНСа
// TODO: костыльненько, но пока будем делать по времени. можно по проверке выставки
void SettingImuCallback() { 
    if (MPU6050_CheckDataReady() == true) {
        MPU6050_GetData();
    }

    // если время выставки прошло, переключимся на основной callback
    if (millis() - startTime > MPU_SETTING_TIME) {

        // что-то должно было накопиться в буфере за время выставки, поэтому сохраним это  
        // значение перед отправкой сигнала
        yaw = degrees(ypr[0]);

        if (pMpuInitializedStatus != NULL) {
            // сообщим, что мы выставились
            pMpuInitializedStatus->signalComplete();
        }

        tGyro.setCallback(&DmpDataReadyCallback);
    }

    PrepareForDataReady();
}


// основной callback, который оповещает всех заинтересованных, что у нас новый угол
void DmpDataReadyCallback() {
    if (MPU6050_CheckDataReady() == true) {
        if (MPU6050_GetData() == true) {
            // у нас есть данные, сохраним их в интерфейсную переменную
            yaw = degrees(ypr[0]);

            if (pGyroDataReady != NULL) {
                pGyroDataReady->signalComplete();
            }
        }
    }

    PrepareForDataReady();
}



// ================================================================
// ===               FUNCTIONS                                  ===
// ================================================================

void PrepareForDataReady() {
    dmpDataReadyStatus.setWaiting();
    tGyro.waitFor(&dmpDataReadyStatus);
}



void MPU6050_Prepare() {
    // initialize device
    DPRINTLN(F("Initializing I2C devices..."));
    mpu.initialize();

    // меняем Sample Rate
    mpu.setDLPFMode(MPU6050_DLPF_MODE);
#ifdef DEBUG
    uint8_t mode = mpu.getDLPFMode();
    DPRINT(F("mode = "));
    DPRINTLN(mode);
#endif

    mpu.setRate(MPU6050_SAMPLE_RATE_MODE);
#ifdef DEBUG
    uint8_t rate = mpu.getRate();
    DPRINT(F("rate = "));
    DPRINTLN(rate);
#endif

    // verify connection
    DPRINTLN(F("Testing device connections..."));
    bool connectionSuccessful = mpu.testConnection();
    DPRINTLN(
        connectionSuccessful ? 
        F("MPU6050 connection successful") : 
        F("MPU6050 connection failed")
    );

    // wait for ready
    DPRINTLN(F("\nSend any character to begin DMP programming and demo: "));
#ifdef DEBUG
    while (Serial.available() && Serial.read()); // empty buffer
    while (!Serial.available());                 // wait for data
    while (Serial.available() && Serial.read()); // empty buffer again
#endif

    // load and configure the DMP
    DPRINTLN(F("Initializing DMP..."));
    uint8_t devStatus = mpu.dmpInitialize();      // return status after each device operation (0 = success, !0 = error)

    // supply your own gyro offsets here, scaled for min sensitivity
    mpu.setXGyroOffset(GYRO_OFFSET_X);
    mpu.setYGyroOffset(GYRO_OFFSET_Y);
    mpu.setZGyroOffset(GYRO_OFFSET_Z);
    mpu.setZAccelOffset(ACCEL_OFFSET_Z); // 1688 factory default for my test chip

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        DPRINTLN(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        DPRINTLN(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
        attachInterrupt(digitalPinToInterrupt(MPU_INTERRUPT_PIN), isrDmpDataReady, RISING);

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        DPRINTLN(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
    }
}



bool MPU6050_CheckDataReady() {
    return mpu.dmpPacketAvailable();
}



bool MPU6050_GetData() {
    DPRINT("GetData, ");
    DPRINTLN(millis());

    // get INT_STATUS byte
    mpuIntStatus = mpu.getIntStatus();

    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & MPU6050_FIFO_OFLOW_INT) || fifoCount == MPU6050_FIFO_SIZE) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        Serial.println(F("FIFO overflow!"));
        return false;

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & MPU6050_DATA_RDY_INT) {
        // DPRINTLN("DMP data ready!");

        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) {
            // DPRINTLN("fifoCount < packetSize");
            fifoCount = mpu.getFIFOCount();
        }

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;

        // собственно преобразование данных в нужный формат
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

        return true;
    }

    return false;
}








// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

void InitImuManager(Scheduler * taskManager, StatusRequest * gyroDataReady, StatusRequest * mpuInitializedStatus) {
    pGyroTaskManager = taskManager;
    pGyroDataReady = gyroDataReady;
    pMpuInitializedStatus = mpuInitializedStatus;

    Wire.begin();
    Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties

    pGyroTaskManager->addTask(tGyro);

    tGyro.enable();
}