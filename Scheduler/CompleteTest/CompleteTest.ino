#include "TaskSchedulerComplete.h"

#include "DriveManager.h"
#include "ImuManager.h"
#include "UltrasonicTimerManager.h"
#include "CommunicationManager.h"

#include "ArrayFunctions.h"


extern float yaw;
extern uint16_t stateVector[STATE_VECTOR_SIZE]; // из UltrasonicTimerManager


#define STATE_VECTOR_READY_EVENT
#define SHOW_YAW


// Schedulers
// gyroManager is a higher priority scheduler
// taskManager is a base scheduler
Scheduler gyroManager;
Scheduler taskManager;

extern StatusRequest newDirectionReceivedStatus;
extern StatusRequest mpuInitializedStatus; 

#ifdef STATE_VECTOR_READY_EVENT
extern StatusRequest stateVectorReady;

// Callback methods prototypes
void UltrasonicCycleCallback();

Task tUltrasonicCycle(&UltrasonicCycleCallback);


void PrepareUltrasonicCycleStatus() {
    tUltrasonicCycle.waitFor(&stateVectorReady);
}


void UltrasonicCycleCallback() {
    DisplayStateVector(stateVector, SENSORS_NUM, SENSORS_NUM, true);
    PrepareUltrasonicCycleStatus();
}
#endif  // STATE_VECTOR_READY_EVENT



#ifdef SHOW_YAW
StatusRequest gyroDataReady;

// Callback methods prototypes
void GyroCycleCallback();

Task tGyroCycle(&GyroCycleCallback);



void PrepareGyroDataStatus() {
    gyroDataReady.setWaiting();
    tGyroCycle.waitFor(&gyroDataReady);
}


void GyroCycleCallback() {
    Serial.print(millis());
    Serial.print(", yaw = ");
    Serial.println(yaw);

    PrepareGyroDataStatus();
}
#endif // SHOW_YAW


// ----------------------- MAIN CODE --------------------------------

void setup() {
    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");

    Serial.print("Wait ");
    Serial.print(MPU_SETTING_TIME / 1000);
    Serial.println(" sec before IMU gets working");

#ifdef STATE_VECTOR_READY_EVENT
    PrepareUltrasonicCycleStatus();
#endif

#ifdef SHOW_YAW
    PrepareGyroDataStatus();

    InitImuManager(&gyroManager, &gyroDataReady, &mpuInitializedStatus);
#else
    InitImuManager(&gyroManager, NULL, &mpuInitializedStatus);
#endif
    InitDriveManager(&gyroManager);
    InitUltrasonicTimerManager(&taskManager, &stateVectorReady);
    InitCommunicationManager(&taskManager, &newDirectionReceivedStatus);

    taskManager.setHighPriorityScheduler(&gyroManager);

#ifdef SHOW_YAW
    gyroManager.addTask(tGyroCycle);
    tGyroCycle.enable();
#endif


#ifdef STATE_VECTOR_READY_EVENT
    taskManager.addTask(tUltrasonicCycle);
    tUltrasonicCycle.enable();
#endif
}


void loop() {
    taskManager.execute();
}