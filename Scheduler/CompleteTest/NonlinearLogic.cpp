#include "NonlinearLogic.h"

#include <Arduino.h>

#include "UltrasonicTimerManager.h"

extern uint16_t stateVector[STATE_VECTOR_SIZE]; // из UltrasonicTimerManager

#define ZERO_DELTA      0.001

// #define DEBUG

// enum Directions {
//     Direction_Stop,
//     Direction_North,        // прямо
//     Direction_NorthEast,    // по диагонали направо
//     Direction_NorthWest,    // по диагонали налево
//     Direction_East,         // направо
//     Direction_West,         // налево
//     Direction_South,        // назад

//     Direction_East15,       // направо на 15 градусов
//     Direction_West15,       // налево на 15 градусов
//     Direction_RotateRight,  // вращаться направо
//     Direction_RotateLeft    // вращаться налево
// };

//****************************************************************************************
float Min4Alpha(float x1, float x2, float x3, float x4)
{
    float minx, minx1, minx2;
    minx1 = min(x1, x2);
    minx2 = min(x3, minx1);
    return (min(x4, minx2));
}
//****************************************************************************************
float Min3Alpha(float x1, float x2, float x3)
{
    float minx, minx1;
    minx1 = min(x1, x2);
    return (min(x3, minx1));
}
//****************************************************************************************
Directions FuzzyLogicDirection()
{
#ifdef DEBUG
    Serial.println("FuzzyLogicDirection()");
#endif

    // для управления можно использовать функции:
    // void SetDirection(Directions newDirection, float currentYaw);

    //*****************************************************************************************

    float x, y, a, b, c, d, e;
    float AlphaD1N, AlphaD1P, AlphaD2N, AlphaD2P, AlphaD3N, AlphaD3P, AlphaD4N, AlphaD4P;
    float MinAlpha, mMinAlpha;
    float Alpha1, Alpha2, Alpha3, Alpha4, mAlpha1, mAlpha2, mAlpha3, mAlpha4;
    float MR, RR, SR;
    float RC0, RC1, RC2, FR, FL, FF, FB;
    boolean FORWARD, BACK, RIGHT, LEFT;
    boolean F, B, R, L;
    // float D2new,D2old,D4new,D4old;

    FORWARD = false;
    BACK = false;
    RIGHT = false;
    LEFT = false;

    float aD1N = -0.05;
    float bD1N = 75;
    float aD1P = 0.05;
    float bD1P = 75;

    float aD2N = -0.05;
    float bD2N = 75;
    float aD2P = 0.05;
    float bD2P = 75;

    float aD3N = -0.05;
    float bD3N = 75;
    float aD3P = 0.05;
    float bD3P = 75;

    float aD4N = -0.05;
    float bD4N = 75;
    float aD4P = 0.05;
    float bD4P = 75;

    float c1 = 1;
    float c2 = -1;
    float c3 = 0.75;
    float c4 = -0.75;

    float mAlphaD1N, mAlphaD1P, mAlphaD2N, mAlphaD2P, mAlphaD3N, mAlphaD3P, mAlphaD4N, mAlphaD4P;

    float maD1N = -0.05;
    float mbD1N = 75;
    float maD1P = 0.05;
    float mbD1P = 75;

    float maD2N = -0.05;
    float mbD2N = 75;
    float maD2P = 0.05;
    float mbD2P = 75;

    float maD3N = -0.05;
    float mbD3N = 75;
    float maD3P = 0.05;
    float mbD3P = 75;

    float maD4N = -0.05;
    float mbD4N = 75;
    float maD4P = 0.05;
    float mbD4P = 75;

    float mc1 = 1;
    float mc2 = -1;
    float mc3 = 0.5;
    float mc4 = -0.5;

    uint16_t distance_forward = stateVector[0] / 57;
    uint16_t distance_right = stateVector[5] / 57;
    uint16_t distance_back = stateVector[10] / 57;
    uint16_t distance_left = stateVector[15] / 57;

#ifdef DEBUG
    Serial.print("distance_right = ");
    Serial.println(distance_right);
    Serial.print("distance_left = ");
    Serial.println(distance_left);
    Serial.print("distance_forward = ");
    Serial.println(distance_forward);
    Serial.print("distance_back = ");
    Serial.println(distance_back);
#endif

    //**********************************************
    //   D2new = distance_right;
    //   D4new = distance_left;

    //********************************************
    //    x=1;
    //    a=1;
    //    b=0.5;
    //    c=1.2;
    //    d=0.3;
    //    y= 1/(1+exp(a*(x-b)));
    //    x=min(a,b);

    //*******************************************
    // FUZZY ROTATION CONTROL
    //*************************************************
    AlphaD1N = 1 / (1 + exp(-aD1N * (distance_forward - bD1N)));
    AlphaD1P = 1 / (1 + exp(-aD1P * (distance_forward - bD1P)));

    AlphaD2N = 1 / (1 + exp(-aD2N * (distance_right - bD2N)));
    AlphaD2P = 1 / (1 + exp(-aD2P * (distance_right - bD2P)));

    //   AlphaD3N=1/(1+exp(-aD3N*(distance_back-bD3N)));
    //   AlphaD3P=1/(1+exp(-aD3P*(distance_back-bD3P)));

    AlphaD4N = 1 / (1 + exp(-aD4N * (distance_left - bD4N)));
    AlphaD4P = 1 / (1 + exp(-aD4P * (distance_left - bD4P)));

    Alpha1 = Min3Alpha(AlphaD1N, AlphaD2P, AlphaD4N);
    Alpha2 = Min3Alpha(AlphaD1N, AlphaD2N, AlphaD4P);
    Alpha3 = Min3Alpha(AlphaD1P, AlphaD2P, AlphaD4N);
    Alpha4 = Min3Alpha(AlphaD1P, AlphaD2N, AlphaD4P);

    if (abs(Alpha1 + Alpha2 + Alpha4) > ZERO_DELTA)
        RR = (Alpha1 * c1 + Alpha2 * c2 + Alpha4 * c4) / (Alpha1 + Alpha2 + Alpha4);
    else
        RR = 1.0;

    //*****************************************************************************************
    // FUZZY MOTION CONTROL
    //*************************************************

    mAlphaD1N = 1 / (1 + exp(-maD1N * (distance_forward - mbD1N)));
    mAlphaD1P = 1 / (1 + exp(-maD1P * (distance_forward - mbD1P)));

    mAlphaD2N = 1 / (1 + exp(-maD2N * (distance_right - mbD2N)));
    mAlphaD2P = 1 / (1 + exp(-maD2P * (distance_right - mbD2P)));

    mAlphaD3N = 1 / (1 + exp(-maD3N * (distance_back - mbD3N)));
    mAlphaD3P = 1 / (1 + exp(-maD3P * (distance_back - mbD3P)));

    mAlphaD4N = 1 / (1 + exp(-maD4N * (distance_left - mbD4N)));
    mAlphaD4P = 1 / (1 + exp(-maD4P * (distance_left - mbD4P)));

    mAlpha1 = Min4Alpha(mAlphaD1P, mAlphaD2N, mAlphaD3N, mAlphaD4N);
    mAlpha2 = Min4Alpha(mAlphaD1N, mAlphaD2N, mAlphaD3P, mAlphaD4N);
    mAlpha3 = Min4Alpha(mAlphaD1P, mAlphaD2P, mAlphaD3N, mAlphaD4P);
    mAlpha4 = Min4Alpha(mAlphaD1N, mAlphaD2P, mAlphaD3P, mAlphaD4P);

    if (abs(mAlpha1 + mAlpha2 + mAlpha3 + mAlpha4) > ZERO_DELTA)
        MR = (mAlpha1 * mc1 + mAlpha2 * mc2 + mAlpha3 * mc3 + mAlpha4 * mc4) / (mAlpha1 + mAlpha2 + mAlpha3 + mAlpha4);
    else
        MR = 1.0;

    //************************************************************************************************
#ifdef DEBUG
    Serial.print("RR = ");
    Serial.println(RR);
    Serial.print("MR = ");
    Serial.println(MR);
#endif

    if ((abs(RR) > abs(MR)) && (RR > 0))
        R = true;
    else
        R = false;

    // if ((R = true) && (RC1 = 1) && (RC0 = -1))
    // {
    //     FR = false;
    //     RR = 0;
    // }
    // else
    //     FR = true;

    // if ((R = true) && (FR = true))
    // {
    //     RIGHT = true;
    //     RC2 = RC1;
    //     RC1 = RC0;
    //     RC0 = 1;
    // }
    // else
    //     RIGHT = false;



    if ((abs(RR) > abs(MR)) && (RR < 0))
        L = true;
    else
        L = false;

    // if ((L = true) && (RC1 = -1) && (RC0 = 1))
    // {
    //     FL = false;
    //     RR = 0;
    // }
    // else
    //     FL = true;

    // if ((L = true) && (FL = true))
    // {
    //     LEFT = true;
    //     RC2 = RC1;
    //     RC1 = RC0;
    //     RC0 = -1;
    // }
    // else
    //     LEFT = false;



    if ((abs(RR) < abs(MR)) && (MR > 0))
        F = true;
    else
        F = false;

    // if ((F = true) && (RC1 = 2) && (RC0 = -2))
    // {
    //     FF = false;
    //     MR = 0;
    // }
    // else
    //     FF = true;

    // if ((F = true) && (FF = true))
    // {
    //     FORWARD = true;
    //     RC2 = RC1;
    //     RC1 = RC0;
    //     RC0 = 2;
    // }
    // else
    //     FORWARD = false;



    if ((abs(RR) < abs(MR)) && (MR < 0))
        B = true;
    else
        B = false;

    // if ((B = true) && (RC1 = -2) && (RC0 = 2))
    // {
    //     FB = false;
    //     MR = 0;
    // }
    // else
    //     FB = true;

    // if ((B = true) && (FB = true))
    // {
    //     BACK = true;
    //     RC2 = RC1;
    //     RC1 = RC0;
    //     RC0 = -2;
    // }
    // else
    //     BACK = false;


    // TODO: временно
    RIGHT = R;
    LEFT = L;
    FORWARD = F;
    BACK = B;

#ifdef DEBUG
    // if (RIGHT)
    //     Serial.println("RIGHT");
    // if (LEFT)
    //     Serial.println("LEFT");
    // if (FORWARD)
    //     Serial.println("FORWARD");
    // if (BACK)
    //     Serial.println("BACK");
#endif


    //***************************************************************************************************

    // ВЫБОР НАПРАВЛЕНИЯ

    if (RIGHT) {
        // return Direction_RotateRight;    // вращаться до новой команды

        return Direction_East; // поворот на 90 градусов
    }
 
    if (LEFT) {
        // return Direction_RotateLeft;    // вращаться до новой команды

        return Direction_West; // поворот на 90 градусов
    }

    if ((FORWARD) && (distance_right < 5)) {
        return Direction_West15;
    }

    if ((FORWARD) && (distance_left < 5)) {
        return Direction_East15;
    }

    if ((BACK) && (distance_right < 5)) {
        return Direction_East15;
    }

    if ((BACK) && (distance_left < 5)) {
        return Direction_West15;
    }

    if (FORWARD) { 
       return Direction_North;
    }

    if (BACK) {
        return Direction_South;
    }
    //***********************************************************************************************

    return Direction_Stop;
}