#include "CommunicationManager.h"

// в этом заголовнике определён таск на измерения с УЗД и 
// вся обрамляющая оплётка
#include "UltrasonicTimerManager.h"   // TODO: закомметили в виду нерабочего состояния UltrasonicManager


// ВНИМАНИЕ: чтобы посылалось и принималось одинаковое число компонент вектора состояния,
// в StateVectorMessage должен быть объявлен тот же размер STATE_VECTOR_SIZE, что и в UltrasonicManager
#include "StateVectorMessage.h"

#include "ArrayFunctions.h"


// #define DISABLE_SEND
// #define DEBUG
// #define CALLBACK_DEBUG


Scheduler * pCommunicationTaskManager;
StatusRequest * pCommandReceived;

// событие, на которое мы подписываемся. если получаем сигнал о зввершении этого 
// события, отсылаем вектор состояния на компьютер
StatusRequest stateVectorReady;

// в этой переменной храним последнюю полученную команду. из неё можно будет
// забрать комманду снаружи
Command lastCommand;


// этот массив мы забираем из UltrasonicTimerManager.h чтобы по сигналу от UltrasonicTimerManager
// переправлять на компьютер
extern uint16_t stateVector[STATE_VECTOR_SIZE];

#define BUFFER_SIZE 256

StateVectorMessage message;
uint8_t buffer[BUFFER_SIZE];
uint8_t messageSize;


// ----------------------- TASKS --------------------------------

// Callback methods prototypes
void SendDataCallback(); void SendDataOnDisable();
void ReceiveDataCallback();

Task tSendData(&SendDataCallback, NULL, NULL, &SendDataOnDisable);
Task tReceiveData(RECEIVE_DATA_INTERVAL, TASK_FOREVER, &ReceiveDataCallback);



// ----------------------- FUNCTIONS --------------------------------

// подготовим tSendData к ожиданию сигнала о готовности вектора состояния
void PrepareForStateVectorReadySignal() {
    stateVectorReady.setWaiting();
    tSendData.waitFor(&stateVectorReady);
}


// подготавливаем сообщение к отправке и записываем его в Serial
void SendUltrasonic() {
    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        // TODO: закомметили в виду нерабочего состояния UltrasonicManager
        message.ultrasonicTimes[i] = stateVector[i];
    }

    messageSize = packStateVectorMessage(buffer, message);

    Serial.write(buffer, messageSize);
}


// ----------------------- CALLBACKS --------------------------------

void SendDataCallback() {
#ifdef CALLBACK_DEBUG
    Serial.print(millis());
    Serial.println(", new SendDataCallback ************************************");
#endif

#ifdef DEBUG
    DisplayStateVector(stateVector, SENSORS_NUM, SENSORS_NUM, true);
#elif !defined(DISABLE_SEND)
    SendUltrasonic();
#endif
}

void SendDataOnDisable() {
    PrepareForStateVectorReadySignal();
}



void ReceiveDataCallback() {
    if (Serial.available() > 0) {

        // сохраняем последнюю полученную команду
        lastCommand = Serial.read();

#ifdef CALLBACK_DEBUG
        Serial.print(millis());
        Serial.println(", ReceiveDataCallback");

        Serial.print("Received: ");
        Serial.write(lastCommand);
        Serial.println();
#endif

        // если нам передали StatusRequest, на получение команды, то будем оповещать,
        // что команда получена
        if (pCommandReceived != NULL) {
            pCommandReceived->signalComplete();
        }

        // проверим, чтобы больше не осталось непринятых команд
        // TODO: нужно ли это делать?
        tReceiveData.forceNextIteration();
    }
}



// ----------------------- INTERFACE FUNCTION --------------------------------

void InitCommunicationManager(Scheduler * taskManager, StatusRequest * commandReceived) {
    pCommunicationTaskManager = taskManager;
    pCommandReceived = commandReceived;

    message.yaw = 0;

    pCommunicationTaskManager->addTask(tSendData);
    pCommunicationTaskManager->addTask(tReceiveData);

    // подготовим tSendData к ожиданию сигнала о готовности вектора состояния
    PrepareForStateVectorReadySignal();

    // TODO: на самом деле не нужно, так как waitFor делает enable
    // tSendData.enable();
    tReceiveData.enable();
}