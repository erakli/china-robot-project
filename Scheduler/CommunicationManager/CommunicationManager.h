#ifndef COMMUNICATION_MANAGER_H
#define COMMUNICATION_MANAGER_H 

#include <Arduino.h>

#include "TaskSchedulerComplete.h"

// период, с которым мы будем проверять наличие команды в Serial
#define RECEIVE_DATA_INTERVAL	100


typedef int Command;

// в этой переменной храним последнюю полученную команду. из неё можно будет
// забрать команду снаружи
extern Command lastCommand;

// событие, на которое мы подписываемся. если получаем сигнал о зввершении этого 
// события, отсылаем вектор состояния на компьютер. 
extern StatusRequest stateVectorReady;

// на всякий случай, таски этого модуля, чтобы ими можно было управлять
extern Task tSendData;
extern Task tReceiveData;

// чтобы добавить таск измерений к существующему планировщику, нужно вызвать этот метод
// второй аргумент - это событие, о котором мы можем сигнализировать
void InitCommunicationManager(Scheduler * taskManager, StatusRequest * commandReceived = NULL);

#endif