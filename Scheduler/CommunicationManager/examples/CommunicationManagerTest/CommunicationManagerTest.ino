// основан на CommunicationTest.ino

#include "TaskSchedulerComplete.h"

#include "UltrasonicManager.h"
#include "CommunicationManager.h"




// Callback methods prototypes
void CycleCallback();

// taskManager is a base scheduler
Scheduler taskManager;


Task tCycle(500, TASK_FOREVER, &CycleCallback);


void CycleCallback() {
    Serial.println();
    Serial.println();
    Serial.println();

    Serial.print(millis());
    Serial.println(", new cycle ************************************");

    Serial.println();
    Serial.println();
    Serial.println();
}




// ----------------------- MAIN CODE --------------------------------

void setup() {
    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    // запускаем таски
    InitUltrasonicManager(&taskManager, &stateVectorReady);
    InitCommunicationManager(&taskManager, NULL);

    taskManager.addTask(tCycle);
    tCycle.enable();
}


void loop() {
    taskManager.execute();
}