#include "TaskSchedulerComplete.h"

// в этом заголовнике определён таск на измерения с УЗД и 
// вся обрамляющая оплётка
#include "UltrasonicManager.h"


// ВНИМАНИЕ: чтобы посылалось и принималось одинаковое число компонент вектора состояния,
// в StateVectorMessage должен быть объявлен тот же размер STATE_VECTOR_SIZE, что и в UltrasonicManager
#include "StateVectorMessage.h"


// ----------------------- TASKS --------------------------------

// Callback methods prototypes
void SendDataCallback(); void SendDataOnDisable();
void ReceiveDataCallback();


// taskManager is a base scheduler
Scheduler taskManager;
StatusRequest stateVectorReady;

Task tSendData(&SendDataCallback, NULL, NULL, &SendDataOnDisable);
Task tReceiveData(100, TASK_FOREVER, &ReceiveDataCallback);



// ----------------------- FUNCTIONS --------------------------------

void PrepareStatus() {
    stateVectorReady.setWaiting();
    tSendData.waitFor(&stateVectorReady);
}


// этот массив мы забираем из UltrasonicManager.h
extern uint16_t stateVector[STATE_VECTOR_SIZE];

#define BUFFER_SIZE 66

StateVectorMessage message;
uint8_t buffer[BUFFER_SIZE];
uint8_t messageSize;

void SendUltrasonic() {
    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        message.ultrasonicTimes[i] = stateVector[i];
    }

    messageSize = packStateVectorMessage(buffer, message);

    Serial.write(buffer, messageSize);
}


// ----------------------- CALLBACKS --------------------------------

void SendDataCallback() {
    // Serial.println();
    // Serial.println();
    // Serial.println();

    // Serial.print(millis());
    // Serial.println(", new SendDataCallback ************************************");

    // SendUltrasonic();
    Serial.println();
    DisplayStateVector();

    // Serial.println();
    // Serial.println();
    // Serial.println();
}

void SendDataOnDisable() {
    PrepareStatus();
}



void ReceiveDataCallback() {
    if (Serial.available() > 0) {
        Serial.print(millis());
        Serial.println(", ReceiveDataCallback");

        Serial.print("Received: ");
        Serial.write(Serial.read());
        Serial.println();

        // проверим, чтобы больше не осталось непринятых команд
        tReceiveData.forceNextIteration();
    }
}




// ----------------------- MAIN CODE --------------------------------

void setup() {
    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    message.yaw = 0;

    // подготовим tSendData к ожиданию сигнала о готовности вектора состояния
    PrepareStatus();

    // запускаем таск опроса датчиков
    InitUltrasonicManager(&taskManager, &stateVectorReady);

    taskManager.addTask(tSendData);
    taskManager.addTask(tReceiveData);

    tSendData.enable();
    tReceiveData.enable();
}


void loop() {
    taskManager.execute();
}