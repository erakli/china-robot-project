#ifndef SONARS_MANAGER_H
#define SONARS_MANAGER_H 

// Основан на SensorsManager из Sensors/Ultrasonic

#include <Arduino.h>    // uint8_t, uint16_t, delay() и тд.

#include "Sensor.h"
#include "Relay.h"

#include "pins.h"


#define MAX_DISTANCE            500   // максимальная дистанция в см, которую будут обнаруживать датчики

#define PING_INTERVAL			50    // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define RELAY_INTERVAL          10    // mS

#define CONSIDER_START_DELAY    false  // учитывать ли задержку между стартом датчиков

#define PENDING_WAIT_START      false  // новая реализация старта пинга


class SonarsManager {
    public:
        SonarsManager();
        SonarsManager(Sensor *sensors, Relay *relays);

        void SetArrays(Sensor *sensors, Relay *relays);

        void MeasurementCycle();
        void SetOpenedRelay(uint8_t openedRelay);
        void SetActiveSensor(uint8_t activeSensor);

        const uint16_t * GetStateVector() const;

    public:
        void AssignTimers();
        void ActiveSensorCycle();

		bool SensorsSimultaneousPing();

        void SaveMeasurements();
        void CopyDistancesToStateVector();

        void ClearActiveSensorDistances();
        void ClearStateVector();

    private:
        // массивы
        Sensor *m_sensors;
        Relay *m_relays;

        uint8_t m_activeSensor;

        // дистанции текущего сенсора и отклики с остальных сенсоров
        uint16_t m_activeSensorDistances[SENSORS_NUM];
        uint16_t m_stateVector[STATE_VECTOR_SIZE];
};

#endif