#include "Sensor.h"

#define TRIGGER_PIN 30   // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN 31      // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 500 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

Sensor sonar(NewPingMega(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE));

unsigned int pingSpeed = 50; // How frequently are we going to send out a ping (in milliseconds). 50ms would be 20 times a second.
unsigned long pingTimer;     // Holds the next ping time.

void setup()
{
    sonar.assign_timer(Timer1);

    Serial.begin(115200);

    pinMode(38, OUTPUT);
    digitalWrite(38, LOW);

    pingTimer = millis(); // Start now.
}

// volatile bool received = false;

void loop()
{
    if (millis() >= pingTimer)
    { // pingSpeed milliseconds since last ping, do another ping.
        if (sonar.PingRecieved()) {
        // if (received) {
            Serial.print("Ping: ");
            Serial.print(sonar.ping_result / US_ROUNDTRIP_CM); // Ping returned, uS result in ping_result, convert to cm with US_ROUNDTRIP_CM.
            Serial.println("cm");
        } else {
            Serial.print("Ping: ");
            Serial.print(0); // Ping returned, uS result in ping_result, convert to cm with US_ROUNDTRIP_CM.
            Serial.println("cm");
        }

        pingTimer += pingSpeed; // Set the next ping time.
        if (sonar.activate_trigger() == true) {
            sonar.wait_ping_start();
            sonar.start_ping_timer(echoCheck);
        }

        // prevPing = millis();

        // sonar.ClearPingRecieved();

        // pingTimer += pingSpeed; // Set the next ping time.
        // sonar.ping_timer(echoCheck);
    }
}

void echoCheck()
{
    sonar.CheckEcho();
    // if (sonar.PingRecieved())
    // { // This is how you check to see if the ping was received.
    //     // Here's where you can add code.
    //     Serial.print("Ping: ");
    //     Serial.print(sonar.ping_result / US_ROUNDTRIP_CM); // Ping returned, uS result in ping_result, convert to cm with US_ROUNDTRIP_CM.
    //     Serial.println("cm");
    // }

    // if (sonar.check_timer()) {
    //     received = true;
    // }

    // if (sonar.check_timer())
    // { // This is how you check to see if the ping was received.
    //     // Here's where you can add code.
    //     Serial.print("Ping: ");
    //     Serial.print(sonar.ping_result / US_ROUNDTRIP_CM); // Ping returned, uS result in ping_result, convert to cm with US_ROUNDTRIP_CM.
    //     Serial.println("cm");
    // }
}
