// Тест UltrasonicTimerManager. Является продолжением теста своего предшественника

#include "TaskSchedulerComplete.h" 

#include <DirectIO.h>

// в этом заголовнике определён таск на измерения с УЗД и вся обрамляющая оплётка
#include "UltrasonicTimerManager.h"

#include "ArrayFunctions.h"


extern uint16_t stateVector[STATE_VECTOR_SIZE]; // из UltrasonicTimerManager



// taskManager is a base scheduler
Scheduler taskManager;
StatusRequest stateVectorReady;

// Callback methods prototypes
void CycleCallback();

Task tCycle(&CycleCallback);


void PrepareCycleStatus() {
    stateVectorReady.setWaiting();
    tCycle.waitFor(&stateVectorReady);
}


void CycleCallback() {
    // Serial.println();
    // Serial.println();
    // Serial.println();

    // Serial.print(millis());
    // Serial.println(", new cycle ************************************");

    DisplayStateVector(stateVector, SENSORS_NUM, SENSORS_NUM, true);
    PrepareCycleStatus();

    // Serial.println();
    // Serial.println();
    // Serial.println();
}




void setup() {
    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    InitUltrasonicTimerManager(&taskManager, &stateVectorReady);

    taskManager.addTask(tCycle);
    tCycle.enable();
}


void loop() {
    taskManager.execute();
}