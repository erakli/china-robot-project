#include "SonarsManager.h"

#include "ArrayFunctions.h"

#define BAUDRATE	115200

#define OUTPUT_READABLE

// всю магию будет творить вот этот вот красавчик
// перед использованием надо заложить в него массивы датчиков и реле
SonarsManager testSonarsManager;

uint16_t * sonarsStateVector;

void setup() {
    // Open serial monitor to see ping results.
    Serial.begin(BAUDRATE);
    Serial.println("Initialized.");
    Serial.println();
    Serial.println();
}

void loop() {
    testSonarsManager.MeasurementCycle();
    sonarsStateVector = testSonarsManager.GetStateVector();

    DisplayStateVector(sonarsStateVector, SENSORS_NUM, SENSORS_NUM, true);

    // delay(500);
    // Serial.println();
    // Serial.println();
}