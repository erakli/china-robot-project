// Тест UltrasonicManager. Является продолжением тестового скетча
// EventDrivenStatusMany3.ino


#include "TaskSchedulerComplete.h" 

#include <DirectIO.h>

// в этом заголовнике определён таск на измерения с УЗД и 
// вся обрамляющая оплётка
#include "UltrasonicManager.h"


// Callback methods prototypes
void CycleCallback();


// taskManager is a base scheduler
Scheduler taskManager;

Task tCycle(500, TASK_FOREVER, &CycleCallback);


void CycleCallback() {
    Serial.println();
    Serial.println();
    Serial.println();

    Serial.print(millis());
    Serial.println(", new cycle ************************************");

    DisplayStateVector();

    Serial.println();
    Serial.println();
    Serial.println();
}




void setup() {
    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    InitUltrasonicManager(&taskManager);

    taskManager.addTask(tCycle);
    tCycle.enable();
}


void loop() {
    taskManager.execute();
}