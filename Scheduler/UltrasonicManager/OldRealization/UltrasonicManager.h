#ifndef ULTRASONIC_MANAGER_H
#define ULTRASONIC_MANAGER_H 

#include <Arduino.h>

#include "TaskSchedulerComplete.h"

#include "pins.h"

#define STATE_VECTOR_SIZE		SENSORS_NUM * SENSORS_NUM


#define PING_INTERVAL	50     // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define RELAY_INTERVAL  10     // mS
#define MILLIS_TIMEOUT  30     // 30 milliseconds or max range of ~5.1 meters


// наш интерфейс с внешним миром, сюда кладём готовые векторы состояния, 
// отсюда их можно брать, когда нужно
extern uint16_t stateVector[STATE_VECTOR_SIZE]; // ВНИМАНИЕ: поменяли с uint32_t на uint16_t

// на всякий случай, таск измерений с УЗД, чтобы им можно было 
// управлять
extern Task tMeasure;

// чтобы добавить таск измерений к существующему планировщику, нужно вызвать этот метод
// второй аргумент - это событие, о котором мы можем сигнализировать
void InitUltrasonicManager(Scheduler * taskManager, StatusRequest * stateVectorReady = NULL);

// отображает последний готовый вектор состояния в Serial
void DisplayStateVector();

#endif