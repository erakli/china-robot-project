#include "UltrasonicManager.h"

#include "FastUltrasonic.h"
#include "Relay.h"


// вычисление циклического индекса
#define CYCLE_INDEX(current, offset, count) ( ( (current) + (offset) ) % (count) )


// дефайны для всякого рода выводов

// #define DEBUG
// #define CALLBACK_DEBUG
#define PRINT_OUTPUT

// #define OUTPUT_READABLE


Scheduler * pTaskManager;
StatusRequest * pStateVectorReady;

uint8_t activeSensor;

// ВНИМАНИЕ: поменяли с uint32_t на uint16_t
// наш интерфейс с внешним миром, сюда кладём готовые векторы состояния
uint16_t stateVector[STATE_VECTOR_SIZE];  

// это рабочий вектор состояния
uint16_t currentStateVector[STATE_VECTOR_SIZE];


// ----------------------- TASKS --------------------------------

// Callback methods prototypes
void InitMeasureCallback();
void MeasureCallback(); bool MeasureOnEnable(); void MeasureOnDisable();
void PingTimeoutCallback();

#define MEASURE_CALLBACK_NUMBER     2

Task tMeasure(  PING_INTERVAL, SENSORS_NUM * MEASURE_CALLBACK_NUMBER, 
                &InitMeasureCallback, NULL, false, 
                &MeasureOnEnable, &MeasureOnDisable);


// объявляем таски для таймаутов по количеству сенсоров
#if defined SENSORS_NUM
    Task tPing_0(MILLIS_TIMEOUT, TASK_ONCE, &PingTimeoutCallback);

    #if SENSORS_NUM > 1
        Task tPing_1(MILLIS_TIMEOUT, TASK_ONCE, &PingTimeoutCallback);
    #endif

    #if SENSORS_NUM > 2
        Task tPing_2(MILLIS_TIMEOUT, TASK_ONCE, &PingTimeoutCallback);
    #endif

    #if SENSORS_NUM > 3
        Task tPing_3(MILLIS_TIMEOUT, TASK_ONCE, &PingTimeoutCallback);
    #endif

#endif // defined SENSORS_NUM


#if SENSORS_NUM == 1
    Task *pingTasks[] = { &tPing_0 };

#elif SENSORS_NUM == 2
    Task *pingTasks[] = { &tPing_0, &tPing_1 };

#elif SENSORS_NUM == 3
    Task *pingTasks[] = { &tPing_0, &tPing_1, &tPing_2 };

#elif SENSORS_NUM == 4
    Task *pingTasks[] = { &tPing_0, &tPing_1, &tPing_2, &tPing_3 };

#endif // defined SENSORS_NUM



// ----------------------- PING TIMEOUT FUNCS --------------------------------

// создаём глобальные функции для каждого таймаут-таска, чтобы их можно было передать
// в FastUltrasonic сенсоры

#define _make_timeout_func(idx) \
    void enableTimeout_##idx() { \
        pingTasks[idx]->restartDelayed(); \
    } \
    \
    void disableTimeout_##idx() { \
        pingTasks[idx]->disable(); \
    }


#if defined SENSORS_NUM
    _make_timeout_func(0)

    #if SENSORS_NUM > 1
        _make_timeout_func(1)
    #endif

    #if SENSORS_NUM > 2
        _make_timeout_func(2)
    #endif

    #if SENSORS_NUM > 3
        _make_timeout_func(3)
    #endif

#endif // defined SENSORS_NUM


#if SENSORS_NUM == 1
    VoidFunc enableExternalTimeoutFuncs[] = { 
        enableTimeout_0 
    };

    VoidFunc disableExternalTimeoutFuncs[] = {
        disableTimeout_0
    };

#elif SENSORS_NUM == 2
    VoidFunc enableExternalTimeoutFuncs[] = {
        enableTimeout_0, enableTimeout_1
    };

    VoidFunc disableExternalTimeoutFuncs[] = {
        disableTimeout_0, disableTimeout_1
    };

#elif SENSORS_NUM == 3
    VoidFunc enableExternalTimeoutFuncs[] = {
        enableTimeout_0, enableTimeout_1, enableTimeout_2
    };

    VoidFunc disableExternalTimeoutFuncs[] = {
        disableTimeout_0, disableTimeout_1, disableTimeout_2
    };

#elif SENSORS_NUM == 4
    VoidFunc enableExternalTimeoutFuncs[] = {
        enableTimeout_0, enableTimeout_1, enableTimeout_2, enableTimeout_3
    };

    VoidFunc disableExternalTimeoutFuncs[] = {
        disableTimeout_0, disableTimeout_1, disableTimeout_2, disableTimeout_3
    };

#endif // defined SENSORS_NUM



// ----------------------- RELAYS --------------------------------

#if SENSORS_NUM == 1
    Relay relays[] = { RELAY_0 };

#elif SENSORS_NUM == 2
    Relay relays[] = { RELAY_0, RELAY_1 };

#elif SENSORS_NUM == 3
    Relay relays[] = { RELAY_0, RELAY_1, RELAY_2 };

#elif SENSORS_NUM == 4
    Relay relays[] = { RELAY_0, RELAY_1, RELAY_2, RELAY_3 };

#endif // defined SENSORS_NUM



// ----------------------- FUNCS --------------------------------

void CopyActiveDistancesToStateVector(bool considerStartDelay);
void ClearStateVector();
void SaveStateVector();

void SetActiveSensor(uint8_t newActiveSensor);
void SensorsRearrange();

void DisableSensorTask(uint8_t taskNum);
bool PingTrigger();


// considerStartDelay - учитывать ли задержку при старте относительно 
// активного сенсора
void CopyActiveDistancesToStateVector(bool considerStartDelay) {

    // аккуратнее с uint16_t, переменные каждого датчика в uint32_t
    uint16_t currentDistance;
    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        currentDistance = sensors[i]->pingDistance;

        // если пинг был получен, то сохраняем дистанцию, иначе 0
        if (currentDistance != 0) {

            // если необходимо учитывать задержку между стартом текущего
            // сенсора относительно активного, то прибавим её к результату
            if (considerStartDelay && i != activeSensor) {
                currentDistance += 
                    sensors[i]->pulseStart - sensors[activeSensor]->pulseStart;
            }
        }

        currentStateVector[i + activeSensor * SENSORS_NUM] = currentDistance;
    }
}

void ClearStateVector() {
    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        currentStateVector[i] = 0;
    }
}

void SaveStateVector() {
    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        stateVector[i] = currentStateVector[i];
    }
}



void SetActiveSensor(uint8_t newActiveSensor) {
#ifdef DEBUG
    Serial.println(" * SetActiveSensor()");
#endif

    activeSensor = newActiveSensor;

#ifdef DEBUG
    Serial.print("Now active sensor is ");
    Serial.println(activeSensor);
#endif

    uint8_t currentSensor;

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        currentSensor = CYCLE_INDEX(activeSensor, i, SENSORS_NUM);

        // TODO: закомментили
        // переключим релехи
        if (currentSensor == activeSensor)
            relays[currentSensor].SetState(OPENED);
        else
            relays[currentSensor].SetState(CLOSED);

#ifdef DEBUG
        Serial.print("Relay ");
        Serial.print(currentSensor);
        Serial.print(" now is ");        
        Serial.println(
            (relays[currentSensor].GetState() == OPENED) ? 
            "OPENED" :
            "CLOSED"
        );
#endif

        // зарегистрируем друг у друга pingTasks и sensors
        sensors[currentSensor]->enableExternalTimeout = enableExternalTimeoutFuncs[i];
        sensors[currentSensor]->disableExternalTimeout = disableExternalTimeoutFuncs[i];

        pingTasks[i]->setLtsPointer( sensors[currentSensor] );
    } 
}



// сдвигаем циклически последовательность тасков таким образом,
// чтобы активный сенсор стартовал первым
void SensorsRearrange() {
    uint8_t nextActiveSensor = CYCLE_INDEX(activeSensor, 1, SENSORS_NUM);

    SetActiveSensor(nextActiveSensor);
}


void DisableSensorTask(uint8_t taskNum) {
    pingTasks[taskNum]->restart();
}



// функция включает сенсоры на запуск процесса посылки ультразвукового пинга
bool PingTrigger() {
#ifdef DEBUG
    Serial.println(" * PingTrigger()");
#endif

    bool includeSensor[SENSORS_NUM];

    uint8_t currentSensor;

    // если какой-нибудь из сенсоров ждёт эхо, ничего не делаем
    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        currentSensor = CYCLE_INDEX(activeSensor, i, SENSORS_NUM);

#ifdef DEBUG
        Serial.print("sensorId = ");
        Serial.println(sensors[currentSensor]->sensorId);
#endif

        if (sensors[currentSensor]->CheckPing() == false) {
            // данный сенсор всё ещё ожидает предыдущего пинга, выключим его
            DisableSensorTask(i);

            // активным сенсором считается самый первый.
            // если он до сих пор ждёт эхо с предыдущего раза, 
            // пропустим целиком весь цикл, иначе просто пропустим запуск сенсора
            if (currentSensor == activeSensor)
                return false;
            else
                includeSensor[currentSensor] = false;
        }

        includeSensor[currentSensor] = true;
#ifdef DEBUG
        Serial.println("CheckPing... OK");
#endif
    }

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {

        currentSensor = CYCLE_INDEX(activeSensor, i, SENSORS_NUM);

        if (includeSensor[currentSensor] == true) {
            pingTasks[i]->restartDelayed();
        }
    }

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {

        currentSensor = CYCLE_INDEX(activeSensor, i, SENSORS_NUM);

        if (includeSensor[currentSensor] == true) {
            sensors[currentSensor]->pingTrig();
            sensors[currentSensor]->pingStart();
        }
    }

    return true;
}



// ----------------------- OUTPUT FUNCS --------------------------------

#ifdef PRINT_OUTPUT

void DisplayCurrentDistances() {
    Serial.print(millis());

    unsigned long cm = 0;

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        cm = sensors[i]->pingDistance / US_ROUNDTRIP_CM;
    
        Serial.print("      ");
        Serial.print(cm);
        Serial.print("(");
        // время старта относительно активного сенсора
        Serial.print(sensors[i]->pulseStart - sensors[activeSensor]->pulseStart);   
        Serial.print(", ");
        Serial.print(sensors[i]->pulseStart);
        Serial.print(", ");
        // длительность пинга
        Serial.print(sensors[i]->pulseStop - sensors[i]->pulseStart);
        Serial.print(")");
    }

    Serial.println();
}

#endif  // PRINT_OUTPUT


// отображает последний готовый вектор состояния
void DisplayStateVector() {
    Serial.print(millis());
    Serial.print(",     ");

    unsigned long cm = 0;

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
#ifdef OUTPUT_READABLE
        Serial.print("      s");
        Serial.print(i);
        Serial.print(": ");
#endif
        for (uint8_t j = 0; j < SENSORS_NUM; j++) {
            cm = stateVector[i * SENSORS_NUM + j] / US_ROUNDTRIP_CM;

            Serial.print(cm);
            Serial.print(", ");
        }
    }

    Serial.println();
}




// ----------------------- CALLBACKS --------------------------------

// просто очистка вектора состояния
bool MeasureOnEnable() {
#ifdef CALLBACK_DEBUG
    Serial.print("> MeasureOnEnable: ");
    Serial.println(millis());
#endif

    ClearStateVector();

    tMeasure.setCallback(&InitMeasureCallback);
    tMeasure.forceNextIteration();

    return true;
}

// иниицируем активный сенсор на запуск
void InitMeasureCallback() {
#ifdef CALLBACK_DEBUG
    Serial.print("> InitMeasureCallback: ");
    Serial.println(millis());
#endif

    ClearSensorsData();

    tMeasure.setCallback(&MeasureCallback);

    // если возвращено false, значит активный сенсор не может быть
    // запущен, пропускаем его
    if (PingTrigger() == false)
        tMeasure.forceNextIteration();
}

// обрабатываем собранные дальности с сенсоров, 
// переключаемся на следующий активный сенсор
void MeasureCallback() {
#ifdef CALLBACK_DEBUG
    Serial.print("> MeasureCallback: ");
    Serial.println(millis());
#endif

#if defined PRINT_OUTPUT
    // TODO: выведем текущие отклики
    // DisplayCurrentDistances();
#endif

    // заполним текущие отклики в вектор состояния
    CopyActiveDistancesToStateVector(false);

    SensorsRearrange();

    tMeasure.setCallback(&InitMeasureCallback);

    // даём релехам время на переключение
    // пока они переключаются, планировщик успеет выполнить другие команды
    tMeasure.delay(RELAY_INTERVAL);
}


// после того, как все сенсоры были опрошены в качестве активных, вектор состояния
// считается сформированным, можем начинать по новой
void MeasureOnDisable() {
#ifdef CALLBACK_DEBUG
    Serial.print("> MeasureOnDisable: ");
    Serial.println(millis());
#endif

    // сохраняем вектор состояния в интерфейсную переменную, чтобы снаружи могли 
    // её взять
    SaveStateVector();

#if defined PRINT_OUTPUT
    DisplayStateVector();
#ifdef OUTPUT_READABLE
    Serial.println();
#endif
#endif

    // если нам был передан StatusRequest, то будем оповещать, что вектор состояния готов
    if (pStateVectorReady != NULL) {
        pStateVectorReady->signalComplete();
    }

    // криво-косо, но дадим последней релехе переключиться
    tMeasure.restartDelayed(RELAY_INTERVAL);
}



void PingTimeoutCallback() {
    // определяем, с каким датчиком мы имеем дело
    BaseSensor& currentSensor = *( (BaseSensor*) pTaskManager->currentLts() );

#if defined CALLBACK_DEBUG && defined DEBUG
    Serial.print("> PingTimeoutCallback");
    Serial.print(", id = ");
    Serial.print(currentSensor.sensorId);
    Serial.print(", ");
    Serial.println(millis());
#endif

    currentSensor.pulseTimeout = true;
    if (currentSensor.pulseBusy) {
        currentSensor.isrStopClock();
    } 
}




// ----------------------- INTERFACE FUNCTION --------------------------------

void InitUltrasonicManager(Scheduler * taskManager, StatusRequest * stateVectorReady) {
    pTaskManager = taskManager;
    pStateVectorReady = stateVectorReady;

    // таким образом мы инициализируем массивы вектора состояния нулями
    ClearStateVector();
    SaveStateVector();

    // инициализируем состояние сенсоров и реле для того, чтобы 0-ой сенсор был активным
    SetActiveSensor(0);

    // добавим таски к taskManager
    pTaskManager->addTask(tMeasure); 

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        // добавляем, но не активируем
        pTaskManager->addTask(*pingTasks[i]);
    } 

    // не забываем, что при enable вызывается метод OnEnable сразу же.
    // в данной ситуации это не страшно, но стоит помнить об этом
    tMeasure.enable();
}