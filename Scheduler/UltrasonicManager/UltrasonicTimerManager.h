#ifndef ULTRASONIC_TIMER_MANAGER_H
#define ULTRASONIC_TIMER_MANAGER_H 

#include <Arduino.h>    // uint8_t, uint16_t, delay() и тд.

#include "TaskSchedulerComplete.h"

#include "pins.h"

// наш интерфейс с внешним миром, сюда кладём готовые векторы состояния, 
// отсюда их можно брать, когда нужно
extern uint16_t stateVector[STATE_VECTOR_SIZE]; // ВНИМАНИЕ: поменяли с uint32_t на uint16_t

// на всякий случай, таск измерений с УЗД, чтобы им можно было управлять
extern Task tMeasure;

// чтобы добавить таск измерений к существующему планировщику, нужно вызвать этот метод
// второй аргумент - это событие, о котором мы можем сигнализировать
void InitUltrasonicTimerManager(Scheduler * taskManager, StatusRequest * stateVectorReady = NULL);

#endif