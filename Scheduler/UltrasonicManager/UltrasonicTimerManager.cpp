#include "UltrasonicTimerManager.h"

#include "SonarsManager.h"


// #define CALLBACK_DEBUG


Scheduler * pTaskManager;
StatusRequest * pStateVectorReady;

// всю красоту делает вот этот вот красавчик
SonarsManager sonarsManager;

uint8_t activeSensor = 0;

// наш интерфейс с внешним миром, сюда кладём готовые векторы состояния
uint16_t stateVector[STATE_VECTOR_SIZE];  

// ================================================================
// ===               TASKS                                      ===
// ================================================================

// Callback methods prototypes
bool MeasureOnEnable(); void MeasureOnDisable();

void PrepareMeasureCallback();
void ActiveSensorMeasureCallback();
void FinishMeasureCallback(); 

#define NUM_OF_STAGES   3

Task tMeasure(  TASK_IMMEDIATE, SENSORS_NUM * NUM_OF_STAGES, 
                &PrepareMeasureCallback, NULL, false, 
                &MeasureOnEnable, &MeasureOnDisable);



// ================================================================
// ===               FUNCTIONS                                  ===
// ================================================================

void SaveStateVector() {
    const uint16_t * currentStateVector = sonarsManager.GetStateVector();  

    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        stateVector[i] = currentStateVector[i];
    }
}

// ================================================================
// ===               CALLBACKS                                  ===
// ================================================================

bool MeasureOnEnable() {
#ifdef CALLBACK_DEBUG
    Serial.print(millis());
    Serial.println(", MeasureOnEnable()");
#endif

    sonarsManager.ClearStateVector();
    activeSensor = 0;

    tMeasure.setCallback(&PrepareMeasureCallback);

    return true;
}

void PrepareMeasureCallback() {
#ifdef CALLBACK_DEBUG
    Serial.print(millis());
    Serial.println(", PrepareMeasureCallback()");
#endif

    sonarsManager.SetActiveSensor(activeSensor);

    tMeasure.setCallback(&ActiveSensorMeasureCallback);
    tMeasure.delay(RELAY_INTERVAL);
}

void ActiveSensorMeasureCallback() {
#ifdef CALLBACK_DEBUG
    Serial.print(millis());
    Serial.println(", ActiveSensorMeasureCallback()");
    Serial.print("activeSensor = ");
    Serial.println(activeSensor);
#endif

    sonarsManager.ClearActiveSensorDistances();
    sonarsManager.AssignTimers();

    sonarsManager.SensorsSimultaneousPing();

    tMeasure.setCallback(&FinishMeasureCallback);
    tMeasure.delay(PING_INTERVAL);
}

void FinishMeasureCallback() {
#ifdef CALLBACK_DEBUG
    Serial.print(millis());
    Serial.println(", FinishMeasureCallback()");
#endif

    sonarsManager.SaveMeasurements();
    sonarsManager.CopyDistancesToStateVector();
    activeSensor++;

    tMeasure.setCallback(&PrepareMeasureCallback);
}

void MeasureOnDisable() {
#ifdef CALLBACK_DEBUG
    Serial.print(millis());
    Serial.println(", MeasureOnDisable()");
    Serial.println();
    Serial.println();
#endif

    SaveStateVector();

    if (pStateVectorReady != NULL) {
        pStateVectorReady->signalComplete();
    }

    tMeasure.restart();
}


// ================================================================
// ===               INTERFACE FUNCTION                         ===
// ================================================================

void InitUltrasonicTimerManager(Scheduler * taskManager, StatusRequest * stateVectorReady) {
    pTaskManager = taskManager;
    pStateVectorReady = stateVectorReady;

    // добавим таски к taskManager
    pTaskManager->addTask(tMeasure); 

    // не забываем, что при enable вызывается метод OnEnable сразу же.
    // в данной ситуации это не страшно, но стоит помнить об этом
    tMeasure.enable();
}