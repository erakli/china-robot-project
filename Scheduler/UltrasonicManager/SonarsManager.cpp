#include "SonarsManager.h"

#include "ArrayFunctions.h"

// #define DEBUG
// #define TEST
// #define TEST_DISABLE_RELAY_SWITCHING

// #define PRINT_ARRAYS
// #define PRINT_IN_CM     false


// вычисление циклического индекса
#define CYCLE_INDEX(current, offset, count) ( ( (current) + (offset) ) % (count) )

// ================================================================
// ===               GLOBAL ARRAYS                              ===
// ================================================================

Sensor g_sensors[SENSORS_NUM] = {
    // Each sensor's trigger pin, echo pin, and max distance to ping.
    NewPingMega(SENSOR_0_TRIG, SENSOR_0_ECHO, MAX_DISTANCE),  
    NewPingMega(SENSOR_1_TRIG, SENSOR_1_ECHO, MAX_DISTANCE),
    NewPingMega(SENSOR_2_TRIG, SENSOR_2_ECHO, MAX_DISTANCE),
    NewPingMega(SENSOR_3_TRIG, SENSOR_3_ECHO, MAX_DISTANCE)
};

Relay g_relays[SENSORS_NUM] = {
    RELAY_0,
    RELAY_1,
    RELAY_2,
    RELAY_3
};

// последовательность таймеров в порядке приоритета
const Timers g_timersSequence[SENSORS_NUM] = {
    Timer1, Timer3, Timer4, Timer5
};



// ================================================================
// ===               ISR FUNCTIONS FOR TIMERS                   ===
// ================================================================

// используем глобальные массивы
void echoCheck0() {
    ::g_sensors[0].CheckEcho();
}

void echoCheck1() {
    ::g_sensors[1].CheckEcho();
}

void echoCheck2() {
    ::g_sensors[2].CheckEcho();
}

void echoCheck3() {
    ::g_sensors[3].CheckEcho();
}


typedef void (*isrFunc)(void);
isrFunc g_isrFuncArray[SENSORS_NUM] = {
    echoCheck0, echoCheck1, echoCheck2, echoCheck3
};



// ================================================================
// ===               SonarsManager methods                      ===
// ================================================================

SonarsManager::SonarsManager() {
    // глобальные массивы
    SetArrays(::g_sensors, ::g_relays);

    SetActiveSensor(0);

    // TODO: temp OPENED all relays
#ifdef TEST
    // for(uint8_t i = 0; i < SENSORS_NUM; i++)
    //     m_relays[i].SetState(OPENED);
#endif
}

SonarsManager::SonarsManager(Sensor *sensors, Relay *relays) { 
    SetArrays(sensors, relays);

    SetActiveSensor(0);
}



void SonarsManager::SetArrays(Sensor *sensors, Relay *relays) {    
    m_sensors = sensors;
    m_relays = relays;

    ClearStateVector();
    ClearActiveSensorDistances();
}


void SonarsManager::MeasurementCycle() {
#ifdef DEBUG
    Serial.println("SonarsManager::MeasurementCycle()");
#endif

    ClearStateVector();

    for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
        SetActiveSensor(sensor);
        // TODO: даём время для реле на переключение
        delay(RELAY_INTERVAL);
        ActiveSensorCycle();
    }

#ifdef PRINT_ARRAYS
    DisplayStateVector(m_stateVector, SENSORS_NUM, SENSORS_NUM, PRINT_IN_CM, "m_stateVector");
#endif
}


void SonarsManager::SetOpenedRelay(uint8_t openedRelay) {
    // закрываем все остальные реле кроме активного
    for (uint8_t relay = 0; relay < SENSORS_NUM; relay++) {
        if (relay == openedRelay)
            m_relays[relay].SetState(OPENED);
        else
            m_relays[relay].SetState(CLOSED);
    }
}


void SonarsManager::SetActiveSensor(uint8_t activeSensor) {
    // if (activeSensor == m_activeSensor) {
    //     return;
    // }

    m_activeSensor = activeSensor;

#ifndef TEST_DISABLE_RELAY_SWITCHING
    // закрываем все остальные реле кроме активного
    SetOpenedRelay(m_activeSensor);
#endif
}



// TODO: отдаём указатель, а не копию!
const uint16_t * SonarsManager::GetStateVector() const {
    return m_stateVector;
}


void SonarsManager::AssignTimers() {
    uint8_t sensorIndex; // вычисляем индекс со сдвигом

    for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
        // присваеваем таймеры каждому из датчиков, начиная с активного
        sensorIndex = CYCLE_INDEX(m_activeSensor, sensor, SENSORS_NUM);
        m_sensors[sensorIndex].assign_timer(g_timersSequence[sensorIndex]);
    }
}

// TODO: добавить условие выбора типа измерений: одновременно или 
// последовательно
void SonarsManager::ActiveSensorCycle() {
#ifdef DEBUG
    Serial.println("SonarsManager::ActiveSensorCycle()");
    Serial.print(">> m_activeSensor = ");
    Serial.println(m_activeSensor);
#endif

    ClearActiveSensorDistances();
    AssignTimers();

    // можно пропускать пинги, которые вообще не были запущены
	bool pingDone = SensorsSimultaneousPing();

    // TODO: ну это типо плохо
    // максимальное время, которое ждём получения echo
    // if (pingDone)
        delay(PING_INTERVAL);	

    // после того, как все пинги были получены, можем их сохранить
    SaveMeasurements();

#ifdef PRINT_ARRAYS
    // DisplayStateVector(m_activeSensorDistances, SENSORS_NUM, PRINT_IN_CM, "m_activeSensorDistances");
#endif

    // копируем отклики со всех сенсоров в вектор состояния на позицию текущего сенсора в векторе состояния
    CopyDistancesToStateVector();
}



// одновременный запуск всех датчиков. между всеми операциями не должно
// происходить никаких лишних действий
// TODO: ввести какой-то список с последовательностью и номерами действующих сенсоров для скорости
bool SonarsManager::SensorsSimultaneousPing() {
    uint8_t sensor;
    uint8_t sensorIndex; // вычисляем индекс со сдвигом

    // массив, в который мы записываем флаги на то, будет ли использован в данной итерации
    // конкректный сеснор
    bool includeSensor[SENSORS_NUM];
#ifdef PENDING_WAIT_START
    uint8_t pendingSensors = SENSORS_NUM;   // подсчитываем количесвто действующих сенсоров
#endif

    // запускаем все сенсоры начиная с активного
    for (sensor = 0; sensor < SENSORS_NUM; sensor++) {
        sensorIndex = CYCLE_INDEX(m_activeSensor, sensor, SENSORS_NUM);

        // если сенсор успешно запущен, то мы будем продолжать с этим сенсором все 
        // следующие этапы
        if (m_sensors[sensorIndex].activate_trigger() == true)
            includeSensor[sensorIndex] = true;

        // иначе, запустить не удалось
        else if (sensorIndex == m_activeSensor)
            // если это активный сенсор (единственный излучающий), то можно сразу прервать
            // процесс пинга
            return false;
        else {
            // иначе, мы просто игнорируем этот сенсор на следующих этапах
            includeSensor[sensorIndex] = false;
#ifdef PENDING_WAIT_START
            pendingSensors--;   // заодно вычитаем данный сеснор из числа действующих
#endif
        }
    }


#if PENDING_WAIT_START == true

    // инициализируем процесс ожидания старта пинга (когда echo == HIGH)
    for (sensor = 0; sensor < SENSORS_NUM; sensor++) {
        sensorIndex = CYCLE_INDEX(m_activeSensor, sensor, SENSORS_NUM);

        if (includeSensor[sensorIndex] == true)
            m_sensors[sensorIndex].init_wait_ping();
    }

    StartPingResults startPingResult; 
    sensor = 0;

    // начинаем проверять начало старта пинга каждого участвующего датчика. если датчик
    // стартанул либо превысил допустимое время ожидания, то уменьшаем количество
    // действующих датчиков и пропускаем его
    while (pendingSensors > 0) {
        sensorIndex = CYCLE_INDEX(m_activeSensor, sensor, SENSORS_NUM);

        if (includeSensor[sensorIndex] == true) {
            startPingResult = m_sensors[sensorIndex].check_ping_start();
            
            switch (startPingResult) {
                case StartPingResult_Ok:
                    // всё норм, стратуем таймер
                    m_sensors[sensorIndex].start_ping_timer(g_isrFuncArray[sensorIndex]);
                    includeSensor[sensorIndex] = false;
                    pendingSensors--;
                break;

                case StartPingResult_Timeout:
                    includeSensor[sensorIndex] = false;
                    pendingSensors--;
                break;

                case StartPingResult_Pending:
                    // продолжаем ждать начала пинга
                break;
            }
            
        }

        sensor++;
    }

#else   // if PENDING_WAIT_START == false

    // ждём старта отсчёта времени echo
    for (sensor = 0; sensor < SENSORS_NUM; sensor++) {
        sensorIndex = CYCLE_INDEX(m_activeSensor, sensor, SENSORS_NUM);

        if (includeSensor[sensorIndex] == true) {
            m_sensors[sensorIndex].wait_ping_start();

            m_sensors[sensorIndex].start_ping_timer(g_isrFuncArray[sensorIndex]);
        }
    }

#endif // PENDING_WAIT_START == true

    return true;
}



// ---------------------------------------------------------------------------
// всякая работа с массивами
// ---------------------------------------------------------------------------

void SonarsManager::SaveMeasurements() {
    uint16_t ping_result;

    for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
        // если пинг был получен, то сохраняем результат, иначе 0
        if (m_sensors[sensor].PingRecieved()) {
#if CONSIDER_START_DELAY == true
            ping_result = 
                m_sensors[sensor].ping_result +
                (m_sensors[sensor].pingStartTime - 
                m_sensors[m_activeSensor].pingStartTime);
#else
            ping_result = m_sensors[sensor].ping_result;
#endif
        } else {
            ping_result = 0;
        }

        m_activeSensorDistances[sensor] = ping_result;
    }
}



void SonarsManager::CopyDistancesToStateVector() {
    uint8_t offset = m_activeSensor * SENSORS_NUM;
    for (uint8_t i = 0; i < SENSORS_NUM; i++)
        m_stateVector[i + offset] = m_activeSensorDistances[i];
}



// для массивов будем пользоваться
// void * memcpy( void * dest, const void * src, size_t count ); // count - num of bytes
// void * memset ( void * ptr, int value, size_t num );

// num - Number of elements to allocate. size - Size of each element.
// void * calloc ( size_t num, size_t size ); 

void SonarsManager::ClearActiveSensorDistances() {
    memset(m_activeSensorDistances, 0, SENSORS_NUM * sizeof(uint16_t));
}

void SonarsManager::ClearStateVector() {
    memset(m_stateVector, 0, STATE_VECTOR_SIZE * sizeof(uint16_t));
}