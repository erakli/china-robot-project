#ifndef DRIVE_MANAGER_H
#define DRIVE_MANAGER_H 

#include <Arduino.h>

#include "TaskSchedulerComplete.h"

#define PID_OUTPUT_LIMIT_MIN    -255
#define PID_OUTPUT_LIMIT_MAX    255

// TODO: возможно с этим стоит поиграться
#define PID_SAMPLE_TIME         5   // время, через которое будет вычисляться выход ПИДа

#define PID_COEFF_P     1.7  // было 18  // напрямую зависит от MOVE_PERIOD
#define PID_COEFF_I     0.9
#define PID_COEFF_D     0.3

#define OUTPUT_MIN                  40  // от 0 до 255
#define ANGLE_SETPOINT_DELTA        1.0 // в градусах

#define BASE_SPEED                  80  // от 0 до 255

#define ANALOG_DELAY_AFTER_STOP     100 // микросекунды

// период с которым будет проводиться проверка и отработка команды 
// напрямую зависит от скорости выдачи DMP (задаётся в MPU6050_6Axis_MotionApps20.h,
// где-то в 305 строчке
#define MOVE_PERIOD                 5 //25  // ms


enum Directions {
    Direction_Stop,

    Direction_NorthEast,    // по диагонали направо
    Direction_NorthWest,    // по диагонали налево

    Direction_East,         // направо
    Direction_West,         // налево

    Direction_North,        // прямо
    Direction_South,        // назад

    Direction_East15,       // направо на 15 градусов
    Direction_West15,       // налево на 15 градусов

    Direction_RotateRight,  // вращаться направо
    Direction_RotateLeft    // вращаться налево
};

enum Moves {
    Move_Stop,
    Move_Forward,
    Move_Backwards,
    Move_Left,
    Move_Right
};


// будем ожидать этого события, чтобы отрабатывать команды
extern StatusRequest newDirectionReceivedStatus;

// будем ожидать сигнала выставки БИНСа
extern StatusRequest mpuInitializedStatus; 

// чтобы добавить таск к существующему планировщику, нужно вызвать этот метод
void InitDriveManager(Scheduler * taskManager);

void SetDirection(Directions newDirection, float currentYaw);
void DoMove(Moves moveCommand, uint8_t speed);

#endif