#include "DriveManager.h"

#include "ImuManager.h"             // extern float yaw;
#include "CommunicationManager.h"   // extern Command lastCommand;

#include <DirectIO.h>

#include "pid_v1.h"

#include "pins.h"


#define ROTATION_15_ANGLE       15.0
#define ROTATION_QUATER_ANGLE   45.0
#define ROTATION_HALF_ANGLE     90.0
#define ROTATION_FULL_ANGLE     180.0
#define ROTATION_CIRCLE         360.0


// #define DEBUG
// #define OUTPUT_PRINT
// #define CALLBACK_DEBUG


// ================================================================
// ===          DIRECT I/O  PINS DEFINITIONS                    ===
// ================================================================

Output<DIR_RIGHT> pDirRight;
Output<DIR_LEFT> pDirLeft;


// ================================================================
// ===          PID                                             ===
// ================================================================

double pidSetpoint = 0.0;
double pidInput = 0.0;
double pidOutput = 0.0;

PID rotationPID(    &pidInput, &pidOutput, &pidSetpoint,
                    PID_COEFF_P, PID_COEFF_I, PID_COEFF_D,
                    DIRECT);


// ================================================================
// ===          GLOBAL VARIABLES                                ===
// ================================================================

bool isPerformingRotation = false; // чтобы игнорировать команды, пока выполняем поворот  
bool isStopped = false;

Scheduler * pDriveTaskManager;

StatusRequest newDirectionReceivedStatus;
StatusRequest mpuInitializedStatus; // будем ожидать сигнала выставки БИНСа
// StatusRequest GyroDataReadyStatus;

// пользуемся интерфейсной переменной из ImuManager.h
extern float yaw;

// а также расшифровываем команды из Serial
extern Command lastCommand;

// ================================================================
// ===          FUNCTIONS PROTOTYPES                            ===
// ================================================================

void PrepareForImuInitialization();
void PrepareForNextCommand();

Directions TranslateCommand(Command command);

void InitializePID();
void InitializeMotors();
void NormalizeSetpoint();
void SetDirection(Directions newDirection, float currentYaw);
void CheckFunction(float currentAngle);
void DoMove(Moves moveCommand, uint8_t speed);



// ================================================================
// ===          TASKS                                           ===
// ================================================================

// Callback methods prototypes
void MoveCallback();
void InitSetpointCallback();
void ProcessCommandCallback();

Task tMove(MOVE_PERIOD, TASK_FOREVER, &MoveCallback);
Task tProcessCommand(&InitSetpointCallback);



// ================================================================
// ===          CALLBACKS                                       ===
// ================================================================

void MoveCallback() {
#ifdef DEBUG
    // Serial.println("MoveCallback");
    // Serial.print("yaw = ");
    // Serial.println(yaw);
#endif

    CheckFunction(yaw);
}


// когда бинс выставиться, мы вызовем этот колбек, чтобы установить pidSetpoint
void InitSetpointCallback() {
#ifdef CALLBACK_DEBUG
        Serial.print(millis());
        Serial.println(", InitSetpointCallback");
#endif

    SetDirection(Direction_Stop, yaw);

    Serial.println("Commands working!");

    // переключаемся на основной колбэк
    tProcessCommand.setCallback(&ProcessCommandCallback);

    PrepareForNextCommand();
}

void ProcessCommandCallback() {
#ifdef CALLBACK_DEBUG
        Serial.print(millis());
        Serial.println(", ProcessCommandCallback");
#endif

    Directions newDirection = TranslateCommand(lastCommand);
    SetDirection(newDirection, yaw);

    PrepareForNextCommand();
}



// ================================================================
// ===          FUNCTIONS                                       ===
// ================================================================


void PrepareForImuInitialization() {
    mpuInitializedStatus.setWaiting();
    tProcessCommand.waitFor(&mpuInitializedStatus);
}


void PrepareForNextCommand() {
    newDirectionReceivedStatus.setWaiting();
    tProcessCommand.waitFor(&newDirectionReceivedStatus);
}



Directions TranslateCommand(Command command) {
    Directions direction = Direction_Stop;

    switch (command) {
        case '7' :
            return Direction_NorthWest;

        case '9' :
            return Direction_NorthEast;

        case '4' :
            return Direction_West;

        case '6' :
            return Direction_East;

        case '5' :
            return Direction_Stop;

        case '8' :
            return Direction_North;

        case '2' :
            return Direction_South;


        // TODO: дополнительные команды
        case '1' :
            return Direction_West15;

        case '3' :
            return Direction_East15;


        case 'R' :
            return Direction_RotateRight;

        case 'L' :
            return Direction_RotateLeft;


        default:
            // TODO: по хорошему, надо выводить сообщение об неудачной команде
#ifdef DEBUG
            Serial.println("Unknown command");
#endif
            return Direction_Stop;
    }
}



void InitializePID() {
    rotationPID.SetOutputLimits(PID_OUTPUT_LIMIT_MIN, PID_OUTPUT_LIMIT_MAX);
    rotationPID.SetSampleTime(PID_SAMPLE_TIME); // установим частоту вычисления ПИДа

    //turn the PID on
    rotationPID.SetMode(AUTOMATIC);
}

void InitializeMotors() {
    pDirRight = HIGH;
    pDirLeft = HIGH;
}



void NormalizeSetpoint() {
    if (pidSetpoint >= ROTATION_FULL_ANGLE)
        pidSetpoint -= ROTATION_CIRCLE;
    else if (pidSetpoint < -ROTATION_FULL_ANGLE)
        pidSetpoint += ROTATION_CIRCLE;
}


void SetDirection(Directions newDirection, float currentYaw) {
#ifdef DEBUG
    Serial.print("SetDirection(");
    Serial.print("newDirection = ");
    Serial.print(newDirection);
    Serial.print(", currentYaw = ");
    Serial.print(currentYaw);
    Serial.println(")");

    Serial.print("isPerformingRotation = ");
    Serial.println(isPerformingRotation);
#endif

    // если совершаем разворот (отработку команды), то не будем реагировать на новую команду
    if (isPerformingRotation)
        return;

    switch (newDirection) {
        case Direction_NorthWest :
            pidSetpoint = currentYaw - ROTATION_QUATER_ANGLE;
        break;

        case Direction_NorthEast :
            pidSetpoint = currentYaw + ROTATION_QUATER_ANGLE;
        break;

        case Direction_West :
            pidSetpoint = currentYaw - ROTATION_HALF_ANGLE;
        break;

        case Direction_East :
            pidSetpoint = currentYaw + ROTATION_HALF_ANGLE;
        break;

        case Direction_Stop :
            pidSetpoint = currentYaw;
            DoMove(Move_Stop, 0);
        break;

        case Direction_North :
            DoMove(Move_Forward, BASE_SPEED);
        break;

        case Direction_South :
            DoMove(Move_Backwards, BASE_SPEED);
        break;


        // TODO: дополнительные команды
        case Direction_West15 :
            pidSetpoint = currentYaw - ROTATION_15_ANGLE;
        break;

        case Direction_East15 :
            pidSetpoint = currentYaw + ROTATION_15_ANGLE;
        break;

        
        case Direction_RotateRight :
            DoMove(Move_Right, BASE_SPEED);
        break;

        case Direction_RotateLeft :
            DoMove(Move_Left, BASE_SPEED);
        break;
    }

    NormalizeSetpoint();

#ifdef DEBUG
    Serial.print("pidSetpoint = ");
    Serial.println(pidSetpoint);
#endif

    if (    newDirection == Direction_North || 
            newDirection == Direction_South || 
            newDirection == Direction_Stop ||
            newDirection == Direction_RotateRight ||
            newDirection == Direction_RotateLeft)
    {
        isPerformingRotation = false;
    }
    else {
        isPerformingRotation = true;
    }
}



Moves moveCommand;
uint8_t outputValue;

void CheckFunction(float currentAngle) {

#ifdef DEBUG
    // Serial.print("CheckFunction(");
    // Serial.print("currentAngle = ");
    // Serial.print(currentAngle);
    // Serial.println(")");
#endif

    pidInput = currentAngle;
    rotationPID.Compute(true);

#ifdef DEBUG
    // Serial.print("pidOutput = ");
    // Serial.println(pidOutput);
    // Serial.print("pidSetpoint = ");
    // Serial.println(pidSetpoint);
#endif

    // если последняя команда была по прямой, то не отрабатываем с помощью
    // ПИДа отклонения угла
    if (isPerformingRotation == false) 
        return;

    if (abs(currentAngle - pidSetpoint) > ANGLE_SETPOINT_DELTA) {
        // мы ещё далеко от требуемого положения
        outputValue = abs(pidOutput);
        if (outputValue > OUTPUT_MIN) {
            moveCommand = (pidOutput < 0.0) ? Move_Left : Move_Right;
            isStopped = false;  // мы вышли из состояния покоя
        } else {
            // выход с ПИДа маловат, поэтому просто ничего не будем делать
            isPerformingRotation = false;   // отработку команды завершили, можем принимать 
            return;
        }
    } else {
        // если мы уже остановились, то не будем ничего выполнять
        if (isStopped == true)
            return;

        isStopped = true;
        isPerformingRotation = false;   // отработку команды завершили, можем принимать 

        // мы достаточно близко к требуемому положению, можем остановиться
        outputValue = 0;
        moveCommand = Move_Stop;
    }

    DoMove(moveCommand, outputValue);

#ifdef DEBUG
    Serial.println();
#endif
}



// управление моторами

void MotorsStop() {
    analogWrite(PWM_RIGHT, LOW);
    analogWrite(PWM_LEFT, LOW); 

    // TODO: надо этого как-то избежать
    delayMicroseconds(ANALOG_DELAY_AFTER_STOP);
}

void MotorsForward() {
    pDirRight = LOW;
    pDirLeft = LOW;

#ifdef OUTPUT_PRINT
    Serial.println("Forward");
#endif
}

void MotorsBackwards() {
    pDirRight = HIGH;
    pDirLeft = HIGH;

#ifdef OUTPUT_PRINT
    Serial.println("Back");
#endif
}

void MotorsLeft() {
    pDirRight = HIGH;
    pDirLeft = LOW; 

#ifdef OUTPUT_PRINT
    Serial.println("Left");
#endif    
}

void MotorsRight() {
    pDirRight = LOW;
    pDirLeft = HIGH; 

#ifdef OUTPUT_PRINT
    Serial.println("Right");
#endif
}


// включение моторов
void DoMove(Moves moveCommand, uint8_t speed) {

#ifdef DEBUG
    Serial.print("DoMove(");
    Serial.print("moveCommand = ");
    Serial.print(moveCommand);
    Serial.print(", speed = ");
    Serial.print(speed);
    Serial.println(")");
#endif

    MotorsStop();


    switch (moveCommand) {

        case Move_Forward :          
            MotorsForward();
        break;
      
        case Move_Backwards :
            MotorsBackwards();
        break;

        case Move_Left :
            MotorsLeft();
        break;

        case Move_Right :// вправо
            MotorsRight();
        break;

        case Move_Stop:
#ifdef OUTPUT_PRINT
            // Serial.println("Stop");
#endif
            return;
        break;
        
    } // end switch


    if (    moveCommand == Move_Forward || 
            moveCommand == Move_Backwards ||
            moveCommand == Move_Left ||
            moveCommand == Move_Right)
    {
        analogWrite(PWM_RIGHT, speed);
        analogWrite(PWM_LEFT, speed);
    }
}





// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

void InitDriveManager(Scheduler * taskManager) {
    pDriveTaskManager = taskManager;

    InitializeMotors();

    InitializePID();

    pDriveTaskManager->addTask(tProcessCommand);
    pDriveTaskManager->addTask(tMove);

    // до того момента, пока бинс не выставиться, ждём от него сигнала о готовности
    PrepareForImuInitialization();
    // PrepareForNextCommand();

    tProcessCommand.enable();
    tMove.enable();
}