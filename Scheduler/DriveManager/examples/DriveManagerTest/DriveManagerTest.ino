#include "TaskSchedulerComplete.h"

#include "DriveManager.h"
#include "CommunicationManager.h"
#include "ImuManager.h"


extern float yaw;

// Schedulers
// gyroManager is a higher priority scheduler
// taskManager is a base scheduler
Scheduler gyroManager;
Scheduler taskManager;

StatusRequest gyroDataReady;

// Callback methods prototypes
void CycleCallback();

Task tCycle(&CycleCallback);


void PrepareStatus() {
    gyroDataReady.setWaiting();
    tCycle.waitFor(&gyroDataReady);
}


void CycleCallback() {
    Serial.print(millis());
    Serial.print(", yaw = ");
    Serial.println(yaw);

    PrepareStatus();
}



// ----------------------- MAIN CODE --------------------------------

void setup() {
    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");

    Serial.print("Wait ");
    Serial.print(MPU_SETTING_TIME / 1000);
    Serial.print(" sec before IMU gets working");

    PrepareStatus();

    // запускаем таски
    InitImuManager(&gyroManager, &gyroDataReady, &mpuInitializedStatus);
    InitDriveManager(&gyroManager);
    InitCommunicationManager(&taskManager, &newDirectionReceivedStatus);

    gyroManager.addTask(tCycle);

    taskManager.setHighPriorityScheduler(&gyroManager);

    tCycle.enable();
}


void loop() {
    taskManager.execute();
}