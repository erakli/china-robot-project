#define _TASK_STATUS_REQUEST
#include <TaskScheduler.h>  

#include <DirectIO.h>
#include <EnableInterrupt.h>


#define TRIGGERPIN 6
#define ECHOPIN 7


Output<TRIGGERPIN> pTrigger;
Input<ECHOPIN> pEcho;


Scheduler r;
StatusRequest measure;


// Callback methods prototypes
void measureCallback();
void pingTimeoutCallback();
void displayCallback();

bool measureEnable();

Task tMeasure(50, TASK_ONCE, &measureCallback, &r, false, &measureEnable);
Task tPing(0, TASK_ONCE, &pingTimeoutCallback, &r, false);
Task tDisplay(&displayCallback, &r);


volatile bool pulseBusy = false;
volatile bool pulseTimeout = false;
volatile unsigned long pulseStart = 0;
volatile unsigned long pulseStop = 0;
volatile unsigned long pingDistance = 0;


void pingTrigger(unsigned long aTimeout) {
    if (pulseBusy)
        return; // do not trigger if in the middle of a pulse

    if (pEcho == HIGH)
        return; // do not trigger if ECHO pin is high

    pulseBusy = true;
    pulseTimeout = false;

    pTrigger = LOW;
    delayMicroseconds(4);
    pTrigger = HIGH;

    tPing.setInterval(aTimeout);

    delayMicroseconds(10);
    pTrigger = LOW;

    tPing.restartDelayed(); // timeout countdown starts now
    
    // will start the pulse clock on the rising edge of ECHO pin

    // PCintPort::attachInterrupt(ECHOPIN, &pingStartClock, RISING);
    enableInterrupt(ECHOPIN, &pingStartClock, RISING);
}



/////////////////////// ISR //////////////////////////////////////////////

// Start clock on the rising edge of the ultrasonic pulse
void pingStartClock() {
    pulseStart = micros();

    // PCintPort::detachInterrupt(ECHOPIN); // not sure this is necessary
    // PCintPort::attachInterrupt(ECHOPIN, &pingStopClock, FALLING);
    disableInterrupt(ECHOPIN); 
    enableInterrupt(ECHOPIN, &pingStopClock, FALLING);

    // start counting timeout
    tPing.restartDelayed();
}


// Stop clock on the falling edge of the ultrasonic pulse
void pingStopClock() {
    pulseStop = micros();

    // PcintPort::detachInterrupt(ECHOPIN);
    disableInterrupt(ECHOPIN); 

    if (pulseTimeout == false) {
        pingDistance = pulseStop - pulseStart;
        measure.signal();   // signal about ping completed
    } else {
        pingDistance = 0;
        measure.signal(-1); // signal about error
    }

    pulseBusy = false;
    tPing.disable(); // disable timeout 
}



////////////////// Callbacks /////////////////////////////////////////////

// Stop clock because of the timeout – the wave did not return
void pingTimeoutCallback() {
    pulseTimeout = true;
    if (pulseBusy) {
        pingStopClock();
    } 
}

bool measureEnable() {
    // Serial.println("MeasureEnable: Activating sensors");  

    // when measure signal recieved, display result
    measure.setWaiting(); // Set the StatusRequest to wait for 3 signals. 
    tDisplay.waitFor(&measure);

    pingTrigger(30); // 30 milliseconds or max range of ~5.1 meters
    
    return true;
}

// Initial measure callback sets the trigger
void measureCallback() {
    // Serial.println("measureCallback");

    if (measure.pending()) {
        tDisplay.disable();
        measure.signalComplete(-1);  // signal error
        Serial.println("MeasureCallback: Timeout!");
    }

    tMeasure.restartDelayed();
}


void displayCallback() {
    // char d[256];

    unsigned long cm = 0;

    if ( measure.getStatus() >= 0) // only calculate if statusrequest ended successfully
        cm = pingDistance / 57;

    // snprintf(
    //     d, 256, 
    //     "pulseStart = %8lu\tpulseStop=%8lu\tdistance, cm=%8lu", 
    //     pulseStart, pulseStop, cm);

    // Serial.println(d);

    Serial.print(millis());
    Serial.print("      ");
    Serial.println(cm);
}





void setup() {
    // put your setup code here, to run once:

    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    pTrigger = LOW;
    pEcho = LOW;

    tMeasure.restartDelayed();
}


void loop() {
    r.execute();
}