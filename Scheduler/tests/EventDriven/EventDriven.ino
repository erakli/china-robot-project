#include <DirectIO.h>
#include <TaskScheduler.h>
#include <EnableInterrupt.h>


#define TRIGGERPIN 5
#define ECHOPIN 6


Output<TRIGGERPIN> pTrigger;
Input<ECHOPIN> pEcho;


// Callback methods prototypes
void measureCallback();
void displayCallback();
void pingCalcCallback();


Scheduler r;

Task tMeasure(50, TASK_FOREVER, &measureCallback, &r, true);
Task tDisplay(50, TASK_FOREVER, &displayCallback, &r, true);
Task tPing(TASK_IMMEDIATE, TASK_ONCE, &pingCalcCallback, &r, false);


volatile bool pulseBusy = false;
volatile bool pulseTimeout = false;
volatile unsigned long pulseStart = 0;
volatile unsigned long pulseStop = 0;
volatile unsigned long pingDistance = 0;


void pingTrigger(unsigned long aTimeout)
{
    if (pulseBusy)
        return; // do not trigger if in the middle of a pulse
    if (pEcho == HIGH)
        return; // do not trigger if ECHO pin is high

    pulseBusy = true;
    pulseTimeout = false;

    pTrigger = LOW;
    delayMicroseconds(4);
    pTrigger = HIGH;

    tPing.setInterval(aTimeout);

    delayMicroseconds(10);
    pTrigger = LOW;

    tPing.restartDelayed(); // timeout countdown starts now
    
    // will start the pulse clock on the rising edge of ECHO pin

    // PCintPort::attachInterrupt(ECHOPIN, &pingStartClock, RISING);
    enableInterrupt(ECHOPIN, pingStartClock, RISING);
}


// Start clock on the rising edge of the ultrasonic pulse
void pingStartClock()
{
    pulseStart = micros();

    // PCintPort::detachInterrupt(ECHOPIN); // not sure this is necessary
    // PCintPort::attachInterrupt(ECHOPIN, &pingStopClock, FALLING);
    disableInterrupt(ECHOPIN); 
    enableInterrupt(ECHOPIN, pingStopClock, FALLING);

    tPing.restartDelayed();
}


// Stop clock on the falling edge of the ultrasonic pulse
void pingStopClock()
{
    pulseStop = micros();

    // PcintPort::detachInterrupt(ECHOPIN);
    disableInterrupt(ECHOPIN); 

    pingDistance = pulseStop - pulseStart;
    pulseBusy = false;
    tPing.disable(); // disable timeout
}


// Stop clock because of the timeout – the wave did not return
void pingCalcCallback()
{
    if (pulseBusy)
    {
        pingStopClock();
    }
    pulseTimeout = true;
}


// Initial measure callback sets the trigger
void measureCallback()
{
    if (pulseBusy)
    { // already measuring, try again
        tMeasure.enable();
        return;
    }
    pingTrigger(30); // 30 milliseconds or max range of ~5.1 meters
    tMeasure.setCallback(&measureCallbackWait);
}


// Wait for the measurement to
void measureCallbackWait()
{
    if (pulseBusy)
        return;

    tMeasure.setCallback(&measureCallback);
}


void displayCallback()
{
    // char d[256];

    unsigned long cm = pingDistance / 57; // cm

    // snprintf(
    //     d, 256, 
    //     "pulseStart = %8lu\tpulseStop=%8lu\tdistance, cm=%8lu", 
    //     pulseStart, pulseStop, cm);

    // Serial.println(d);
    Serial.println(cm);
}



void setup()
{
    // put your setup code here, to run once:

    Serial.begin(115200);

    pTrigger = LOW;
    pEcho = LOW;
}


void loop()
{
    // put your main code here, to run repeatedly:
    r.execute();
}