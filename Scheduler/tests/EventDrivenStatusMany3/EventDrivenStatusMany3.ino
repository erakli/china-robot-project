#define _TASK_LTS_POINTER       // Compile with support for Local Task Storage pointer
#include <TaskScheduler.h>  

#include <DirectIO.h>

#include "FastUltrasonic.h"
#include "Relay.h"

#include "pins.h"


#define PING_INTERVAL	50     // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define RELAY_INTERVAL  10   // mS
#define MILLIS_TIMEOUT  10     // 30 milliseconds or max range of ~5.1 meters

#define STATE_VECTOR_SIZE		SENSORS_NUM * SENSORS_NUM

// вычисление циклического индекса
#define CYCLE_INDEX(current, offset, count) ( ( (current) + (offset) ) % (count) )




// ----------------------- RELAYS --------------------------------

#if SENSORS_NUM == 1
    Relay relays[] = { RELAY_0 };

#elif SENSORS_NUM == 2
    Relay relays[] = { RELAY_0, RELAY_1 };

#elif SENSORS_NUM == 3
    Relay relays[] = { RELAY_0, RELAY_1, RELAY_2 };

#elif SENSORS_NUM == 4
    Relay relays[] = { RELAY_0, RELAY_1, RELAY_2, RELAY_3 };

#endif // defined SENSORS_NUM



// ----------------------- TASKS --------------------------------

// Callback methods prototypes
void CycleCallback();
void initMeasureCallback();
void measureCallback(); bool MeasureOnEnable(); void MeasureOnDisable();
void pingTimeoutCallback();



// taskManager is a base scheduler
Scheduler taskManager;


Task tCycle(PING_INTERVAL, TASK_FOREVER, &CycleCallback);
Task tMeasure(PING_INTERVAL, SENSORS_NUM * 2, &initMeasureCallback, NULL, false, &MeasureOnEnable, &MeasureOnDisable);


// объявляем таски для таймаутов по количеству сенсоров
#if defined SENSORS_NUM
    Task tPing_0(MILLIS_TIMEOUT, TASK_ONCE, &pingTimeoutCallback);

    #if SENSORS_NUM > 1
        Task tPing_1(MILLIS_TIMEOUT, TASK_ONCE, &pingTimeoutCallback);
    #endif

    #if SENSORS_NUM > 2
        Task tPing_2(MILLIS_TIMEOUT, TASK_ONCE, &pingTimeoutCallback);
    #endif

    #if SENSORS_NUM > 3
        Task tPing_3(MILLIS_TIMEOUT, TASK_ONCE, &pingTimeoutCallback);
    #endif

#endif // defined SENSORS_NUM


#if SENSORS_NUM == 1
    Task *pingTasks[] = { &tPing_0 };

#elif SENSORS_NUM == 2
    Task *pingTasks[] = { &tPing_0, &tPing_1 };

#elif SENSORS_NUM == 3
    Task *pingTasks[] = { &tPing_0, &tPing_1, &tPing_2 };

#elif SENSORS_NUM == 4
    Task *pingTasks[] = { &tPing_0, &tPing_1, &tPing_2, &tPing_3 };

#endif // defined SENSORS_NUM



// ----------------------- PING TIMEOUT FUNCS --------------------------------

#define _make_timeout_func(idx) \
    void enableTimeout_##idx() { \
        pingTasks[idx]->restartDelayed(); \
    } \
    \
    void disableTimeout_##idx() { \
        pingTasks[idx]->disable(); \
    }


#if defined SENSORS_NUM
    _make_timeout_func(0)

    #if SENSORS_NUM > 1
        _make_timeout_func(1)
    #endif

    #if SENSORS_NUM > 2
        _make_timeout_func(2)
    #endif

    #if SENSORS_NUM > 3
        _make_timeout_func(3)
    #endif

#endif // defined SENSORS_NUM


#if SENSORS_NUM == 1
    VoidFunc enableExternalTimeoutFuncs[] = { 
        enableTimeout_0 
    };

    VoidFunc disableExternalTimeoutFuncs[] = {
        disableTimeout_0
    };

#elif SENSORS_NUM == 2
    VoidFunc enableExternalTimeoutFuncs[] = {
        enableTimeout_0, enableTimeout_1
    };

    VoidFunc disableExternalTimeoutFuncs[] = {
        disableTimeout_0, disableTimeout_1
    };

#elif SENSORS_NUM == 3
    VoidFunc enableExternalTimeoutFuncs[] = {
        enableTimeout_0, enableTimeout_1, enableTimeout_2
    };

    VoidFunc disableExternalTimeoutFuncs[] = {
        disableTimeout_0, disableTimeout_1, disableTimeout_2
    };

#elif SENSORS_NUM == 4
    VoidFunc enableExternalTimeoutFuncs[] = {
        enableTimeout_0, enableTimeout_1, enableTimeout_2, enableTimeout_3
    };

    VoidFunc disableExternalTimeoutFuncs[] = {
        disableTimeout_0, disableTimeout_1, disableTimeout_2, disableTimeout_3
    };

#endif // defined SENSORS_NUM



// ----------------------- FUNCS --------------------------------

uint8_t activeSensor = 0;
uint32_t stateVector[STATE_VECTOR_SIZE] = {0};



// considerStartDelay - учитывать ли задержку при старте относительно 
// активного сенсора
void CopyActiveDistancesToStateVector(bool considerStartDelay) {
    uint32_t currentDistance;
    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        currentDistance = sensors[i]->pingDistance;

        // если пинг был получен, то сохраняем дистанцию, иначе 0
        if (currentDistance != 0) {

            // если необходимо учитывать задержку между стартом текущего
            // сенсора относительно активного, то прибавим её к результату
            if (considerStartDelay && i != activeSensor) {
                currentDistance += 
                    sensors[i]->pulseStart - sensors[activeSensor]->pulseStart;
            }

            stateVector[i + activeSensor * SENSORS_NUM] = currentDistance;

        } else {
            stateVector[i + activeSensor * SENSORS_NUM] = 0;
        }
    }
}

void ClearStateVector() {
    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        stateVector[i] = 0;
    }
}



void SetActiveSensor(uint8_t newActiveSensor) {
    activeSensor = newActiveSensor;

    // Serial.print("Now active sensor is ");
    // Serial.println(activeSensor);

    uint8_t currentSensor;

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        currentSensor = CYCLE_INDEX(activeSensor, i, SENSORS_NUM);

        // переключим релехи
        if (currentSensor == activeSensor)
            relays[currentSensor].SetState(OPENED);
        else
            relays[currentSensor].SetState(CLOSED);

        // Serial.print("Relay ");
        // Serial.print(currentSensor);
        // Serial.print(" now is ");        
        // Serial.println(
        //     (relays[currentSensor].GetState() == OPENED) ? 
        //     "OPENED" :
        //     "CLOSED"
        // );

        // зарегестрируем друг у друга pingTasks и sensors
        sensors[currentSensor]->enableExternalTimeout = enableExternalTimeoutFuncs[i];
        sensors[currentSensor]->disableExternalTimeout = disableExternalTimeoutFuncs[i];

        pingTasks[i]->setLtsPointer( sensors[currentSensor] );
    } 
}


// сдвигаем циклически последовательность тасков таким образом,
// чтобы активный сенсор стартовал первым
void SensorsRearrange() {
    uint8_t nextActiveSensor = CYCLE_INDEX(activeSensor, 1, SENSORS_NUM);

    SetActiveSensor(nextActiveSensor);
}


void DisableSensorTask(uint8_t taskNum) {
    pingTasks[taskNum]->restart();
}



// функция включает сенсоры на запуск процесса посылки ультразвукового пинга
bool pingTrigger() {
    // Serial.println("pingTrigger()");

    bool includeSensor[SENSORS_NUM];

    uint8_t currentSensor;

    // если какой-нибудь из сенсоров ждёт эхо, ничего не делаем
    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        // Serial.print("sensorId = ");
        // Serial.println(sensors[i]->sensorId);

        currentSensor = CYCLE_INDEX(activeSensor, i, SENSORS_NUM);

        if (sensors[currentSensor]->CheckPing() == false) {
            // данный сенсор всё ещё ожидает предыдущего пинга, выключим его
            DisableSensorTask(i);

            // активным сенсором считается самый первый.
            // если он до сих пор ждёт эхо с предыдущего раза, 
            // пропустим целиком весь цикл, иначе просто пропустим запуск сенсора
            if (currentSensor == activeSensor)
                return false;
            else
                includeSensor[currentSensor] = false;
        }

        includeSensor[currentSensor] = true;

        // Serial.println("CheckPing... OK");
    }

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {

        currentSensor = CYCLE_INDEX(activeSensor, i, SENSORS_NUM);

        if (includeSensor[currentSensor] == true) {
            pingTasks[i]->restartDelayed();
        }
    }

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {

        currentSensor = CYCLE_INDEX(activeSensor, i, SENSORS_NUM);

        if (includeSensor[currentSensor] == true) {
            sensors[currentSensor]->pingTrig();
            sensors[currentSensor]->pingStart();
        }
    }

    return true;
}



void display() {
    Serial.print(millis());

    // uint8_t currentSensor;
    unsigned long cm = 0;

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        cm = sensors[i]->pingDistance / US_ROUNDTRIP_CM;
    
        Serial.print("      ");
        Serial.print(cm);
        Serial.print("(");
        // время старта относительно активного сенсора
        Serial.print(sensors[i]->pulseStart - sensors[activeSensor]->pulseStart);   
        Serial.print(", ");
        Serial.print(sensors[i]->pulseStart);
        Serial.print(", ");
        // длительность пинга
        Serial.print(sensors[i]->pulseStop - sensors[i]->pulseStart);
        Serial.print(")");
    }

    Serial.println();
}


void displayStateVector() {
    Serial.print(millis());

    unsigned long cm = 0;

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        Serial.print("      s");
        Serial.print(i);
        Serial.print(": ");
        for (uint8_t j = 0; j < SENSORS_NUM; j++) {
            cm = stateVector[i * SENSORS_NUM + j] / US_ROUNDTRIP_CM;

            Serial.print(cm);
            Serial.print(", ");
        }
    }

    Serial.println();
}



// ----------------------- CALLBACKS --------------------------------


void CycleCallback() {
    // Serial.print(millis());
    // Serial.println(", new cycle ************************************");
}



bool MeasureOnEnable() {
    // Serial.print("MeasureOnEnable: ");
    // Serial.println(millis());

    ClearStateVector();

    tMeasure.setCallback(&initMeasureCallback);
    tMeasure.forceNextIteration();

    return true;
}

// запускаем активный сенсор на запуск
void initMeasureCallback() {
    // Serial.print("initMeasureCallback: ");
    // Serial.println(millis());

    ClearSensorsData();

    tMeasure.setCallback(&measureCallback);

    // если возвращено false, значит активный сенсор не может быть
    // запущен, пропускаем его
    if (pingTrigger() == false)
        tMeasure.forceNextIteration();
}


void measureCallback() {
    // Serial.print("measureCallback: ");
    // Serial.println(millis());

    // TODO: выведем текущие отклики
    display();

    // заполним текущие отклики в вектор состояния
    CopyActiveDistancesToStateVector(false);

    SensorsRearrange();

    tMeasure.setCallback(&initMeasureCallback);

    // даём релехам время на переключение
    // пока они переключаются, планировщик успеет выполнить другие команды
    tMeasure.delay(RELAY_INTERVAL);
}


void MeasureOnDisable() {
    // Serial.print("MeasureOnDisable: ");
    // Serial.println(millis());

    displayStateVector();
    Serial.println();

    tMeasure.restart();
}



void pingTimeoutCallback() {
    // определяем, с каким датчиком мы имеем дело
    BaseSensor& currentSensor = *( (BaseSensor*) taskManager.currentLts() );

    // Serial.print("pingTimeoutCallback");
    // Serial.print(", id = ");
    // Serial.print(currentSensor.sensorId);
    // Serial.print(", ");
    // Serial.println(millis());

    currentSensor.pulseTimeout = true;
    if (currentSensor.pulseBusy) {
        currentSensor.isrStopClock();
    } 
}



// ----------------------- MAIN CODE --------------------------------

#define TEST_SENSOR 1

void pingTest(bool simultaneous) {
    Serial.println("pingTest()...");

    if (simultaneous == false) {
        uint8_t curSensor = TEST_SENSOR;

        Serial.println(sensors[curSensor]->pulseBusy);
        Serial.println(sensors[curSensor]->pulseTimeout);
        delay(1000);

        while (1) {
            sensors[curSensor]->pingTrig();
            sensors[curSensor]->pingStart();

            delay(MILLIS_TIMEOUT);

            if (sensors[curSensor]->pulseBusy) {
                // Serial.println("Pulse Timeout");

                sensors[curSensor]->pulseTimeout = true;
                sensors[curSensor]->isrStopClock();

                // continue;
            }

            Serial.print(millis());
            Serial.print("  sensor #");
            Serial.print(curSensor);
            Serial.print("  ");
            // Serial.print(sensors[curSensor]->pulseStart);
            // Serial.print("  ");
            // Serial.print(sensors[curSensor]->pulseStop);
            // Serial.print("  ");
            Serial.print(sensors[curSensor]->pingDistance / US_ROUNDTRIP_CM);
            Serial.println();

            // delay(1000);
        }
    } else {
        while (1) {
            for (uint8_t i = 0; i < SENSORS_NUM; i++) {
                sensors[i]->pingTrig();
            }

            for (uint8_t i = 0; i < SENSORS_NUM; i++) 
                sensors[i]->pingStart();

            delay(MILLIS_TIMEOUT);

            for (uint8_t i = 0; i < SENSORS_NUM; i++) {
                if (sensors[i]->pulseBusy) {
                    Serial.println("Pulse Timeout");

                    sensors[i]->pulseTimeout = true;
                    sensors[i]->isrStopClock();

                    // continue;
                }
            }

            for (uint8_t i = 0; i < SENSORS_NUM; i++) {
                Serial.print(millis());
                Serial.print("  sensor #");
                Serial.print(i);
                Serial.print("  ");
                Serial.print(sensors[i]->pulseStart);
                Serial.print("  ");
                Serial.print(sensors[i]->pulseStop);
                Serial.print("  ");
                Serial.print(sensors[i]->pingDistance / US_ROUNDTRIP_CM);
                Serial.println();
            }

            Serial.println();
            delay(100);
        }
    }
}

void test() {
    Serial.println();
    Serial.println();
    Serial.println();

    Serial.println("test()...");

    while (1) {
        Serial.print(millis());
        Serial.println(", Making ping");

        sensors[TEST_SENSOR]->pingTrig();
        sensors[TEST_SENSOR]->pingStart();

        unsigned long duration = micros();

        // while (sensors[TEST_SENSOR]->GetEcho() == HIGH) {
        while (sensors[TEST_SENSOR]->pulseBusy) {
            // Serial.print(millis());
            // // Serial.print(", Echo == HIGH");
            // Serial.print(", pulseBusy, Echo ==");
            // Serial.print(sensors[TEST_SENSOR]->GetEcho());
            // Serial.println();
        }

        // duration = micros() - duration;
        // Serial.print("duration = ");
        // Serial.println(duration);
        // Serial.println("* * * done");

        delay(500);

        Serial.print("duration = ");
        Serial.print(sensors[TEST_SENSOR]->pingDistance);
        Serial.println(" us");

        Serial.println();
        Serial.println();
        Serial.println();
    }


    // while (1) {
    //     sensors[0]->pingTrig();
    // }

    // Serial.println(sensors[0]->pulseBusy);
    // Serial.println(sensors[0]->pulseTimeout);
    // Serial.println();

    // Serial.println(sensors[1]->pulseBusy);
    // Serial.println(sensors[1]->pulseTimeout);
    // Serial.println();

    // sensors[0]->pingTrig();

    // Serial.println(sensors[0]->pulseBusy);
    // Serial.println(sensors[0]->pulseTimeout);
    // Serial.println();

    // Serial.println(sensors[1]->pulseBusy);
    // Serial.println(sensors[1]->pulseTimeout);
    // Serial.println();

    // while (1) {
    //     continue;
    // }

    pingTest(true);
}

void setup() {
    Serial.begin(115200);

    // НЕ ЗАБЫВАЕМ ВОТ ЭТО ЗАКОММЕНТИРОВАТЬ!!!
    // test();

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    // инициализируем состояние сенсоров и реле на то, чтобы 0-ой сенсор был активным
    SetActiveSensor(0);

    // добавим все task к taskManager
    taskManager.addTask(tCycle);
    taskManager.addTask(tMeasure); 

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        taskManager.addTask(*pingTasks[i]);
    } 

    tCycle.enable();
    tMeasure.enable();
}


void loop() {
    taskManager.execute();
}