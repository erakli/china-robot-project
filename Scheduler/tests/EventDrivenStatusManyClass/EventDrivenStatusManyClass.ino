// НЕ РАБОТАЕТ

#define _TASK_STATUS_REQUEST
#define _TASK_WDT_IDS           // Compile with support for Task IDs and Watchdog timer
#define _TASK_LTS_POINTER       // Compile with support for Local Task Storage pointer

#include <TaskScheduler.h>  

#include <DirectIO.h>
#include <EnableInterrupt.h>


#define MILLIS_TIMEOUT  15      // 30 milliseconds or max range of ~5.1 meters
#define US_ROUNDTRIP_CM 57      // Microseconds (uS) it takes sound to travel round-trip 1cm (2cm total), uses integer to save compiled code space. Default=57

#define SENSORS_NUM  2


#define TRIGGERPIN_1 6
#define ECHOPIN_1 7

#define TRIGGERPIN_2 8
#define ECHOPIN_2 9


class Sensor {
public:
    Sensor(u8 trig_pin, u8 echo_pin, StatusRequest *sr);

    bool checkPing();
    void pingTrig();
    void startPing(void (*isrFunc)());

    void pingStartClock(void (*isrFunc)());
    void pingStopClock();

    volatile bool pulseBusy;
    volatile bool pulseTimeout;
    volatile unsigned long pulseStart;
    volatile unsigned long pulseStop;
    volatile unsigned long pingDistance;

    u8 trigPin;
    u8 echoPin;

// private:
    OutputPin pTrig;
    InputPin pEcho;

    StatusRequest *measure;

    unsigned int id;
};

Sensor::Sensor(u8 trig_pin, u8 echo_pin, StatusRequest *sr) :  
    pulseBusy(false),
    pulseTimeout(false),
    pulseStart(0),
    pulseStop(0),
    pingDistance(0),
    trigPin(trig_pin), 
    echoPin(echo_pin),
    pTrig(OutputPin(trig_pin)),
    pEcho(InputPin(echo_pin)),
    measure(sr),
    id(0)
{
    // pTrig = LOW;
    // pEcho = LOW;
}

bool Sensor::checkPing() {
    Serial.print("pulseBusy = "); 
    Serial.print(pulseBusy);
    Serial.print(", pEcho = "); 
    Serial.println(pEcho == HIGH);

    if (pulseBusy)
        return false; // do not trigger if in the middle of a pulse

    if (pEcho == HIGH)
        return false; // do not trigger if ECHO pin is high

    return true;
}

void Sensor::pingTrig() {
    pulseBusy = true;
    pulseTimeout = false;

    pTrig = LOW;
    delayMicroseconds(4);
    pTrig = HIGH;

    delayMicroseconds(10);
    pTrig = LOW;
}

void Sensor::startPing(void (*isrFunc)()) {
    enableInterrupt(echoPin, isrFunc, RISING);
}

void Sensor::pingStartClock(void (*isrFunc)()) {
    pulseStart = micros();

    disableInterrupt(echoPin); 
    enableInterrupt(echoPin, isrFunc, FALLING);
}

void Sensor::pingStopClock() {
    pulseStop = micros();

    disableInterrupt(echoPin); 

    if (pulseTimeout == false) {
        pingDistance = pulseStop - pulseStart;
        measure->signal();   // signal about ping completed
    } else {
        pingDistance = 0;
        measure->signal(-1); // signal about error
    }

    pulseBusy = false;
}





Scheduler r;
StatusRequest measure;

// Callback methods prototypes
void pingTimeoutCallback();

void displayCallback();

void measureCallback();
bool measureEnable();

Task tPing_1(0, TASK_ONCE, &pingTimeoutCallback, &r, false);
Task tPing_2(0, TASK_ONCE, &pingTimeoutCallback, &r, false);

Task tDisplay(&displayCallback, &r);

Task tMeasure(50, TASK_ONCE, &measureCallback, &r, false, &measureEnable);



Sensor sensors[SENSORS_NUM] = {
    Sensor(TRIGGERPIN_1, ECHOPIN_1, &measure),
    Sensor(TRIGGERPIN_2, ECHOPIN_2, &measure)
};

Task     *tasks[] = { &tPing_1, &tPing_2 };

/////////////////////// ISR //////////////////////////////////////////////

// Start clock on the rising edge of the ultrasonic pulse
void pingStartClock_0() {
    sensors[0].pingStartClock(pingStopClock_0);
    tasks[0]->restartDelayed(); // start counting timeout
}

// Stop clock on the falling edge of the ultrasonic pulse
void pingStopClock_0() {
    sensors[0].pingStopClock();
    tasks[0]->disable(); // disable timeout 
}

void pingStartClock_1() {
    sensors[1].pingStartClock(pingStopClock_1);
    tasks[1]->restartDelayed(); // start counting timeout
}

void pingStopClock_1() {
    sensors[1].pingStopClock();
    tasks[1]->disable(); // disable timeout 
}




void pingTrigger(unsigned long aTimeout) {
    Serial.println("pingTrigger");

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        if (sensors[i].checkPing() == false)
            return;
    }

    Serial.println("checkPing... OK");

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        sensors[i].pingTrig();
    }
    
    Serial.println("pingTrig... OK");

    tPing_1.setInterval(aTimeout);
    tPing_2.setInterval(aTimeout);

    tPing_1.restartDelayed(); // timeout countdown starts now
    tPing_2.restartDelayed();

    // will start the pulse clock on the rising edge of ECHO pin
    sensors[0].startPing(pingStartClock_0);
    sensors[1].startPing(pingStartClock_1);
}


////////////////// Callbacks /////////////////////////////////////////////

// Stop clock because of the timeout – the wave did not return
void pingTimeoutCallback() {
    Serial.println("pingTimeoutCallback");

    Task& T = r.currentTask();
    // Another way to get to LTS pointer:  
    Sensor& curSensor = *((Sensor*) T.getLtsPointer());

    curSensor.pulseTimeout = true;
    if (curSensor.pulseBusy) {
        curSensor.pingStopClock();
    } 
}


bool measureEnable() {
    // Serial.println("MeasureEnable: Activating sensors");  

    // when measure signal recieved, display result
    measure.setWaiting(2); // Set the StatusRequest to wait for 3 signals. 
    tDisplay.waitFor(&measure);

    pingTrigger(MILLIS_TIMEOUT);
    
    return true;
}

// Initial measure callback sets the trigger
void measureCallback() {
    // Serial.println("measureCallback");

    if (measure.pending()) {
        // tDisplay.disable();
        measure.signalComplete(-1);  // signal error
        // Serial.print(millis());
        // Serial.print("      ");
        // Serial.println("measureCallback: Timeout!");
        displayCallback();
    }

    tMeasure.restartDelayed();
}


void displayCallback() {
    // char d[256];

    // if (pulseTimeout_1 && pulseTimeout_2)
    //     return;

    unsigned long cm_1 = 0;
    unsigned long cm_2 = 0;

    cm_1 = sensors[0].pingDistance / US_ROUNDTRIP_CM;
    cm_2 = sensors[1].pingDistance / US_ROUNDTRIP_CM;

    // snprintf(
    //     d, 256, 
    //     "pulseStart_1 = %8lu\tpulseStop=%8lu\tdistance, cm=%8lu", 
    //     pulseStart_1, pulseStop, cm);

    // Serial.println(d);

    Serial.print(millis());
    Serial.print("      ");
    Serial.print(cm_1);
    Serial.print("      ");
    Serial.print(cm_2);
    Serial.println();
}




void setup() {
    // put your setup code here, to run once:

    Serial.begin(115200);

    Serial.println(sensors[0].pEcho == HIGH);

    delay(1000);

    // sensors[0].pEcho = LOW;

    delay(1000);

    Serial.println(sensors[0].pEcho == HIGH);

    // sensors[1].pTrig = LOW;

    // delay(1000);

    // sensors[1].pTrig = HIGH;

    // delay(1000);

    // sensors[1].pTrig = LOW;

    // delay(1000);

    // sensors[1].pTrig = HIGH;

    // delay(1000);

    // sensors[1].pTrig = LOW;

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    // sensors[0].pTrig = LOW;

    for (int i = 0; i < SENSORS_NUM; i++) {
        Task& T = *tasks[i];
        T.setLtsPointer( &sensors[i] );
    }

    tMeasure.restartDelayed();
}


void loop() {
    r.execute();
}