#define _TASK_STATUS_REQUEST
#define _TASK_LTS_POINTER       // Compile with support for Local Task Storage pointer
#include <TaskScheduler.h>  

#include <DirectIO.h>

#include "FastUltrasonic.h"


#define PING_INTERVAL	50     // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define MILLIS_TIMEOUT  30     // 30 milliseconds or max range of ~5.1 meters



// будем передавать сюда массив тасков pingTasks чтобы делать сдвиг,
// который позволит делать активным каждый последующий сенсор
void arrayRotateLeft(void** arr, int size) {
	register int i;
	void * temp = arr[0];
	for (i = 0, size--; i < size; i++)
		arr[i] = arr[i + 1];
	arr[size] = temp;
}



// ----------------------- TASKS --------------------------------

// Callback methods prototypes
// void FullCycleCallback(); void FullCycleOnDisable();
void measureCallback(); bool MeasureOnEnable();// bool MeasureOnDisable();
void ProcessPingDistancesCallback();
void pingTimeoutCallback(); void PingTimeoutOnDisable();



// taskManager is a base scheduler
Scheduler taskManager;

StatusRequest allPingsDone;


// Task tCycle(PING_INTERVAL, SENSORS_NUM, &FullCycleCallback, NULL, false, NULL, &FullCycleOnDisable);
Task tMeasure(PING_INTERVAL, TASK_ONCE, &measureCallback, NULL, false, &MeasureOnEnable);
Task tProcessPingDistances(&ProcessPingDistancesCallback);


// объявляем таски для таймаутов по количеству сенсоров
#if defined SENSORS_NUM
    Task tPing_0(TASK_IMMEDIATE, TASK_ONCE, &pingTimeoutCallback, NULL, false, NULL, &PingTimeoutOnDisable);

    #if SENSORS_NUM > 1
        Task tPing_1(TASK_IMMEDIATE, TASK_ONCE, &pingTimeoutCallback);
    #endif

    #if SENSORS_NUM > 2
        Task tPing_2(TASK_IMMEDIATE, TASK_ONCE, &pingTimeoutCallback);
    #endif

    #if SENSORS_NUM > 3
        Task tPing_3(TASK_IMMEDIATE, TASK_ONCE, &pingTimeoutCallback);
    #endif

#endif // defined SENSORS_NUM


#if SENSORS_NUM == 1
    Task *pingTasks[] = { &tPing_0 };

#elif SENSORS_NUM == 2
    Task *pingTasks[] = { &tPing_0, &tPing_1 };

#elif SENSORS_NUM == 3
    Task *pingTasks[] = { &tPing_0, &tPing_1, &tPing_2 };

#elif SENSORS_NUM == 4
    Task *pingTasks[] = { &tPing_0, &tPing_1, &tPing_2, &tPing_3 };

#endif // defined SENSORS_NUM



// ----------------------- FUNCS --------------------------------

// сдвигаем циклически последовательность тасков таким образом,
// чтобы текущий сенсор стартовал первым
void PingTasksRearrange() {
    arrayRotateLeft(pingTasks, SENSORS_NUM);
}



// функция включает сенсоры на запуск процесса посылки ультразвукового пинга
bool pingTrigger(unsigned long aTimeout) {

    // если какой-нибудь из сенсоров ждёт эхо, ничего не делаем
    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        if (sensors[i]->CheckPing() == false) {
            Serial.print("Still waiting id = ");
            Serial.print(i);
            Serial.print(", ");
            Serial.println(millis());
            return false;
        }
    }

    // Serial.println("CheckPing... OK");

    // включааем сенсоры
    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        sensors[i]->pingTrig();
    }

    // Serial.println("pingTrig... OK");

    // запускаем таймаут, после которого считается, что сигнал не был принят
    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        pingTasks[i]->setInterval(aTimeout);
        pingTasks[i]->restartDelayed();
    }

    // включаем прерывания для фиксирования момента старта и последующего
    // вычисления времени пинга
    for (uint8_t i = 0; i < SENSORS_NUM; i++) 
        sensors[i]->pingStart();

    return true;
}



void display() {
    Serial.print(millis());

    unsigned long cm = 0;

    for (uint8_t i = 0; i < SENSORS_NUM; i++)  {
        cm = sensors[i]->pingDistance / US_ROUNDTRIP_CM;
    
        Serial.print("      ");
        Serial.print(cm);
    }

    Serial.println();
}



// ----------------------- CALLBACKS --------------------------------

// void FullCycleCallback() {
//     // Serial.print("FullCycleCallback: ");
//     // Serial.println(millis());

//     if (tCycle.isFirstIteration() == false) {
//         // PingTasksRearrange();
//     }

//     tMeasure.restartDelayed();
// }

// void FullCycleOnDisable() {
//     // Serial.print("FullCycleOnDisable: ");
//     // Serial.println(millis());
//     Serial.println();

//     tCycle.restart();
// }


// не забываем вернуть true, чтобы включить таск и false,
// чтобы пропустить его
bool MeasureOnEnable() {
    // Serial.print("MeasureOnEnable: ");
    // Serial.println(millis());

    allPingsDone.setWaiting(SENSORS_NUM);
    tProcessPingDistances.waitFor(&allPingsDone);

    // если мы ожидаем пинга, перезапустимся
    if (pingTrigger(MILLIS_TIMEOUT) == false) {
        // tMeasure.restart();
    }

    return true;
}

void measureCallback() {
    // Serial.print("measureCallback: ");
    // Serial.print(tMeasure.getIterations());
    // Serial.print(", ");
    // Serial.println(millis());



    // if (tMeasure.isFirstIteration() == false) {
    //     // PingTasksRearrange();
    // }

    // pingTrigger(MILLIS_TIMEOUT);

    // if (tMeasure.isLastIteration()) {
    //     Serial.println("Cycle done ----");
    //     Serial.println();
    //     tMeasure.restartDelayed();
    // }

    if (allPingsDone.pending()) {
        // for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        //     pingTasks[i]->disable();
        // } 

        allPingsDone.signalComplete(-1);  // signal error
    }

    tMeasure.restartDelayed();
}

// bool MeasureOnDisable() {
//     Serial.print("measureCallback: ");
//     Serial.println(millis());
//     PingTasksRearrange();
// }


void ProcessPingDistancesCallback() {
    // Serial.print("measureCallback: ");
    // Serial.println(millis());

    display();
}



void pingTimeoutCallback() {
    // определяем, с каким датчиком мы имеем дело
    Task& currentTask = taskManager.currentTask();
    BaseSensor& currentSensor = *((BaseSensor*) currentTask.getLtsPointer());

    Serial.print("pingTimeoutCallback, id = ");
    Serial.print(currentSensor.sensorId);
    Serial.print(", ");
    Serial.println(millis());

    currentSensor.pulseTimeout = true;
    if (currentSensor.pulseBusy) {
        currentSensor.isrStopClock();
        currentTask.disable();
    } 
}

// при окончании пинга оповещаем об этом
void PingTimeoutOnDisable() {
    allPingsDone.signal();
}



// ----------------------- PING TIMEOUT FUNCS --------------------------------

#define _make_timeout_func(idx) \
    void enableTimeout_##idx() { \
        pingTasks[idx]->restartDelayed(); \
    } \
    \
    void disableTimeout_##idx() { \
        pingTasks[idx]->disable(); \
    }

_make_timeout_func(0)
_make_timeout_func(1)
_make_timeout_func(2)
_make_timeout_func(3)

VoidFunc enableExternalTimeoutFuncs[] = {
    enableTimeout_0,
    enableTimeout_1,
    enableTimeout_2,
    enableTimeout_3
};

VoidFunc disableExternalTimeoutFuncs[] = {
    disableTimeout_0,
    disableTimeout_1,
    disableTimeout_2,
    disableTimeout_3
};



// ----------------------- MAIN CODE --------------------------------

#define TEST_SENSOR 0

void pingTest(bool simultaneous) {
    Serial.println("pingTest()...");

    if (simultaneous == false) {
        uint8_t curSensor = TEST_SENSOR;

        Serial.println(sensors[curSensor]->pulseBusy);
        Serial.println(sensors[curSensor]->pulseTimeout);
        delay(1000);

        while (1) {
            sensors[curSensor]->pingTrig();
            sensors[curSensor]->pingStart();

            delay(MILLIS_TIMEOUT);

            if (sensors[curSensor]->pulseBusy) {
                // Serial.println("Pulse Timeout");

                sensors[curSensor]->pulseTimeout = true;
                sensors[curSensor]->isrStopClock();

                // continue;
            }

            Serial.print(millis());
            Serial.print("  sensor #");
            Serial.print(curSensor);
            Serial.print("  ");
            // Serial.print(sensors[curSensor]->pulseStart);
            // Serial.print("  ");
            // Serial.print(sensors[curSensor]->pulseStop);
            // Serial.print("  ");
            Serial.print(sensors[curSensor]->pingDistance / US_ROUNDTRIP_CM);
            Serial.println();

            // delay(1000);
        }
    } else {
        while (1) {
            for (uint8_t i = 0; i < SENSORS_NUM; i++) {
                sensors[i]->pingTrig();
            }

            for (uint8_t i = 0; i < SENSORS_NUM; i++) 
                sensors[i]->pingStart();

            delay(MILLIS_TIMEOUT);

            for (uint8_t i = 0; i < SENSORS_NUM; i++) {
                if (sensors[i]->pulseBusy) {
                    Serial.println("Pulse Timeout");

                    sensors[i]->pulseTimeout = true;
                    sensors[i]->isrStopClock();

                    // continue;
                }
            }

            for (uint8_t i = 0; i < SENSORS_NUM; i++) {
                Serial.print(millis());
                Serial.print("  sensor #");
                Serial.print(i);
                Serial.print("  ");
                Serial.print(sensors[i]->pulseStart);
                Serial.print("  ");
                Serial.print(sensors[i]->pulseStop);
                Serial.print("  ");
                Serial.print(sensors[i]->pingDistance / US_ROUNDTRIP_CM);
                Serial.println();
            }

            Serial.println();
            delay(100);
        }
    }
}

void test() {
    Serial.println();
    Serial.println();
    Serial.println();

    Serial.println("test()...");

    // while (1) {
    //     sensors[0]->pingTrig();
    // }

    // Serial.println(sensors[0]->pulseBusy);
    // Serial.println(sensors[0]->pulseTimeout);
    // Serial.println();

    // Serial.println(sensors[1]->pulseBusy);
    // Serial.println(sensors[1]->pulseTimeout);
    // Serial.println();

    // sensors[0]->pingTrig();

    // Serial.println(sensors[0]->pulseBusy);
    // Serial.println(sensors[0]->pulseTimeout);
    // Serial.println();

    // Serial.println(sensors[1]->pulseBusy);
    // Serial.println(sensors[1]->pulseTimeout);
    // Serial.println();

    // while (1) {
    //     continue;
    // }

    pingTest(true);
}

void setup() {
    Serial.begin(115200);

    // test();

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    // добавим все task к taskManager
    // taskManager.addTask(tCycle);
    taskManager.addTask(tMeasure);
    taskManager.addTask(tProcessPingDistances);

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        // зарегестрируем друг у друга pingTasks и sensors
        sensors[i]->enableExternalTimeout = enableExternalTimeoutFuncs[i];
        sensors[i]->disableExternalTimeout = disableExternalTimeoutFuncs[i];

        // Serial.print(i); 
        // Serial.print(" enable = ");
        // Serial.print(sensors[i]->enableExternalTimeout != NULL); 
        // Serial.print(" disable = ");
        // Serial.print(sensors[i]->disableExternalTimeout != NULL); 
        // Serial.println(); 

        pingTasks[i]->setLtsPointer( sensors[i] );

        taskManager.addTask(*pingTasks[i]);
    } 

    // tCycle.enable();
    tMeasure.enable();
}


void loop() {
    taskManager.execute();
}