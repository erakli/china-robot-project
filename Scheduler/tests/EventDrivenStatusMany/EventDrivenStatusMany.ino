#define _TASK_STATUS_REQUEST

#include <TaskScheduler.h>  

#include <DirectIO.h>
#include <EnableInterrupt.h>

#define TRIGGERPIN_1 6
#define ECHOPIN_1 7
// #define ECHOPIN_1 8

#define TRIGGERPIN_2 8
#define ECHOPIN_2 9
// #define ECHOPIN_2 9


Output<TRIGGERPIN_1> pTrigger_1;
Output<TRIGGERPIN_2> pTrigger_2;

// pins 6, 7 (bit(0), bit(1))
// OutputPort<PORT_D, 6, 2> portTrigger;

Input<ECHOPIN_1> pEcho_1;
Input<ECHOPIN_2> pEcho_2;



#define MILLIS_TIMEOUT  15      // 30 milliseconds or max range of ~5.1 meters
#define US_ROUNDTRIP_CM 57      // Microseconds (uS) it takes sound to travel round-trip 1cm (2cm total), uses integer to save compiled code space. Default=57


Scheduler r;
StatusRequest measure;


// Callback methods prototypes
void measureCallback();
void pingTimeoutCallback_1();
void pingTimeoutCallback_2();
void displayCallback();

bool measureEnable();

Task tMeasure(50, TASK_ONCE, &measureCallback, &r, false, &measureEnable);
Task tPing_1(0, TASK_ONCE, &pingTimeoutCallback_1, &r, false);
Task tPing_2(0, TASK_ONCE, &pingTimeoutCallback_2, &r, false);
Task tDisplay(&displayCallback, &r);


volatile bool pulseBusy_1 = false;
volatile bool pulseTimeout_1 = false;
volatile unsigned long pulseStart_1 = 0;
volatile unsigned long pulseStop_1 = 0;
volatile unsigned long pingDistance_1 = 0;

volatile bool pulseBusy_2 = false;
volatile bool pulseTimeout_2 = false;
volatile unsigned long pulseStart_2 = 0;
volatile unsigned long pulseStop_2 = 0;
volatile unsigned long pingDistance_2 = 0;


void pingTrigger(unsigned long aTimeout) {
    if (pulseBusy_1 || pulseBusy_2)
        return; // do not trigger if in the middle of a pulse

    if (pEcho_1 == HIGH || pEcho_2 == HIGH)
        return; // do not trigger if ECHO pin is high

    pulseBusy_1 = true;
    pulseTimeout_1 = false;

    pulseBusy_2 = true;
    pulseTimeout_2 = false;


    pTrigger_1 = LOW;
    delayMicroseconds(4);
    pTrigger_1 = HIGH;

    delayMicroseconds(10);
    pTrigger_1 = LOW;


    pTrigger_2 = LOW;
    delayMicroseconds(4);
    pTrigger_2 = HIGH;

    delayMicroseconds(10);
    pTrigger_2 = LOW;


    // portTrigger = LOW;
    // delayMicroseconds(4);
    // portTrigger = bit(0);

    // delayMicroseconds(10);
    // portTrigger = LOW;


    // delayMicroseconds(4);
    // portTrigger = bit(1);

    // delayMicroseconds(10);
    // portTrigger = LOW;


    tPing_1.setInterval(aTimeout);
    tPing_2.setInterval(aTimeout);

    tPing_1.restartDelayed(); // timeout countdown starts now
    tPing_2.restartDelayed();


    // will start the pulse clock on the rising edge of ECHO pin
    enableInterrupt(ECHOPIN_1, &pingStartClock_1, RISING);
    enableInterrupt(ECHOPIN_2, &pingStartClock_2, RISING);
}



/////////////////////// ISR //////////////////////////////////////////////

// Start clock on the rising edge of the ultrasonic pulse
void pingStartClock_1() {
    pulseStart_1 = micros();

    disableInterrupt(ECHOPIN_1); 
    enableInterrupt(ECHOPIN_1, &pingStopClock_1, FALLING);

    // start counting timeout
    tPing_1.restartDelayed();
}

// Stop clock on the falling edge of the ultrasonic pulse
void pingStopClock_1() {
    pulseStop_1 = micros();

    disableInterrupt(ECHOPIN_1); 

    if (pulseTimeout_1 == false) {
        pingDistance_1 = pulseStop_1 - pulseStart_1;
        measure.signal();   // signal about ping completed
    } else {
        pingDistance_1 = 0;
        measure.signal(-1); // signal about error
    }

    pulseBusy_1 = false;
    tPing_1.disable(); // disable timeout 
}



void pingStartClock_2() {
    pulseStart_2 = micros();
    
    disableInterrupt(ECHOPIN_2); 
    enableInterrupt(ECHOPIN_2, &pingStopClock_2, FALLING);

    // start counting timeout
    tPing_2.restartDelayed();
}

void pingStopClock_2() {
    pulseStop_2 = micros();

    disableInterrupt(ECHOPIN_2); 

    if (pulseTimeout_2 == false) {
        pingDistance_2 = pulseStop_2 - pulseStart_2;
        measure.signal();   // signal about ping completed
    } else {
        pingDistance_2 = 0;
        measure.signal(-1); // signal about error
    }

    pulseBusy_2 = false;
    tPing_2.disable(); // disable timeout 
}



////////////////// Callbacks /////////////////////////////////////////////

// Stop clock because of the timeout – the wave did not return
void pingTimeoutCallback_1() {
    pulseTimeout_1 = true;
    if (pulseBusy_1) {
        pingStopClock_1();
    } 
}

void pingTimeoutCallback_2() {
    pulseTimeout_2 = true;
    if (pulseBusy_2) {
        pingStopClock_2();
    } 
}

bool measureEnable() {
    // Serial.println("MeasureEnable: Activating sensors");  

    // when measure signal recieved, display result
    measure.setWaiting(2); // Set the StatusRequest to wait for 3 signals. 
    tDisplay.waitFor(&measure);

    pingTrigger(MILLIS_TIMEOUT);
    
    return true;
}

// Initial measure callback sets the trigger
void measureCallback() {
    // Serial.println("measureCallback");

    if (measure.pending()) {
        // tDisplay.disable();
        measure.signalComplete(-1);  // signal error
        // Serial.print(millis());
        // Serial.print("      ");
        // Serial.println("measureCallback: Timeout!");
        displayCallback();
    }

    tMeasure.restartDelayed();
}


void displayCallback() {
    // char d[256];

    // if (pulseTimeout_1 && pulseTimeout_2)
    //     return;

    unsigned long cm_1 = 0;
    unsigned long cm_2 = 0;

    cm_1 = pingDistance_1 / US_ROUNDTRIP_CM;
    cm_2 = pingDistance_2 / US_ROUNDTRIP_CM;

    // snprintf(
    //     d, 256, 
    //     "pulseStart_1 = %8lu\tpulseStop=%8lu\tdistance, cm=%8lu", 
    //     pulseStart_1, pulseStop, cm);

    // Serial.println(d);

    Serial.print(millis());
    Serial.print("      ");
    Serial.print(cm_1);
    Serial.print("      ");
    Serial.print(cm_2);
    Serial.println();
}




void setup() {
    // put your setup code here, to run once:

    Serial.begin(115200);

    Serial.println();
    Serial.println();
    Serial.println();
    Serial.println("Inintialized.");
    delay(1000);

    pTrigger_1 = LOW;
    pTrigger_2 = LOW;

    // portTrigger = LOW;

    pEcho_1 = LOW;
    pEcho_2 = LOW;

    tMeasure.restartDelayed();
}


void loop() {
    r.execute();
}