#include <fstream>
#include <conio.h>
#include <iomanip>
#include <string.h>
#include <stdlib.h>
#include <dos.h>
#include <stdio.h>
#define _USE_MATH_DEFINES 1;
#include <math.h>
#include <sys\stat.h>

// Объявления глобальных переменных
int K;
double D[10], FI, DL, W, Wz, T, t, dt, tv, pi, M, gr;
double k, ta, kr, kv, tav, krv; // Углы курса,тангажа и крена
double vr, pr, ny, L0, L1, L2, L3, L0n, L1n, L2n, L3n, dL0z, dL1z, dL2z, dL3z;
double w1, w2, w3, dL0, dL1, dL2, dL3, Nx, Ny, Nz;
double ws1, ws2, ws3, dF, dF1, dF2, dF3, wg, wv;
double Omega; // Угловая скорость вращения Земли
double phi;   // Широта места
char s[12];
FILE *Fk, *Fr;

/**
 * Функция программы
 */
void main(void)
{
    // Задание начальных значений переменных
    gr = 180. / M_PI;                    // Количество градусов в радиане
    dt = 0.02;                           // Интервал дискретности измерений
    tv = 0.;                             // Счетчик интервала вывода результатов
    Omega = 7.2921158553E-5;             // Угловая скорость вращения Земли
    phi = 55.45;                         // Широта места наблюдения
    wg = Omega * cos(phi * M_PI / 180.); // Горизонтальная проекция угловой скорости вращения Земли на широте phi
    wv = Omega * sin(phi * M_PI / 180.); // Вертикальная проекция угловой скорости вращения Земли на широте phi

    // Открытие файла данных БИМС-Т
    if ((Fk = fopen("T.RES", "r")) == NULL)
        perror("Не удалось открыть для чтения файл исходных данных");

    //Открытие файла для записи результатов вычислений
    if ((Fr = fopen("RESULT.DAT", "w")) == NULL)
        perror("Не удалось открыть для записи файл результатов");

    // Считывание первичных данных и определение начального кватерниона
    fscanf(Fk, "%*8s%lf%lf%lf%lf%lf%lf%lf%lf%lf", &k, &ta, &kr, &w1, &w2, &w3, &Nx, &Ny, &Nz);
    
    //Перевод в радианы, угол курса с обратным знаком
    k = -k / gr;
    ta = ta / gr;
    kr = kr / gr;

    //Начальный кватернион
    L0 = cos(k / 2) * cos(ta / 2) * cos(kr / 2) - sin(k / 2) * sin(ta / 2) * sin(kr / 2);
    L1 = cos(k / 2) * cos(ta / 2) * sin(kr / 2) + sin(k / 2) * sin(ta / 2) * cos(kr / 2);
    L2 = sin(k / 2) * cos(ta / 2) * cos(kr / 2) + cos(k / 2) * sin(ta / 2) * sin(kr / 2);
    L3 = cos(k / 2) * sin(ta / 2) * cos(kr / 2) - sin(k / 2) * cos(ta / 2) * sin(kr / 2);

    //Цикл рекуррентных вычислений с одновременным считыванием данных из файла БИМС-Т
    while (fscanf(Fk, "%8s%lf%lf%lf%lf%lf%lf%lf%lf%lf", &s, &k, &ta, &kr, &w1, &w2, &w3, &Nx, &Ny, &Nz) != EOF)
    {
        //Перевод угловых скоростей в рад/сек и коррекция знаков (по угловой скорости по курсу)
        w1 = w1 / gr;
        w2 = -w2 / gr;
        w3 = w3 / gr;

        //Запоминание предыдущих значений угловых скоростей
        ws1 = w1;
        ws2 = w2;
        ws3 = w3;

        //Вычисление курса,тангажа,крена
        if (t >= tv)
        {
            tv = tv + 0.5;
            kv = atan2(-(2 * L1 * L3 - 2 * L0 * L2), (2 * L1 * L1 + 2 * L0 * L0 - 1));
            tav = asin(2 * L1 * L2 + 2 * L0 * L3);
            krv = atan2(-(2 * L2 * L3 - 2 * L0 * L1), (2 * L2 * L2 + 2 * L0 * L0 - 1));

            // Вывод на экран вычисленных и полученных из БИМС-Т углов ориентации с интервалом tv сек.
            printf("%10.5f%10.5f%10.5f%10.5f%10.5f%10.5f%12s%8.2f\n", kv * gr, tav * gr, krv * gr, k, ta, kr, s, t);

            // Запись в файл вычисленных и полученных из БИМС-Т углов ориентации
            fprintf(Fr, "%10.5f%10.5f%10.5f%10.5f%10.5f%10.5f%12s%8.2f\n", kv * gr, tav * gr, krv * gr, k, ta, kr, s, t);
        }

        //Вычисление приращения вектора ориентации
        dF1 = w1 * dt + dt * dt * (ws2 * w3 - ws3 * w2) / 24;
        dF2 = w2 * dt + dt * dt * (ws3 * w1 - ws1 * w3) / 24;
        dF3 = w3 * dt + dt * dt * (ws1 * w2 - ws2 * w1) / 24;

        //Вычисление приращения кватерниона
        dF = dF1 * dF1 + dF2 * dF2 + dF3 * dF3;
        dL0 = 1 - dF / 8 + dF * dF / 384;
        dL1 = dF1 * (0.5 - dF / 48);
        dL2 = dF2 * (0.5 - dF / 48);
        dL3 = dF3 * (0.5 - dF / 48);

        // Поворот текущего кватерниона
        L0n = L0 * dL0 - L1 * dL1 - L2 * dL2 - L3 * dL3;
        L1n = L0 * dL1 + L1 * dL0 + L2 * dL3 - L3 * dL2;
        L2n = L0 * dL2 + L2 * dL0 + L3 * dL1 - L1 * dL3;
        L3n = L0 * dL3 + L3 * dL0 + L1 * dL2 - L2 * dL1;

        // Кватернион поворота Земли
        dL0z = 1;
        dL1z = wg * dt / 2;
        dL2z = wv * dt / 2;
        dL3z = 0;

        //Доворот текущего кватерниона для исключения поворота Земли
        L0 = L0n * dL0z - L1n * dL1z - L2n * dL2z - L3n * dL3z;
        L1 = L0n * dL1z + L1n * dL0z + L2n * dL3z - L3n * dL2z;
        L2 = L0n * dL2z + L2n * dL0z + L3n * dL1z - L1n * dL3z;
        L3 = L0n * dL3z + L3n * dL0z + L1n * dL2z - L2n * dL1z;

        // Наращивание времени моделирования
        t = t + dt;
    }

    // Закрытие файлов
    fclose(Fk);
    fclose(Fr);
    getch();
}
