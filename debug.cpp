#include "debug.h"

char argBuffer[ARG_BUFFER_SIZE];
uint16_t bufPos;

#if defined(DEBUG) && defined(TRACING)
    uint8_t depthLevel = 0;

    extern const uint8_t NewLine_None = 0;
    extern const uint8_t NewLine_Before = 1;
    extern const uint8_t NewLine_After = 2;
    extern const uint8_t NewLine_Both = 3;
#endif
