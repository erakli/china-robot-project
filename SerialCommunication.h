#ifndef SERIAL_COMMUNICATION_H
#define SERIAL_COMMUNICATION_H

#include <Arduino.h>  // Serial..

// on Arduino Mega we have several serial ports, so define
// macro to make affinity
#ifdef ENABLE_BLUETOOTH 
#define BLUETOOTH_PORT  Serial1 
#endif

void openPorts(unsigned long baudrate);

#endif
