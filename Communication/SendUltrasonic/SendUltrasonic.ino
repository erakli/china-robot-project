#include "SensorsManager.h"
#include "StateVectorMessage.h"
#include "DrivingModule.h"

#define BAUDRATE 115200

// Чтобы отключить тестовый вывод в Serial, закомментируй это
// #define DEBUG

#define BUFFER_SIZE 256

#define Speed 60
char com;

// всю магию будет творить вот этот вот красавчик
// перед использованием надо заложить в него массивы датчиков и реле
SensorsManager sensorsManager;

StateVectorMessage message;
uint8_t buffer[BUFFER_SIZE];
uint8_t messageSize;

#ifdef DEBUG
uint8_t *ptr;
uint16_t bytesRead;
StateVectorMessage readMessage;
#endif

uint16_t *stateVector;

#ifdef DEBUG
void PrintMessage(const StateVectorMessage &_message)
{
    Serial.println("PrintMessage");
    Serial.print("  stateVector: ");
    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++)
    {
        Serial.print(_message.ultrasonicTimes[i] / 57);
        Serial.print("  ");
    }
    Serial.println();

    Serial.print("  yaw: ");
    Serial.print(_message.yaw);

    Serial.println();
    Serial.println();
}
#endif

void setup()
{
    message.yaw = 0;
    pinMode(DIR_RIGHT, OUTPUT);
    pinMode(DIR_LEFT, OUTPUT);

    Serial.begin(BAUDRATE);
}

void loop()
{
    sensorsManager.MeasurementCycle();
    stateVector = sensorsManager.GetStateVector();

    // TODO: считаем вектор состояния и отобразим напрямую
    // Serial.print("  stateVector: ");
    // for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
    //     Serial.print(stateVector[i] / 57);
    //     Serial.print("  ");
    // }
    // Serial.println();

    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++)
    {
        message.ultrasonicTimes[i] = stateVector[i];
    }
    message.yaw = 0;

#ifdef DEBUG
    // for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++)
    // {
    //     Serial.print("BEFOR_1 = ");
    //     Serial.print(message.ultrasonicTimes[i] / 57);
    //     Serial.print("  ");
    // }

    // Serial.print("message.yaw BEFOR = ");
    // Serial.println(message.yaw);
#endif

    messageSize = packStateVectorMessage(buffer, message);

#ifdef DEBUG
    PrintMessage(message);

    // for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++)
    // {
    //     Serial.print("BEFOR_2 = ");
    //     Serial.print(message.ultrasonicTimes[i] / 57);
    //     Serial.print("  ");
    // }

    // Serial.print("message.yaw BEFOR_2  = ");
    // Serial.println(message.yaw);

       //    if (message.yaw != 0) {
    //        Serial.print(millis());
    //        Serial.println(", message.yaw != 0");
    //
    //        PrintMessage(message);
    //
    //        delay(250);
    //
    //        ptr = unpackStateVectorMessage(buffer, BUFFER_SIZE, bytesRead, readMessage);
    //        PrintMessage(readMessage);
    //
    //        delay(250);
    //    }
    // ptr = unpackStateVectorMessage(buffer, BUFFER_SIZE, bytesRead, readMessage);
    // PrintMessage(readMessage);

    // for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++)
    // {
    //     Serial.print("AFTER = ");
    //     Serial.print(readMessage.ultrasonicTimes[i] / 57);
    //     Serial.print("  ");
    // }

    // Serial.print("readMessage.yaw = ");
    // Serial.println(readMessage.yaw);
#else
    Serial.write(buffer, messageSize);
#endif

    if (Serial.available())
    {
        com = Serial.read();

        while (Serial.available())
            Serial.read();
            
        Serial.print("Command received: ");
        Serial.println(com);
        switch (com)
        {
        case '4':
            setCommand('L', Speed);
            break;

        case '6':
            setCommand('R', Speed);
            break;

        case '5':
            setCommand('S', 0);
            break;

        case '8':
            setCommand('F', Speed);
            break;

        case '2':
            setCommand('B', Speed);
            break;
        }
    }
}
