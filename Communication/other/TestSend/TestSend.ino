
#include <string.h>

#define BAUDRATE  9600

#define SOP '<'                 // start of packet
#define EOP '>'                 // end of packet
#define SEPARATE_SYMBOL ','    

#define STATE_VECTOR_SIZE   16

#define MAX_NUM             10


uint16_t stateVector[STATE_VECTOR_SIZE];
uint8_t num = 0;



void setup() {
  // put your setup code here, to run once:
  Serial.begin(BAUDRATE);
}

void loop() {
  // put your main code here, to run repeatedly:
  num = (num + 1) % MAX_NUM;
  FillStateVector(num);
  SendStateVector();
  delay(1000);
}



void SendStateVector() {
    char temp[256];
    char buf[256];

    memset( buf, '\0', sizeof( buf ) );

    // start of packet
//    sprintf(temp, "%c", SOP);
//    strcat(buf, temp);
    Serial.print(SOP);

    for(uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        Serial.print(stateVector[i]);
        Serial.print(SEPARATE_SYMBOL);
//        sprintf(temp, "%u,", stateVector[i]);
//        strcat(buf, temp);
    }
//    sprintf(temp, "%c", EOP);
//    strcat(buf, temp);
//    Serial.write(buf);
    Serial.println();
    
    delay(10);        //added a delay to eliminate missed transmissions
}

void FillStateVector(uint8_t num) {
  for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
    stateVector[i] = num;
  }
}
