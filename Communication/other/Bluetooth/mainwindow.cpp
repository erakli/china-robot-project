#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include <QSerialPort>
//#include <QSerialPortInfo>
#include <QObject>
#include <QDebug>

QSerialPort *serial;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    serial = new QSerialPort(this);
    serialBuffer = "";

    bool serial_is_available = false;
    QString arduino_uno_port_name;

//    foreach(const QSerialPortInfo &serialPortInfo,QSerialPortInfo::availablePorts())
//    {
//        if(serialPortInfo.hasProductIdentifier() && serialPortInfo.hasVendorIdentifier())
//        {
////            if(serialPortInfo.productIdentifier() == arduino_uno_product_id &&
////                    serialPortInfo.vendorIdentifier() == arduino_uno_vendor_id)
////            {
////                serial_is_available = true;
////                arduino_uno_port_name = serialPortInfo.portName();
////            }

//        }
//    }

    serial_is_available = true;

    if(serial_is_available)
    {

        serial->setPortName("COM18");
        serial->setBaudRate(QSerialPort::Baud9600);
        serial->setDataBits(QSerialPort::Data8);
        serial->setParity(QSerialPort::NoParity);
        serial->setStopBits(QSerialPort::OneStop);
        serial->setFlowControl(QSerialPort::NoFlowControl);
        serial->open(QSerialPort::ReadWrite);
//        if(serial->open(QIODevice::ReadWrite))
//        {
//           qDebug() << serial->readAll();
//        }
//        else
//        {
//            qDebug() << serial->errorString();
//        }
//        if(!serial->open((QIODevice::ReadOnly)))
//        {
//            qDebug() << serial->errorString();
//        }
     //  serial->write("8");
        QObject::connect(serial,SIGNAL(readyRead()),this,SLOT(readSerial()));
        QObject::connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_clicked()));
        QObject::connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_2_clicked()));
        QObject::connect(ui->pushButton_3,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_3_clicked()));
        QObject::connect(ui->pushButton_4,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_4_clicked()));
        QObject::connect(ui->pushButton_5,SIGNAL(clicked(bool)),this,SLOT(on_pushButton_5_clicked()));
    }
    else
    {
        qDebug() << "Serial is not available";
    }

}

MainWindow::~MainWindow()
{
    delete ui;
    serial->close();
}

void MainWindow::serialReceived()
{
//    ui->label->setText(serial->readAll());
//    qDebug() << "dfsdf";
}

void MainWindow::readSerial()
{
    serialData = serial->readAll();
    serialBuffer = QString::fromStdString(serialData.toStdString());
    qDebug() << serialBuffer;
    ui->label->setText(serialBuffer);
  //  ui->lineEdit->setText(temp);
}



void MainWindow::on_pushButton_clicked()
{
    serial->write("8");

}

void MainWindow::on_pushButton_3_clicked()
{
    serial->write("5");
}

void MainWindow::on_pushButton_2_clicked()
{
    serial->write("2");
}

void MainWindow::on_pushButton_4_clicked()
{
    serial->write("6");
}

void MainWindow::on_pushButton_5_clicked()
{
    serial->write("4");
}
