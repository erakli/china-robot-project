

#define SIZE    3

struct MessageData {
    uint16_t arr[SIZE];
} __attribute__(( packed, aligned(1) )) messageData;

union Message {
    MessageData messageData;
    uint8_t byteArray[sizeof(MessageData)];
} message;



void setup() {
    for (uint8_t i = 0; i < SIZE; i++)
        messageData.arr[i] = i;

    message.messageData = messageData;

    Serial.begin(9600);
}

void loop() {
    Serial.write(message.byteArray, sizeof(MessageData));
    delay(1000);
}
