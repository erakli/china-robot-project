﻿#include "StateVectorMessage.h"

#ifdef QT_SIDE
using namespace std;
#endif


uint8_t * serialize_StateVectorMessage(uint8_t *buffer, const StateVectorMessage &message) {
    // message start
    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        buffer = serialize_type(buffer, message.ultrasonicTimes[i]);
    }

    buffer = serialize_type(buffer, message.yaw);

    return buffer;
}

uint8_t * deserialize_StateVectorMessage(uint8_t *buffer, StateVectorMessage &message) {
    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        buffer = deserialize_type(buffer, message.ultrasonicTimes[i]);
    }

    buffer = deserialize_type(buffer, message.yaw);

    return buffer;
}



uint8_t packStateVectorMessage(uint8_t *buffer, const StateVectorMessage &message) {
    // message start
    buffer[0] = START_BYTE1;
    buffer[1] = START_BYTE2;

    uint8_t * p_dataBegin = buffer + PACKAGE_START_SIZE;
    uint8_t * p_dataEnd = serialize_StateVectorMessage(p_dataBegin, message);

    // добавим контрольную сумму
    uint8_t * p_packageEnd = p_dataEnd;
    p_packageEnd[0] = evalCheckSum(p_dataBegin, p_packageEnd - p_dataBegin);
    p_packageEnd[1] = END_BYTE;
    p_packageEnd += PACKAGE_END_SIZE;

    return (p_packageEnd - buffer);
}

// читаем только полные сообщения
uint8_t * unpackStateVectorMessage(uint8_t *buffer, uint16_t bufferSize, uint16_t &bytesRead, StateVectorMessage &message) {
    uint16_t bytesGot = 0;
    uint16_t index = 0;
    bool messageDetected = false;

    // ищем начало
    while (index < bufferSize && messageDetected == false) {
        if (bytesGot == 0 && buffer[index] == START_BYTE1) {
            bytesGot++;
        } else if (bytesGot == 1 && buffer[index] == START_BYTE2) {
            bytesGot++;
            messageDetected = true;
        }

        index++;
    }

    // сохраняем размер StateVectorMessage 
    static const uint8_t messageSize = sizeof(StateVectorMessage);

    // если мы не нашли начало сообщения или размер оставшегося буфера меньше
    // размера данных + END_BYTE, то расшифровка неудалась
    if (messageDetected == false || 
        bufferSize - index < messageSize + PACKAGE_END_SIZE) 
    {
        bytesRead = index;
        return NULL;
    }

    // ищем конец сообщения
    bool messageEndDetected = 
        (buffer[index + messageSize + PACKAGE_END_SIZE - 1] == 
        END_BYTE);
        
    if (messageEndDetected == false) {
        bytesRead = index;
        return NULL;
    }

    // сместимся на позицию после стартовых байтов
    uint8_t * p_dataBegin = buffer + index; // позиция старта данных
    uint8_t * p_dataEnd = deserialize_StateVectorMessage(p_dataBegin, message); // позиция после конца данных

    // TODO: stopped here. need to check with message. not working
    // TODO: читаем первый байт после данных - должен быть контрольной суммой
    uint8_t messageCheckSum = p_dataEnd[0];
    uint8_t checkSum = evalCheckSum(p_dataBegin, p_dataEnd - p_dataBegin);

    // если контрольные суммы не совпали, то сообщение не расшифровано
    if (messageCheckSum != checkSum) {
        bytesRead = index;
        return NULL;
    }

    p_dataEnd += PACKAGE_END_SIZE;  // так как конец заведомо был найден
    bytesRead = p_dataEnd - buffer;

    return buffer;
}
