#ifndef TEST_MESSAGE_H
#define TEST_MESSAGE_H 

#include "serialization.h"



#define SIZE 5

// упакованная структура
#ifdef QT_SIDE
    #pragma pack(1)
#endif
struct Message {
    std::uint16_t intVar[SIZE];
    std::uint32_t longVar;
}
#ifdef QT_SIDE
;
    #pragma pack()
#else
    __attribute__( (packed, aligned(1)) ) 
;
#endif


std::uint8_t * serialize_Message(std::uint8_t *buffer, const Message &message);
std::uint8_t * deserialize_Message(std::uint8_t *buffer, Message &message);

std::uint8_t packMessage(std::uint8_t *buffer, const Message &message);
std::uint8_t * unpackMessage(std::uint8_t *buffer, std::uint16_t bufferSize, std::uint16_t &bytesRead, Message &message);

#endif