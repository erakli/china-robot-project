#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#ifndef ARDUINO
#define QT_SIDE 
#endif 

#ifdef QT_SIDE
    #include <cstdint>
#else
    #include <Arduino.h> // для типов
    namespace std {
        typedef uint8_t uint8_t;
        typedef uint16_t uint16_t;
        typedef uint32_t uint32_t;
    }
#endif


#define START_BYTE1         '<'
#define START_BYTE2         '#'
#define END_BYTE            '>'

#define PACKAGE_START_SIZE  2
#define PACKAGE_END_SIZE    2


template <typename T>
std::uint8_t * serialize_type(std::uint8_t *buffer, const T &value) {
    std::uint8_t typeSize = sizeof(T);
    switch (typeSize) {
        case 1:
            buffer[0] = value;
        break;

        case 2:
            buffer[0] = value >> 8;
            buffer[1] = value;
        break;

        case 4:
            buffer[0] = value >> 24;
            buffer[1] = value >> 16;
            buffer[2] = value >> 8;
            buffer[3] = value;
        break;
    }

    return (buffer + typeSize);
}



template <typename T>
std::uint8_t * deserialize_type(std::uint8_t *buffer, T &value) {
    std::uint8_t typeSize = sizeof(T);
    switch (typeSize) {
        case 1:
            value = 
                (buffer[0]);
        break;

        case 2:
            value = 
                (buffer[0] << 8) | 
                (buffer[1]);
        break;

        case 4:
            value = 
                (buffer[0] << 24) | 
                (buffer[1] << 16) | 
                (buffer[2] << 8) | 
                (buffer[3]);
        break;
    }

    return (buffer + typeSize);
}

std::uint8_t evalCheckSum(std::uint8_t *buffer, std::uint16_t bufferSize);

// std::uint8_t * serialize_uint8(std::uint8_t *buffer, std::uint8_t value);
// std::uint8_t * serialize_uint16(std::uint8_t *buffer, std::uint16_t value);
// std::uint8_t * serialize_uint32(std::uint8_t *buffer, std::uint32_t value);

// std::uint8_t * deserialize_uint8(std::uint8_t *buffer, std::uint8_t &value);
// std::uint8_t * deserialize_uint16(std::uint8_t *buffer, std::uint16_t &value);
// std::uint8_t * deserialize_uint32(std::uint8_t *buffer, std::uint32_t &value);


#endif // SERIALIZATION_H
