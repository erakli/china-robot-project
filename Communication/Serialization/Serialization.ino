// #include "TestMessage.h"
#include "StateVectorMessage.h"

#define BAUDRATE            9600
#define BLUETOOTH_PORT      Serial

// #define SIZE 5
// #define BUFFER_SIZE 32

// // упакованная структура
// struct Message {
//     uint16_t intVar[SIZE];
//     uint32_t longVar;
// } __attribute__( (packed, aligned(1)) ) ;


#define BUFFER_SIZE 32

// Message message;
StateVectorMessage message;
uint8_t buffer[BUFFER_SIZE];
uint8_t messageSize;
uint16_t num = 0;
uint16_t tempNum;

void setup() {
    Serial.begin(BAUDRATE);
    // BLUETOOTH_PORT.begin(BAUDRATE);
}

void loop() {
    tempNum = num + 255;

    // for (uint8_t i = 0; i < SIZE; i++) {
    //     message.intVar[i] = i;
    //     message.intVar[i] += tempNum;
    // }
    // message.longVar = 32 + tempNum + 65535;

    for (uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        message.ultrasonicTimes[i] = i;
        message.ultrasonicTimes[i] += tempNum;
    }
    message.yaw = num;

    // messageSize = packMessage(buffer, message);
    messageSize = packStateVectorMessage(buffer, message);

    Serial.write(buffer, messageSize);

    // Serial.print(tempNum);
    // BLUETOOTH_PORT.write(buffer, messageSize);

    // Serial.println(messageSize);
    // Serial.println(buffer[messageSize - 1]);
    // Serial.println(buffer[messageSize]);
    // Serial.println(buffer[messageSize + 1]);

    num = (num + 1) % 10;

    delay(5);
}


// #define START_BYTE1     '<'
// #define START_BYTE2     '#'
// #define END_BYTE        '>'

// uint8_t * serialize_uint8(uint8_t *buffer, uint8_t value) {
//     buffer[0] = value;
//     return (buffer + 1);
// }

// uint8_t * serialize_uint16(uint8_t *buffer, uint16_t value) {
//     // Big-endian
//     buffer[0] = value >> 8;
//     buffer[1] = value;
//     return (buffer + 2);
// }

// uint8_t * serialize_uint32(uint8_t *buffer, uint32_t value) {
//     // Big-endian
//     buffer[0] = value >> 24;
//     buffer[1] = value >> 16;
//     buffer[2] = value >> 8;
//     buffer[3] = value;
//     return (buffer + 4);
// }

// uint8_t * serialize_Message(uint8_t *buffer, const Message &message) {
//     for (uint8_t i = 0; i < SIZE; i++) {
//         buffer = serialize_uint16(buffer, message.intVar[i]);
//     }

//     buffer = serialize_uint32(buffer, message.longVar);

//     return buffer;
// }



// uint8_t packMessage(uint8_t *buffer, const Message &message) {
//     // message start
//     buffer[0] = START_BYTE1;
//     buffer[1] = START_BYTE2;

//     uint8_t * ptr = serialize_Message(buffer + 2, message);
    
//     ptr[0] = END_BYTE;
//     ptr++;

//     return (ptr - buffer);
// }
