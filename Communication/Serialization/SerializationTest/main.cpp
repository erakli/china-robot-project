#include "../serialization.h"


#define BUFFER_SIZE 32

Message message1, message2;
uint8_t buffer[BUFFER_SIZE];
uint16_t messageSize;
uint16_t bytesRead;

uint16_t num = 0;
uint16_t tempNum;

int main(int argc, char *argv[])
{
	while (true) {
		{
			tempNum = num + 255;

			for (uint8_t i = 0; i < SIZE; i++) {
				message1.intVar[i] = i;
				message1.intVar[i] += tempNum;
			}
			message1.longVar = 32 + tempNum + 65535;

			messageSize = packMessage(buffer, message1);
			num = (num + 1) % 10;
		}

		{
			unpackMessage(buffer, messageSize, bytesRead, message2);
		}
	}
}