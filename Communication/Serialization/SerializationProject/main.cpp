#include <QtSerialPort/QSerialPort>

#include <QTextStream>
#include <QCoreApplication>

#include <QDebug>

//#include "../TestMessage.h"
#include "../StateVectorMessage.h"

QT_USE_NAMESPACE


int main(int argc, char *argv[])
{
    QCoreApplication coreApplication(argc, argv);
    QTextStream standardOutput(stdout);

//    int buf_size = 128;
//    uint8_t buffer[buf_size];
//    StateVectorMessage message;
//    StateVectorMessage message2;
//    uint16_t for_read;

//    for(int i = 0; i < STATE_VECTOR_SIZE; i++)
//    {
//        message.ultrasonicTimes[i] = i+1000000;
//    }

//    message.yaw = 5;


//    packStateVectorMessage(buffer, message);
//    unpackStateVectorMessage(buffer,buf_size,for_read,message2);

//   int k = 0;

    QSerialPort serialPort;
    QString serialPortName = "COM308";
    serialPort.setPortName(serialPortName);
    serialPort.setBaudRate(QSerialPort::Baud9600);

    if (!serialPort.open(QIODevice::ReadOnly)) {
        standardOutput << QObject::tr("Failed to open port %1, error: %2").arg(serialPortName).arg(serialPort.error()) << endl;
        return 1;
    }

//    Message message;
    StateVectorMessage message;
    uint16_t bytesRead;
    uint16_t bufferSize;
    uint8_t * ptr;

    QByteArray readData = serialPort.readAll();
    while (serialPort.waitForReadyRead(5000)) {
        readData = readData.append(serialPort.readAll());
        bufferSize = readData.size();
//        if (bufferSize >= sizeof(Message) * 3) {
        if (bufferSize >= sizeof(StateVectorMessage) * 3) {
            // try to read message
//            ptr = unpackMessage((std::uint8_t*)readData.data(), bufferSize, bytesRead, message);
            ptr = unpackStateVectorMessage((std::uint8_t*)readData.data(), bufferSize, bytesRead, message);
            if (ptr == NULL) {
                standardOutput << "Message not found" << endl;
                qDebug() << readData << endl;
                if (bytesRead > 0)
                    readData = readData.mid(bytesRead);
                continue;
            }

//            for (int i = 0; i < SIZE; i++) {
//                standardOutput << QString::number(message.intVar[i]) << " ";
//            }
//            standardOutput << "; " << QString::number(message.longVar) << endl;

            for (int i = 0; i < STATE_VECTOR_SIZE; i++) {
                standardOutput << QString::number(message.ultrasonicTimes[i] / 57) << " ";
            }
            standardOutput << "; " << QString::number(message.yaw) << endl;

            // мы прочитали сообщение, можем продолжать слушать
            if (bufferSize > bytesRead)
                readData = readData.mid(bytesRead);
            else
                readData.clear();
        }
    }

    if (serialPort.error() == QSerialPort::ReadError) {
        standardOutput << QObject::tr("Failed to read from port %1, error: %2").arg(serialPortName).arg(serialPort.errorString()) << endl;
        return 1;
    } else if (serialPort.error() == QSerialPort::TimeoutError && readData.isEmpty()) {
        standardOutput << QObject::tr("No data was currently available for reading from port %1").arg(serialPortName) << endl;
        return 0;
    }

    standardOutput << QObject::tr("Data successfully received from port %1").arg(serialPortName) << endl;
    standardOutput << readData << endl;

    return 0;
}
