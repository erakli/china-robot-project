#include "Relay.h"

// #include "debug.h"

Relay::Relay(uint8_t outputPinNum) 
    : m_outputPin(outputPinNum) {
    m_outputPinNum = outputPinNum;

    // pinMode(m_outputPinNum, OUTPUT);
    // TODO: temp commented
    SetState(CLOSED);
}



void Relay::SetState(RelayState state) {
    // TRACEIN("Relay::SetState"); 
    // TRACE_ARG(state);

    m_state = state;
    // digitalWrite(m_outputPinNum, m_state);
    m_outputPin = m_state;

    // TRACEOUT("Relay::SetState");
}



uint8_t Relay::GetState() const {
    return m_state;
}



void Relay::ToggleRelay() {
    SetState( (m_state == CLOSED) ? OPENED : CLOSED );
}
