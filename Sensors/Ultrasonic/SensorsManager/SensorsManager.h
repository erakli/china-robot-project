#ifndef SENSORS_MANAGER_H
#define SENSORS_MANAGER_H 

#include <Arduino.h>    // uint8_t, uint16_t, delay() и тд.

#include "Sensor.h"
#include "Relay.h"


#define SENSORS_NUM				4	  // Number of sensors.
#define STATE_VECTOR_SIZE		SENSORS_NUM * SENSORS_NUM

#define MAX_DISTANCE            200   // максимальная дистанция в см, которую будут обнаруживать датчики

#define PING_INTERVAL			50    // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define RELAY_INTERVAL          10    // mS

#define DISTANCES_NUM		    1     // количество дистанций, собираемое для среднего значения
#define MIN_DISTANCES_COUNT     0     // минимальное количество дистанций для увереннго приёма


enum SonarCodes {
    ALPHA, 
    BETHA, 
    DELTA, 
    GAMMA  
};

extern const char * SonarNames[SENSORS_NUM];



struct SensorMeasurements {
    // полученные дистанции
    uint16_t distances[DISTANCES_NUM];
    uint8_t currentMeasurement;

    SensorMeasurements();
    void AddDistance(uint16_t newDistance);
    uint16_t GetMeanDistance();
    void Clear();
};



class SensorsManager {
    public:
        SensorsManager();
        SensorsManager(Sensor *sensors, Relay *relays);

        void SetArrays(Sensor *sensors, Relay *relays);

        void MeasurementCycle();
        void SetActiveSensor(uint8_t activeSensor);

        const uint16_t * GetStateVector() const;

    private:
        void ActiveSensorCycle();

		void SensorsSimultaneousPing();

        void SaveMeasurements();
        void SaveMeanDistances();
        void CopyDistancesToStateVector();

        void ClearMeasurements();
        void ClearActiveSensorDistances();
        void ClearStateVector();

    private:
        // массивы
        Sensor *m_sensors;
        Relay *m_relays;

        uint8_t m_activeSensor;

        // храним измерения
        SensorMeasurements m_measurements[SENSORS_NUM];

        // дистанции текущего сенсора и отклики с остальных сенсоров
        uint16_t m_activeSensorDistances[SENSORS_NUM];
        uint16_t m_stateVector[STATE_VECTOR_SIZE];
};


#endif
