#include "SerialCommunication.h"
#include "SensorsManager.h"

#include "ArrayFunctions.h"

#define BAUDRATE	115200

// всю магию будет творить вот этот вот красавчик
// перед использованием надо заложить в него массивы датчиков и реле
SensorsManager sensorsManager;

// void PrintStateVector();

void setup() {
    // Open serial monitor to see ping results.
    openPorts(BAUDRATE);
}


uint16_t * stateVector;

void loop() {
    // Serial.println("loop"); 
    // Serial.print("time: "); 
    // Serial.print(millis()); 
    // Serial.print(",     ");

    sensorsManager.MeasurementCycle();
    stateVector = sensorsManager.GetStateVector();

    // PrintArray(stateVector, STATE_VECTOR_SIZE);
    DisplayStateVector();

    // Serial.println(" ");
    // Serial.println(" ");
    // Serial.println(" ");
    // Serial.println(" ");

    // delay(3000);
}


void DisplayStateVector() {
    Serial.print(millis());
    Serial.print(",     ");

    unsigned long cm = 0;

    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
#ifdef OUTPUT_READABLE
        Serial.print("      s");
        Serial.print(i);
        Serial.print(": ");
#endif
        for (uint8_t j = 0; j < SENSORS_NUM; j++) {
            cm = stateVector[i * SENSORS_NUM + j] / US_ROUNDTRIP_CM;

            Serial.print(cm);
            Serial.print(", ");
        }
    }

    Serial.println();
}
