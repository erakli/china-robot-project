// SensorsManager test + driving sketch

#include "SerialCommunication.h"
#include "SensorsManager.h"

#include "ArrayFunctions.h"

#define BAUDRATE	9600
//#define Serial  Serial1

#define MEASUREMENT_DELAY 1000

// всю магию будет творить вот этот вот красавчик
// перед использованием надо заложить в него массивы датчиков и реле
SensorsManager sensorsManager;

// void PrintStateVector();



// при питании DAGU и ARDUINO от разных источников не забываем
// объединять земли

int pwm2 = 9;
int pwm3 = 10;
int dir2 = 29; //правые (1 и 3)
int dir3 = 28; //левые (2 и 4)

int V = 150; //скорость вращения двигателей

void setCommand(char command, int V);



unsigned long nextMeasurementTime;

void setup() {
    // Open serial monitor to see ping results.
    openPorts(BAUDRATE);

    pinMode(dir2,OUTPUT);
    pinMode(dir3,OUTPUT);

    nextMeasurementTime = millis();
}


uint16_t * stateVector;

void loop() {

    if(Serial.available())
    {
        int value = Serial.read();
        setCommand(value, V);
    }
    
    if (nextMeasurementTime < millis()) {
        Serial.print("time: "); 
        Serial.print(millis() / 1000); 
        Serial.println(" ");
    
        sensorsManager.MeasurementCycle();
        stateVector = sensorsManager.GetStateVector();
    
        PrintArray(stateVector, STATE_VECTOR_SIZE);
        Serial.println(" ");
        Serial.println(" ");
        Serial.println(" ");
        Serial.println(" ");

        nextMeasurementTime += MEASUREMENT_DELAY;
    }
}


// char buf[1024];
// size_t buf_pos;

// void PrintStateVector() {
//     for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
//         sprintf(buf, "    %s: ", SonarNames[sensor]);
//         Serial.print(buf);

//         buf_pos = 0;
//         for (uint8_t responce = 0; responce < SENSORS_NUM; responce++) {
//             buf_pos = sprintf(buf + buf_pos, "%i, ", stateVector[sensor * SENSORS_NUM + responce]);
//         }
//         Serial.println(buf);
//     }
// }



void setCommand(char command, int V) // управление двигателями.
{
    analogWrite (pwm2, LOW);
    analogWrite (pwm3, LOW); 

    delayMicroseconds(250);

    switch (command) 
    {
        case '8': //вперед             
            digitalWrite(dir2,HIGH);
            digitalWrite(dir3,HIGH);
            Serial.println("Forward");
        break;
      
        case '2': //назад
            digitalWrite(dir2,LOW);
            digitalWrite(dir3,LOW); 
            Serial.println("Back");
        break;

        case '4': // влево
            digitalWrite(dir2,HIGH);
            digitalWrite(dir3,LOW); 
            Serial.println("Left");
        break;

        case '6':// вправо
            digitalWrite(dir2,LOW);
            digitalWrite(dir3,HIGH);
            Serial.println("Right");
        break;

        case '5': // stop
          Serial.println("Stop");
        return;
        
    } // end switch

    if (command == '8' || command == '2' || command == '4' || command == '6') {
        analogWrite (pwm2, V);
        analogWrite (pwm3, V);
    }
}
