// SensorsManager test sketch

#include "SerialCommunication.h"
#include "SensorsManager.h"

#include "ArrayFunctions.h"

#define BAUDRATE	115200

// всю магию будет творить вот этот вот красавчик
// перед использованием надо заложить в него массивы датчиков и реле
SensorsManager sensorsManager;

// void PrintStateVector();

void setup() {
    // Open serial monitor to see ping results.
    openPorts(BAUDRATE);
}


uint16_t * stateVector;

void loop() {
    Serial.println("loop"); 
    Serial.print("time: "); 
    Serial.print(millis() / 1000); 
    Serial.println(" ");

    sensorsManager.MeasurementCycle();
    stateVector = sensorsManager.GetStateVector();

    PrintArray(stateVector, STATE_VECTOR_SIZE);
    Serial.println(" ");
    Serial.println(" ");
    Serial.println(" ");
    Serial.println(" ");

    delay(3000);
}


// char buf[1024];
// size_t buf_pos;

// void PrintStateVector() {
//     for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
//         sprintf(buf, "    %s: ", SonarNames[sensor]);
//         Serial.print(buf);

//         buf_pos = 0;
//         for (uint8_t responce = 0; responce < SENSORS_NUM; responce++) {
//             buf_pos = sprintf(buf + buf_pos, "%i, ", stateVector[sensor * SENSORS_NUM + responce]);
//         }
//         Serial.println(buf);
//     }
// }
