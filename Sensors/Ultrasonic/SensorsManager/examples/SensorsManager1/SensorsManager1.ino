#include "SerialCommunication.h"
#include "SensorsManager.h"

#include "ArrayFunctions.h"

#define BAUDRATE	115200

#define SOP '<'                 // start of packet
#define EOP '>'                 // end of packet
#define SEPARATE_SYMBOL ','     

// всю магию будет творить вот этот вот красавчик
// перед использованием надо заложить в него массивы датчиков и реле
SensorsManager sensorsManager;


uint16_t * stateVector;

void SendStateVector() {
    // start of packet
    Serial.print(SOP);

    for(uint8_t i = 0; i < STATE_VECTOR_SIZE; i++) {
        Serial.print(stateVector[i]);
        Serial.print(SEPARATE_SYMBOL);
    }
    Serial.print(EOP); //used as an end of transmission character - used in app for string length
    Serial.println();
    delay(10);        //added a delay to eliminate missed transmissions
}


void setup() {
    // Open serial monitor to see ping results.
    openPorts(BAUDRATE);
}


void loop() {
    sensorsManager.MeasurementCycle();
    stateVector = sensorsManager.GetStateVector();

    SendStateVector();
}