#include "pins.h"

#include "Relay.h"
#include "Sensor.h"



#define SENSORS_NUM				4	  // Number of sensors.
#define MAX_DISTANCE            200   // максимальная дистанция в см, которую будут обнаруживать датчики

Sensor sensors_array[SENSORS_NUM] = {
    // Each sensor's trigger pin, echo pin, and max distance to ping.
    NewPingMega(SENSOR_0_TRIG, SENSOR_0_ECHO, MAX_DISTANCE),  
    NewPingMega(SENSOR_1_TRIG, SENSOR_1_ECHO, MAX_DISTANCE),
    NewPingMega(SENSOR_2_TRIG, SENSOR_2_ECHO, MAX_DISTANCE),
    NewPingMega(SENSOR_3_TRIG, SENSOR_3_ECHO, MAX_DISTANCE)
};

Relay relays_array[SENSORS_NUM] = {
    RELAY_0,
    RELAY_1,
    RELAY_2,
    RELAY_3
};


enum SonarCodes {
    ALPHA, 
    BETHA, 
    DELTA, 
    GAMMA  
};

char activeSensor;


void CloseRelays() {
    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        relays_array[i].SetState(CLOSED);
    }
}


void setup() {
    CloseRelays();

    activeSensor = ALPHA;
    relays_array[activeSensor].SetState(OPENED);

    Serial.begin(9600);
}

void loop() {
    if (Serial.available()) {
        char command = Serial.read();

        switch (command) {
            case '1':
                activeSensor = ALPHA;
            break;

            case '2':
                activeSensor = BETHA;
            break;

            case '3':
                activeSensor = DELTA;
            break;

            case '4':
                activeSensor = GAMMA;
            break;
        }

        if (activeSensor == ALPHA || 
            activeSensor == BETHA ||
            activeSensor == DELTA ||
            activeSensor == GAMMA) 
        {
            CloseRelays();
            relays_array[activeSensor].SetState(OPENED);
        }
    }

    uint32_t cm = sensors_array[activeSensor].ping_cm();
    Serial.print("Sensor ");
    Serial.print(uint16_t(activeSensor));
    Serial.print(" = ");
    Serial.print(cm);
    Serial.println(" cm");

    delay(50);
}
