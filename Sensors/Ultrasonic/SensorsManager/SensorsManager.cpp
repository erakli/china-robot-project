#include "SensorsManager.h"

#include <string.h>
#include "ArrayFunctions.h"
#include "debug.h"

#include "pins.h"

// вычисление циклического индекса
#define CYCLE_INDEX(current, offset, count) ( ( (current) + (offset) ) % (count) )

// TODO: переходим к использованию глобальных данных

Sensor sensors[SENSORS_NUM] = {
    // Each sensor's trigger pin, echo pin, and max distance to ping.
    NewPingMega(SENSOR_0_TRIG, SENSOR_0_ECHO, MAX_DISTANCE),  
    NewPingMega(SENSOR_1_TRIG, SENSOR_1_ECHO, MAX_DISTANCE),
    NewPingMega(SENSOR_2_TRIG, SENSOR_2_ECHO, MAX_DISTANCE),
    NewPingMega(SENSOR_3_TRIG, SENSOR_3_ECHO, MAX_DISTANCE)
};

Relay relays[SENSORS_NUM] = {
    RELAY_0,
    RELAY_1,
    RELAY_2,
    RELAY_3
};


const char * SonarNames[SENSORS_NUM] = {
    "ALPHA",
    "BETHA",
    "DELTA",
    "GAMMA"
};


// ---------------------------------------------------------------------------
// измерения каждого датчика
// ---------------------------------------------------------------------------
SensorMeasurements::SensorMeasurements() {
    Clear();
}

void SensorMeasurements::AddDistance(uint16_t newDistance) {
    distances[currentMeasurement] = newDistance;
    currentMeasurement++;
}

uint16_t SensorMeasurements::GetMeanDistance() {
    uint32_t resultDistance = 0;
    uint8_t num = 0;

    for (uint8_t i = 0; i < currentMeasurement; i++) {
        if (distances[i] != NO_ECHO) {
            resultDistance += distances[i];
            num++;
        }
    }

    if (num > MIN_DISTANCES_COUNT) {
        return (resultDistance / num);
    }

    return 0;
}

void SensorMeasurements::Clear() {
    for (uint8_t i = 0; i < DISTANCES_NUM; i++) {
        distances[i] = 0;
    }

    currentMeasurement = 0;
}


// ---------------------------------------------------------------------------
// менеджер сенсоров
// ---------------------------------------------------------------------------
SensorsManager::SensorsManager() {
    // глобальные массивы
    SetArrays(::sensors, ::relays);

    // TODO: temp changed
    SetActiveSensor(ALPHA);

    // TODO: temp OPENED all relays
    // for(uint8_t i = 0; i < SENSORS_NUM; i++)
    //     m_relays[i].SetState(OPENED);
}

SensorsManager::SensorsManager(Sensor *sensors, Relay *relays) { 
    SetArrays(sensors, relays);

    SetActiveSensor(ALPHA);
}



void SensorsManager::SetArrays(Sensor *sensors, Relay *relays) {
    // TRACEIN("SensorsManager::SetArrays");
    
    m_sensors = sensors;
    m_relays = relays;

    ClearMeasurements();
    ClearActiveSensorDistances();
    ClearStateVector();

// #ifdef DEBUG
//     PrintArray(m_activeSensorDistances, SENSORS_NUM, "m_activeSensorDistances");
//     DPRINTLN(" ");
//     PrintArray(m_stateVector, STATE_VECTOR_SIZE, "m_stateVector");
//     DPRINTLN(" ");
// #endif 
    
//     TRACEOUT("SensorsManager::SetArrays");
}



void SensorsManager::MeasurementCycle() {
    TRACEIN("SensorsManager::MeasurementCycle");

    ClearMeasurements();
    ClearActiveSensorDistances();
    ClearStateVector();

#ifdef DEBUG
   PrintArray(m_stateVector, STATE_VECTOR_SIZE, "m_stateVector");
   DPRINTLN(" ");
#endif

    for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
        SetActiveSensor(sensor);
        // TODO: пробуем сделать паузу между реле
        delay(RELAY_INTERVAL);
        ActiveSensorCycle();
    }

#ifdef DEBUG
   PrintArray(m_stateVector, STATE_VECTOR_SIZE, "m_stateVector");
   DPRINTLN(" ");
#endif

    TRACEOUT("SensorsManager::MeasurementCycle");
}



void SensorsManager::SetActiveSensor(uint8_t activeSensor) {
    // TRACEIN("SensorsManager::SetActiveSensor");
    // TRACE_ARG(activeSensor);

    if (activeSensor == m_activeSensor) {
        // TRACEOUT("SensorsManager::SetActiveSensor");
        return;
    }

    m_activeSensor = activeSensor;

    // TODO: temp skip next statements
    // TRACEOUT("SensorsManager::SetActiveSensor");
    // return;

    // закрываем все остальные реле кроме активного
    for (uint8_t relay = 0; relay < SENSORS_NUM; relay++) {
        if (relay == activeSensor)
            m_relays[relay].SetState(OPENED);
        else
            m_relays[relay].SetState(CLOSED);
    }

    // TRACEOUT("SensorsManager::SetActiveSensor");
}



// TODO: отдаём указатель, а не копию!
const uint16_t * SensorsManager::GetStateVector() const {
    return m_stateVector;
}



// последовательность таймеров в порядке приоритета
const Timers timersSequence[SENSORS_NUM] = {
    Timer1, Timer3, Timer4, Timer5
};

// TODO: добавить условие выбора типа измерений: одновременно или 
// последовательно
void SensorsManager::ActiveSensorCycle() {
    TRACEIN("SensorsManager::ActiveSensorCycle"); 
    DPRINT(">> ");
    VALUE_DPRINTLN(m_activeSensor);

    uint8_t sensorIndex; // вычисляем индекс со сдвигом

    for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
        m_measurements[sensor].Clear();

        // присваеваем таймеры каждому из датчиков, начиная с активного
        sensorIndex = CYCLE_INDEX(m_activeSensor, sensor, SENSORS_NUM);
        m_sensors[sensorIndex].assign_timer(timersSequence[sensorIndex]);
    }

    // делаем сразу много измерений, чтобы получить более достоверный результат
    for (uint8_t iter = 0; iter < DISTANCES_NUM; iter++) {
        // VALUE_DPRINTLN(iter);

		SensorsSimultaneousPing();

        // DPRINT("delay "); 
        // VALUE_DPRINTLN(PING_INTERVAL);

        // TODO: ну это типо плохо
        // даём отражениям затихнуть
        delay(PING_INTERVAL);	

        // после того, как все пинги были получены, можем их сохранить
        SaveMeasurements();
    }

    SaveMeanDistances();

#ifdef DEBUG
   PrintArray(m_activeSensorDistances, SENSORS_NUM, "m_activeSensorDistances");
   DPRINTLN(" ");
#endif

    // копируем отклики со всех сенсоров в вектор состояния на позицию текущего сенсора в векторе состояния
    CopyDistancesToStateVector();

    TRACEOUT("SensorsManager::ActiveSensorCycle");
}




// ISR global functions
// используем глобальные массивы, так как мы на них ссылаемся в классе
void echoCheck0() {
    ::sensors[0].CheckEcho();
}

void echoCheck1() {
    ::sensors[1].CheckEcho();
}

void echoCheck2() {
    ::sensors[2].CheckEcho();
}

void echoCheck3() {
    ::sensors[3].CheckEcho();
}

typedef void (*isrFunc)(void);
isrFunc isrFuncArray[SENSORS_NUM] = {
    echoCheck0,
    echoCheck1,
    echoCheck2,
    echoCheck3
};



// одновременный запуск всех датчиков. между всеми операциями не должно
// происходить никаких лишних действий
void SensorsManager::SensorsSimultaneousPing() {
    uint8_t sensor;
    uint8_t sensorIndex; // вычисляем индекс со сдвигом

    // запускаем все сенсоры начиная с активного
    for (sensor = 0; sensor < SENSORS_NUM; sensor++) {
        sensorIndex = CYCLE_INDEX(m_activeSensor, sensor, SENSORS_NUM);
        m_sensors[sensorIndex].activate_trigger();
    }

    // ждём старта отсчёта времени echo
    for (sensor = 0; sensor < SENSORS_NUM; sensor++) {
        sensorIndex = CYCLE_INDEX(m_activeSensor, sensor, SENSORS_NUM);
        // TODO: не будем сохранять эти времена, так как они храняться в классе NewPingMega
        // startTimes[sensorIndex] = 
            m_sensors[sensorIndex].wait_ping_start();
    }

    // запускаем таймеры, присваиваем функции для проверки
    for (sensor = 0; sensor < SENSORS_NUM; sensor++) {
        sensorIndex = CYCLE_INDEX(m_activeSensor, sensor, SENSORS_NUM);
        m_sensors[sensorIndex].start_ping_timer(isrFuncArray[sensorIndex]);
    }
}



// ---------------------------------------------------------------------------
// всякая работа с массивами
// ---------------------------------------------------------------------------

void SensorsManager::SaveMeasurements() {
    uint16_t ping_result;

    for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
        // если пинг был получен, то сохраняем результат, иначе 0
        if (m_sensors[sensor].PingRecieved()) {
            ping_result = 
                m_sensors[sensor].ping_result +
                (m_sensors[sensor].pingStartTime - 
                m_sensors[m_activeSensor].pingStartTime);
        } else {
            ping_result = 0;
        }

        m_measurements[sensor].AddDistance(ping_result);
    }
}


 void SensorsManager::SaveMeanDistances() {
    for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
        m_activeSensorDistances[sensor] = 
            m_measurements[sensor].GetMeanDistance();
    }
 }



void SensorsManager::CopyDistancesToStateVector() {
    uint8_t offset = m_activeSensor * SENSORS_NUM;
    for (uint8_t i = 0; i < SENSORS_NUM; i++)
        m_stateVector[i + offset] = m_activeSensorDistances[i];
}



void SensorsManager::ClearMeasurements() {
    for (uint8_t sensor = 0; sensor < SENSORS_NUM; sensor++) {
        m_measurements[sensor].Clear();
    }
}

// для массивов будем пользоваться
// void * memcpy( void * dest, const void * src, size_t count ); // count - num of bytes
// void * memset ( void * ptr, int value, size_t num );

// num - Number of elements to allocate. size - Size of each element.
// void * calloc ( size_t num, size_t size ); 

void SensorsManager::ClearActiveSensorDistances() {
    memset(m_activeSensorDistances, 0, SENSORS_NUM * sizeof(uint16_t));
}

void SensorsManager::ClearStateVector() {
    memset(m_stateVector, 0, STATE_VECTOR_SIZE * sizeof(uint16_t));
}