//#include <Arduino.h>
#include <NewPing.h>

//#define ENABLE_BLUETOOTH 
#define DEBUG 

#ifdef ENABLE_BLUETOOTH 
#define BLUETOOTH_PORT  Serial1 
#endif

// макросы для открытия и закрытия реле
#define CLOSED  HIGH
#define OPENED  LOW

#define SONAR_NUM     4     // Number of sensors.
#define PING_INTERVAL 33    // Milliseconds between sensor pings (29ms is about the min to avoid cross-sensor echo).
#define MAX_DISTANCE 700    // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define RELAY_INTERVAL 250 // Задержка, необходимая для переключения реле

unsigned cm[SONAR_NUM];     // Where the ping distances are stored.

NewPing sonars[SONAR_NUM] = {       // Sensor object array.
    NewPing(6, 7, MAX_DISTANCE),    // Each sensor's trigger pin, echo pin, and max distance to ping.
    NewPing(8, 9, MAX_DISTANCE),
    NewPing(25, 26, MAX_DISTANCE),
    NewPing(28, 29, MAX_DISTANCE)
};

//управляющие сигналы Relay
uint8_t relays[SONAR_NUM] = {
    50,
    51,
    52,
    53
};

// вектор состояния реле
int relaysState[SONAR_NUM];

unsigned char key;

#define ALPHA_CODE 'a' 
#define BETHA_CODE 'b' 
#define DELTA_CODE 'd' 
#define GAMMA_CODE 'g' 

enum {
    ALPHA, 
    BETHA, 
    DELTA, 
    GAMMA
} sonarCodes;

String sonarNames[SONAR_NUM] = {
    "ALPHA",
    "BETHA",
    "DELTA",
    "GAMMA"
};



void setup() {
    // Open serial monitor to see ping results.
    Serial.begin(9600);
#ifdef ENABLE_BLUETOOTH 
    BLUETOOTH_PORT.begin(9600);
#endif

    // устанавливаем все реле
    for (uint8_t i = 0; i < SONAR_NUM; i++) {
        pinMode(relays[i], OUTPUT);
        relaysState[i] = CLOSED;
        //relaysState[i] = OPENED;  // TEMP: open all
    }

    //relaysState[ALPHA] = OPENED;
    relaysState[BETHA] = OPENED;
    SetRelaysState(relaysState);

    Serial.print("Initialized.");
    Serial.println();
#ifdef ENABLE_BLUETOOTH 
    BLUETOOTH_PORT.print("Initialized.");
    BLUETOOTH_PORT.println();
#endif
}



void loop() { 
    AllSensorsPing(BETHA, false);
    
    AllSensorsOutput(Serial);
#ifdef ENABLE_BLUETOOTH 
    AllSensorsOutput(BLUETOOTH_PORT);
#endif

    // ReadCode(Serial, key);
    // ReadCode(BLUETOOTH_PORT, key);

    // switch (key) {
    //     case ALPHA_CODE:

    //         break;

    //     case BETHA_CODE:

    //         break;

    //     case DELTA_CODE:

    //         break;

    //     case GAMMA_CODE:

    //         break;

    //     default:
    //         break;
    // }

    //delay(RELAY_INTERVAL);
}



// =====================================================================
// Последовательные порты
// =====================================================================
// читаем код из порта. если на вход ничего не пришло, то ничего и не пишем
void ReadCode(HardwareSerial & port, char & key) {
    if (port.available() > 0) {
        key = port.read();
    }
}

void AllSensorsOutput(HardwareSerial & port) {
    for (uint8_t sensor = 0; sensor < SONAR_NUM; sensor++) {
        SensorOutput(port, sensor);
    }
#ifdef DEBUG
    port.print("  (");
    port.print(millis());
    port.print(")");
#endif     
    port.println();
}

void SensorOutput(HardwareSerial & port, uint8_t sensor) {
    port.print(sonarNames[sensor]);
    port.print(" = ");
    port.print(cm[sensor]);
//    port.print("cm; ");
    port.print("; ");
}



// =====================================================================
// Сенсоры
// =====================================================================
unsigned idx;
void AllSensorsPing(uint8_t start_sensor, bool withInterval) {
    for (uint8_t sensor = 0; sensor < SONAR_NUM; sensor++) {
        idx = (start_sensor + sensor) % SONAR_NUM;
        cm[idx] = sonars[idx].ping();
        if (withInterval){
            delay(PING_INTERVAL);
        }
    }
}



// =====================================================================
// Реле
// =====================================================================
void SetRelayState(uint8_t relay, int state) {
    digitalWrite(relays[relay], state);
}

int GetRelayState(uint8_t relay) {
    return digitalRead(relays[relay]);
}



void SetRelaysState(const int * relaysState) {
    for (uint8_t relay = 0; relay < SONAR_NUM; relay++) {
        SetRelayState(relay, relaysState[relay]);
#ifdef DEBUG
        Serial.println(relaysState[relay]);
        RelayDebug(relay);
#endif 
    }
}



void SetNextRelay(uint8_t current_relay) {
    ToggleRelay(current_relay);
    ToggleRelay((current_relay + 1) % SONAR_NUM); // по идее, имеем циклический сдвиг 
    delay(RELAY_INTERVAL);  // даём время на переключение реле
}

void ToggleRelay(uint8_t relay) {
    SetRelayState(relay, !GetRelayState(relay));
#ifdef DEBUG
    RelayDebug(relay);
#endif 
}

#ifdef DEBUG
void RelayDebug(uint8_t relay) {
    Serial.print("relay ");
    Serial.print(relay);
    Serial.print(" now is ");
    // у нас инвертированы состояния
    Serial.print(!GetRelayState(relay) ? "opened" : "closed");
    Serial.print(" (");
    Serial.print(millis());
    Serial.print(")");
    Serial.println();
}
#endif 
