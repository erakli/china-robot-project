
// делаю ручной опрос 4х датчиков по очереди
#include <SoftwareSerial.h>   //модуль для работы с Serial

#define LOOP_DELAY 4000
#define DELAY_MC 100

//пины мод УЗД Alpha
const int trigPin_A = 3;
const int echoPin_A = 2;

//пины мод УЗД Beta
const int trigPin_B = 5;
const int echoPin_B = 4;

//пины мод УЗД Delta
const int trigPin_D = 6;
const int echoPin_D = 7;

//пины мод УЗД Gamma
const int trigPin_G = 9;
const int echoPin_G = 8;

//управляющие сигналы Relay
const int beamPin_A = 10; //реле №1
const int beamPin_B = 11; //реле №2
const int beamPin_D = 12; //реле №3
const int beamPin_G = 13; //реле №4

const double transf=0.034/2.0;  //первод показаний УЗД в норм. вид

//char    key;      //нажатая клавиша

long    duration_A; //время сигнала
double  dist_A;     //дальность

long    duration_B;
double  dist_B;

long    duration_D;
double  dist_D;

long    duration_G;
double  dist_G;

int key = 'a';

void setup() {  //инициализация
  //Alpha
  pinMode(trigPin_A,  OUTPUT);
  pinMode(echoPin_A,  INPUT );
  
  //Betha
  pinMode(trigPin_B, OUTPUT); 
  pinMode(echoPin_B, INPUT );
  
  //Delta
  pinMode(trigPin_D,  OUTPUT);
  pinMode(echoPin_D,  INPUT );
  
  //Gamma
  pinMode(trigPin_G, OUTPUT); 
  pinMode(echoPin_G, INPUT );
  
  //Relay
  pinMode(beamPin_A,   OUTPUT);
  pinMode(beamPin_B,   OUTPUT);
  pinMode(beamPin_D,   OUTPUT);
  pinMode(beamPin_G,   OUTPUT);
  
  Serial.begin(9600);
}

void loop() { //происходит постоянно

  //проверка нажатия клавиши
  if(Serial.available() > 0) {
     key = Serial.read(); //read it
  }
  switch (key){
    /*==================================================================================*/
    case 'b': //alpha работает как обычный УЗД, а остальные только слушают, все выводят дистанцию
    digitalWrite(beamPin_A, LOW); //управляющий сигнал реле1 включен
    digitalWrite(beamPin_B, HIGH); //управляющий сигнал реле2 выключен
    digitalWrite(beamPin_D, HIGH);
    digitalWrite(beamPin_G, HIGH);
    
    //Alpha
    digitalWrite(trigPin_A, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_A, LOW );
    duration_A = pulseIn(echoPin_A, HIGH);
    dist_A     = duration_A*transf;
    Serial.print("УЗД Alpha (вкл) дистанция = ");
    Serial.println(dist_A);
    
    //Beta
    digitalWrite(trigPin_B, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_B, LOW );
    duration_B = pulseIn(echoPin_B, HIGH);
    dist_B     = duration_B*transf;
    Serial.print("УЗД Beta (вкл) дистанция = ");
    Serial.println(dist_B);
    
    //Delta
    digitalWrite(trigPin_D, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_D, LOW );
    duration_D = pulseIn(echoPin_D, HIGH);
    dist_D     = duration_D*transf;
    Serial.print("УЗД Delta (вкл) дистанция = ");
    Serial.println(dist_D);
    
    //Gamma
    digitalWrite(trigPin_G, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_G, LOW );
    duration_G = pulseIn(echoPin_G, HIGH);
    dist_G     = duration_G*transf;
    Serial.print("УЗД Gamma (вкл) дистанция = ");
    Serial.println(dist_G);
    
      break;
    /*==================================================================================*/
    
    case 'a': ////beta работает как обычный УЗД, а остальные только слушают, все выводят дистанцию
    digitalWrite(beamPin_A, HIGH); //управляющий сигнал реле1 выключен
    digitalWrite(beamPin_B, LOW); //управляющий сигнал реле2 включен
    digitalWrite(beamPin_D, HIGH);
    digitalWrite(beamPin_G, HIGH);
    
    //Alpha
    digitalWrite(trigPin_A, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_A, LOW );
    duration_A = pulseIn(echoPin_A, HIGH);
    dist_A     = duration_A*transf;
    Serial.print("УЗД Alpha (вкл) дистанция = ");
    Serial.println(dist_A);
    
    //Beta
    digitalWrite(trigPin_B, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_B, LOW );
    duration_B = pulseIn(echoPin_B, HIGH);
    dist_B     = duration_B*transf;
    Serial.print("УЗД Beta (вкл) дистанция = ");
    Serial.println(dist_B);
    
    //Delta
    digitalWrite(trigPin_D, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_D, LOW );
    duration_D = pulseIn(echoPin_D, HIGH);
    dist_D     = duration_D*transf;
    Serial.print("УЗД Delta (вкл) дистанция = ");
    Serial.println(dist_D);
    
    //Gamma
    digitalWrite(trigPin_G, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_G, LOW );
    duration_G = pulseIn(echoPin_G, HIGH);
    dist_G     = duration_G*transf;
    Serial.print("УЗД Gamma (вкл) дистанция = ");
    Serial.println(dist_G);
    break;
    /*==================================================================================*/
    case 'd': ////delta работает как обычный УЗД, а остальные только слушают, все выводят дистанцию
    digitalWrite(beamPin_A, HIGH); //управляющий сигнал реле1 выключен
    digitalWrite(beamPin_B, HIGH); //управляющий сигнал реле2 выключен
    digitalWrite(beamPin_D, LOW); //включен
    digitalWrite(beamPin_G, HIGH);
    
    //Alpha
    digitalWrite(trigPin_A, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_A, LOW );
    duration_A = pulseIn(echoPin_A, HIGH);
    dist_A     = duration_A*transf;
    Serial.print("УЗД Alpha (вкл) дистанция = ");
    Serial.println(dist_A);
    
    //Beta
    digitalWrite(trigPin_B, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_B, LOW );
    duration_B = pulseIn(echoPin_B, HIGH);
    dist_B     = duration_B*transf;
    Serial.print("УЗД Beta (вкл) дистанция = ");
    Serial.println(dist_B);
    
    //Delta
    digitalWrite(trigPin_D, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_D, LOW );
    duration_D = pulseIn(echoPin_D, HIGH);
    dist_D     = duration_D*transf;
    Serial.print("УЗД Delta (вкл) дистанция = ");
    Serial.println(dist_D);
    
    //Gamma
    digitalWrite(trigPin_G, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_G, LOW );
    duration_G = pulseIn(echoPin_G, HIGH);
    dist_G     = duration_G*transf;
    Serial.print("УЗД Gamma (вкл) дистанция = ");
    Serial.println(dist_G);
    break;
    /*==================================================================================*/
    case 'g': ////delta работает как обычный УЗД, а остальные только слушают, все выводят дистанцию
    digitalWrite(beamPin_A, HIGH); //управляющий сигнал реле1 выключен
    digitalWrite(beamPin_B, HIGH); //управляющий сигнал реле2 выключен
    digitalWrite(beamPin_D, HIGH); //выключен
    digitalWrite(beamPin_G, LOW);  //включен
    
    //Alpha
    digitalWrite(trigPin_A, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_A, LOW );
    duration_A = pulseIn(echoPin_A, HIGH);
    dist_A     = duration_A*transf;
    Serial.print("УЗД Alpha (вкл) дистанция = ");
    Serial.println(dist_A);
    
    //Beta
    digitalWrite(trigPin_B, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_B, LOW );
    duration_B = pulseIn(echoPin_B, HIGH);
    dist_B     = duration_B*transf;
    Serial.print("УЗД Beta (вкл) дистанция = ");
    Serial.println(dist_B);
    
    //Delta
    digitalWrite(trigPin_D, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_D, LOW );
    duration_D = pulseIn(echoPin_D, HIGH);
    dist_D     = duration_D*transf;
    Serial.print("УЗД Delta (вкл) дистанция = ");
    Serial.println(dist_D);
    
    //Gamma
    digitalWrite(trigPin_G, HIGH);
    delayMicroseconds(DELAY_MC);
    digitalWrite(trigPin_G, LOW );
    duration_G = pulseIn(echoPin_G, HIGH);
    dist_G     = duration_G*transf;
    Serial.print("УЗД Gamma (вкл) дистанция = ");
    Serial.println(dist_G);
    break;
    /*==================================================================================*/
    default: //реле выключены, передачи с УЗД нет
    digitalWrite(trigPin_A,  LOW);
    digitalWrite(trigPin_B,  LOW);
    digitalWrite(trigPin_D,  LOW);
    digitalWrite(trigPin_G,  LOW);
    
    digitalWrite(beamPin_A, HIGH); //управляющий сигнал реле1 выключен
    digitalWrite(beamPin_B, HIGH); //управляющий сигнал реле2 выключен
    digitalWrite(beamPin_D, HIGH);
    digitalWrite(beamPin_G, HIGH);
    Serial.println("Нет сигналов");
    } //скобка switch
  
  delay(LOOP_DELAY);  //задержка в 1 секунду
} //скобка loop
