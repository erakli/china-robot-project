#ifndef RELAY_H
#define RELAY_H 

#include <Arduino.h>    // uint8_t

#include <DirectIO.h>

typedef uint8_t RelayState;

// макросы для открытия и закрытия реле
#define CLOSED  HIGH
#define OPENED  LOW

class Relay {
    public:
        Relay(uint8_t outputPinNum);

        void SetState(RelayState state);
        uint8_t GetState() const;
        void ToggleRelay();

    private:
        uint8_t m_outputPinNum;
        OutputPin m_outputPin;
        RelayState m_state;
};

#endif