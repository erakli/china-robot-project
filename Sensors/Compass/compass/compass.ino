#include <Wire.h> //I2C Arduino Library

#define addr 0x1E //I2C Address for The HMC5883
int intheading;

void setup(){
  
  Serial.begin(9600);
  Wire.begin();
  
  
  Wire.beginTransmission(addr); //start talking
  Wire.write(0x02); // Set the Register
  Wire.write(0x00); // Tell the HMC5883 to Continuously Measure
  Wire.endTransmission();
}


void loop(){
  
  int x,y,z; //triple axis data

  //Tell the HMC what regist to begin writing data into
  Wire.beginTransmission(addr);
  Wire.write(0x03); //start with register 3.
  Wire.endTransmission();
  
 
 //Read the data.. 2 bytes for each axis.. 6 total bytes
  Wire.requestFrom(addr, 6);
  if(6<=Wire.available()){
    x = Wire.read()<<8; //MSB  x 
    x |= Wire.read(); //LSB  x
    z = Wire.read()<<8; //MSB  z
    z |= Wire.read(); //LSB z
    y = Wire.read()<<8; //MSB y
    y |= Wire.read(); //LSB y
  }

    float heading = atan2(y, x);    // высчитываем направление
    intheading = heading;
 
  // корректируем значения с учетом знаков
  if(heading < 0) heading += 2*PI;
  if(heading > 2*PI) heading -= 2*PI;
  
  // Show Values
//  Serial.print("X Value: ");
//  Serial.println(x);
//  Serial.print("Y Value: ");
//  Serial.println(y);
//  Serial.print("Z Value: ");
//  Serial.println(z);
  Serial.println(intheading);
  
  delay(500);
}
