#include "Wire.h"
#include "HMC5883L.h"
#include "math.h"

//int dir2 = 32; //правые (1 и 3)
//int pwm2 = 2;
//int dir3 = 34; //левые (2 и 4)
//int pwm3 = 7;

int dir3 = 4;//левые
int pwm3 = 5;
int dir2 = 2;//правые
int pwm2 = 3;

int V = 10; //скорость вращения двигателей
int need;
int IntHeading;

HMC5883L compass;

void setup()
{
  Serial.begin(9600);
  Wire.begin(); 
  compass = HMC5883L();  // создаем экземпляр HMC5883L библиотеки
  setupHMC5883L();       // инициализация HMC5883L
  
  pinMode(dir2,OUTPUT);
  pinMode(dir3,OUTPUT);
  
}

void loop()
{
  float heading = getHeading();
  IntHeading = heading;
  
  Serial.println(IntHeading);
  int dir = Serial.read();
  Direction(dir); // поиск заданного направления.
 
//  Serial.println("");
//  Serial.flush();
  delay(500);
  
  
    if(Serial.available())
    {
      int value = Serial.read();
      setCommand(value);
    }
}

void Direction(int dir)
{
   if(dir == '7') //поворот на 45 градсов влево.
  {
    need = HeadingCorrection(IntHeading - 45);
    V = 50;
   setCommand('L'); //крутиться влево.
  }
    if(dir == '9') //поворот на 45 градсов вправо.
  {
    need = HeadingCorrection(IntHeading + 45);
    V = 50;
    setCommand('R'); //крутиться вправо.
  }
    if(dir == '4') //поворот на 90 градсов влево.
  {
    need = HeadingCorrection(IntHeading - 90);
    V = 50;
    setCommand('L'); //крутиться влево.
  }
  
    if(dir == '6') //поворот на 90 градсов вправо.
  {
    need = HeadingCorrection(IntHeading + 90);
    V = 50;
    setCommand('R'); //крутиться вправо.
  }
  
      if(dir == '2') //поворот на 180 градсов влево.
  {
    need = HeadingCorrection(IntHeading - 180);
     V = 50;
    setCommand('L'); //крутиться влево.
  }
  
  if(checkAngle(IntHeading,need))
  {
    setCommand('5');
    need = 1000;
  //  V = 100;
  //  setCommand('F');
  }
}

boolean checkAngle(int IntHeading, int need)
{
  if(abs(IntHeading - need) <= 3)
  {
    return true;
  }
  else return false;
}

void setupHMC5883L(){
  // инициализация HMC5883L, и проверка наличия ошибок
  int error;  
  error = compass.SetScale(0.88); // чувствительность датчика из диапазона: 0.88, 1.3, 1.9, 2.5, 4.0, 4.7, 5.6, 8.1
  if(error != 0) Serial.println(compass.GetErrorText(error)); // если ошибка, то выводим ее
 
  error = compass.SetMeasurementMode(Measurement_Continuous); // установка режима измерений как Continuous (продолжительный)
  if(error != 0) Serial.println(compass.GetErrorText(error)); // если ошибка, то выводим ее
}
 
float getHeading(){
  // считываем данные с HMC5883L и рассчитываем  направление
  MagnetometerScaled scaled = compass.ReadScaledAxis(); // получаем масштабированные элементы с датчика
//  Serial.print("Y = ");
//  Serial.println(scaled.YAxis);
//  Serial.print("X = ");
//  Serial.println(scaled.XAxis);
  float heading = atan2(scaled.YAxis, scaled.XAxis);    // высчитываем направление
 
  // корректируем значения с учетом знаков
  if(heading < 0) heading += 2*PI;
  if(heading > 2*PI) heading -= 2*PI;
 
  return heading * RAD_TO_DEG; // переводим радианы в градусы
}

void setCommand(int value) // управление двигателями.
{
  if(value == '8') //вперед
  {
     analogWrite (pwm2, LOW);
     analogWrite (pwm3, LOW);       
     delay(10);        
     digitalWrite(dir2,HIGH);
     digitalWrite(dir3,HIGH);
     analogWrite (pwm2, V);
     analogWrite (pwm3, V);
   //  Serial.println("Forward");
  }

  if(value == 'B') //назад
  {
     analogWrite (pwm2, LOW);
     analogWrite (pwm3, LOW);
     delay(10);
     digitalWrite(dir2,LOW);
     digitalWrite(dir3,LOW);
     analogWrite (pwm2, V);
     analogWrite (pwm3, V);  
  //   Serial.println("Back");
  }

    if(value == 'R') //вправо 
  {
     analogWrite (pwm2, LOW);
     analogWrite (pwm3, LOW);        
     delay(10);
     digitalWrite(dir2,LOW);
     digitalWrite(dir3,HIGH);
     analogWrite (pwm2, V);
     analogWrite (pwm3, V);  
 //    Serial.println("Left");
  }

    if(value == 'L')//влево
  {
     analogWrite (pwm3, LOW);
     delay(10);
     digitalWrite(dir2,HIGH);
     digitalWrite(dir3,LOW); 
     analogWrite (pwm2, V);
     analogWrite (pwm3, V);
  //   Serial.println("Right");
  }

      if(value == '5')// стоп
  {
     analogWrite (pwm2, LOW);
     analogWrite (pwm3, LOW); 
  //   Serial.println("Stop");
  }
}

int HeadingCorrection(int input)
{
  if(input < 0) input += 2*PI;
  if(input > 2*PI) input -= 2*PI;
  return input;
}
