// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif
  

#define MPU6050_FIFO_OFLOW_INT      0x10
#define MPU6050_DATA_RDY_INT        0x02

#define MPU6050_FIFO_SIZE           1024


#define MPU6050_DLPF_MODE           0
#define MPU6050_SAMPLE_RATE_MODE    0

// uncomment "OUTPUT_READABLE_YAWPITCHROLL" if you want to see the yaw/
// pitch/roll angles (in degrees) calculated from the quaternions coming
// from the FIFO. Note this also requires gravity vector calculations.
// Also note that yaw/pitch/roll angles suffer from gimbal lock (for
// more info, see: http://en.wikipedia.org/wiki/Gimbal_lock)
// #define OUTPUT_READABLE_YAWPITCHROLL


// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
//MPU6050 mpu(0x69); // <-- use for AD0 high

#define INTERRUPT_PIN 2  // use pin 2 on Arduino Uno & most boards

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector


// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}



// ================================================================
// ===               CODE FOR PID CONTROL                       ===
// ================================================================

#include "DrivingModule.h"

#define START_TIME      10000

int LED = 13;

bool started = false;
char command;
double Setpoint;


unsigned long operationStartTime = 0;


// pre-defined functions
bool CheckStartTime();
void ProcessCommands(float yaw);
void MPU6050_Prepare();
bool MPU6050_CheckDataReady();
bool MPU6050_GetData();
void PrintYawPitchRoll();


// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

#define COM_PORT        Serial

void setup()
{
    // initialize serial communication
 //   Serial.begin(115200);
      COM_PORT.begin(115200);

    MPU6050_Prepare();
    InicializeDrivingModule();
    // Serial.println(F("Wait 10 seconds for MPU"));
    // delay(10000);

    pinMode(LED, OUTPUT);
    digitalWrite(LED, LOW);

    pinMode(DIR_RIGHT, OUTPUT);
    pinMode(DIR_LEFT, OUTPUT);
}




// ================================================================
// ===                    MAIN PROGRAM LOOP                     ===
// ================================================================

// long unsigned lastTime = 0;
float yaw;

void loop() {
    // if programming failed, don't try to do anything
    if (!dmpReady) {
        COM_PORT.println("dmpReady == false!");
        return;
    }

    // wait for MPU interrupt or extra packet(s) available
    while ( MPU6050_CheckDataReady() == false ) {
        // other program behavior stuff here
        // .
        // .
        // .
        // if you are really paranoid you can frequently test in between other
        // stuff to see if mpuInterrupt is true, and if so, "break;" from the
        // while() loop to immediately process the MPU data
        // .
        // .
        // .

        // TODO: temp
        // continue;

        yaw = degrees(ypr[0]); // привели к диапазону 0-360
       // COM_PORT.print("yaw = ");
       // COM_PORT.println(yaw);
      //  continue;
        if (CheckStartTime() == false)
        {
//            Serial.print("KAAAAAAAAAAAAAAAAAAARL!!!!!! millis() = ");
//            Serial.println(millis());
            setDirection('5',yaw);
            continue;
        }
        ProcessCommands(yaw);   
    }

    if (MPU6050_GetData() == true)
        PrintYawPitchRoll();
}




void ProcessCommands(float yaw) {    
    if (COM_PORT.available() > 0) {
        COM_PORT.println(" * * * * * * got command");
        command = COM_PORT.read();
        setDirection(command,yaw);
    }

    CheckFunction(yaw);
    
}

bool CheckStartTime() {
    if (started == false) {
        Setpoint = yaw;
    }

    // подождём первые START_TIME / 1000 секунд, чтобы бинс выставился
    if (millis() < START_TIME)
        return false;

    if (started == false) {
        digitalWrite(LED, HIGH);
        started = true;
    }

    return true;
}


void MPU6050_Prepare() {
    Wire.begin();
    Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties

    // initialize device
    COM_PORT.println(F("Initializing I2C devices..."));
    mpu.initialize();
    pinMode(INTERRUPT_PIN, INPUT);

    // меняем Sample Rate
    mpu.setDLPFMode(MPU6050_DLPF_MODE);
    uint8_t mode = mpu.getDLPFMode();
    COM_PORT.print(F("mode = "));
    COM_PORT.println(mode);

    mpu.setRate(MPU6050_SAMPLE_RATE_MODE);
    uint8_t rate = mpu.getRate();
    COM_PORT.print(F("rate = "));
    COM_PORT.println(rate);

    // verify connection
    COM_PORT.println(F("Testing device connections..."));
    COM_PORT.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

    // wait for ready
    COM_PORT.println(F("\nSend any character to begin DMP programming and demo: "));
    while (COM_PORT.available() && COM_PORT.read()); // empty buffer
    while (!COM_PORT.available());                 // wait for data
    while (COM_PORT.available() && COM_PORT.read()); // empty buffer again

    // load and configure the DMP
    COM_PORT.println(F("Initializing DMP..."));
    uint8_t devStatus = mpu.dmpInitialize();      // return status after each device operation (0 = success, !0 = error)

    // supply your own gyro offsets here, scaled for min sensitivity
    mpu.setXGyroOffset(201);
    mpu.setYGyroOffset(-9);
    mpu.setZGyroOffset(34);
    mpu.setZAccelOffset(1472); // 1688 factory default for my test chip

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        COM_PORT.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        COM_PORT.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
        // mpuIntStatus = mpu.getIntStatus();   // TODO: commented

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        COM_PORT.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();

        // запомним время начала вычислений
        operationStartTime = millis();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        COM_PORT.print(F("DMP Initialization failed (code "));
        COM_PORT.print(devStatus);
        COM_PORT.println(F(")"));
    }
}



bool MPU6050_CheckDataReady() {
    return (mpuInterrupt && mpu.dmpPacketAvailable());
}



bool MPU6050_GetData() {
    // reset interrupt flag and get INT_STATUS byte
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & MPU6050_FIFO_OFLOW_INT) || fifoCount == MPU6050_FIFO_SIZE) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        COM_PORT.println(F("FIFO overflow!"));
        return false;

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & MPU6050_DATA_RDY_INT) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) 
            fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;

        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);

        return true;
    }

    return false;
}



void PrintYawPitchRoll() {
#ifdef OUTPUT_READABLE_YAWPITCHROLL
    COM_PORT.print(millis() - operationStartTime);
    // display Euler angles in degrees
    COM_PORT.print(",\typr\t");
    COM_PORT.print(degrees(ypr[0]));
    COM_PORT.print("\t");
    COM_PORT.print(degrees(ypr[1]));
    COM_PORT.print("\t");
    COM_PORT.println(degrees(ypr[2]));
#endif
}
