#ifndef MPU6050_H
#define MPU6050_H 

#include <Arduino.h> // для типов

#include "MPU6050_Registers.h"

// Declaring an union for the registers and the axis values.
// The byte order does not match the byte order of 
// the compiler and AVR chip.
// The AVR chip (on the Arduino board) has the Low Byte 
// at the lower address.
// But the MPU-6050 has a different order: High Byte at
// lower address, so that has to be corrected.
// The register part "reg" is only used internally, 
// and are swapped in code.
typedef union accel_t_gyro_union {
  struct {
    uint8_t x_accel_h;
    uint8_t x_accel_l;
    uint8_t y_accel_h;
    uint8_t y_accel_l;
    uint8_t z_accel_h;
    uint8_t z_accel_l;
    uint8_t t_h;
    uint8_t t_l;
    uint8_t x_gyro_h;
    uint8_t x_gyro_l;
    uint8_t y_gyro_h;
    uint8_t y_gyro_l;
    uint8_t z_gyro_h;
    uint8_t z_gyro_l;
  } reg;

  struct {
    int x_accel;  // linear acceleration
    int y_accel;
    int z_accel;
    int temperature;
    int x_gyro;   // angular speed
    int y_gyro;
    int z_gyro;
  } value;
};



unsigned long get_last_time();
float get_last_x_angle();
float get_last_y_angle();
float get_last_z_angle();
float get_last_gyro_x_angle();
float get_last_gyro_y_angle();
float get_last_gyro_z_angle();



void set_last_read_angle_data(  unsigned long time, 
                                float x,
                                float y, 
                                float z, 
                                float x_gyro, 
                                float y_gyro, 
                                float z_gyro);



float    get_base_x_accel();
float    get_base_y_accel();
float    get_base_z_accel();
float    get_base_x_gyro();
float    get_base_y_gyro();
float    get_base_z_gyro();



int read_gyro_accel_vals(uint8_t* accel_t_gyro_ptr);
void calibrate_sensors();

int MPU6050_initialize();
void MPU6050_make_values();

#endif