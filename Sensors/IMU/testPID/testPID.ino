#include "MPU6050/MPU6050.h"
#include "pid_v1/pid_v1.h"

int dir3 = 45; //левые
int pwm3 = 9;
int dir2 = 44; //правые
int pwm2 = 10;

//Define Variables we'll be connecting to
double Setpoint, Input, Output;

//Specify the links and initial tuning parameters
double Kp = 3, Ki = 0.5, Kd = 87;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

// pre-defined function
void setCommand(char command, int V); // управление двигателями.



void setup()
{
    Input = 0;
    Setpoint = 0;

    myPID.SetOutputLimits(0, 255);

    //turn the PID on
    myPID.SetMode(AUTOMATIC);

    // инициализация БИНСа
    MPU6050_initialize();
}

void loop()
{
    // получение значений
    MPU6050_make_values();
    float angle_z = get_last_z_angle(); // Влево - увеличение. Врпаво - уменьшение.

    char command = Serial.read();
    switch (command) 
    {
        case '7':
            Setpoint = angle_z + 45;
        break;

        case '9':
            Setpoint = angle_z - 45;
        break;

        case '4':
            Setpoint = angle_z + 90;
        break;

        case '6':
            Setpoint = angle_z - 90;
        break;
    }

    Input = -abs(angle_z);
    myPID.Compute();

    int IntAngle_z = angle_z;
    int IntSetpoint = Setpoint;

    if(abs(IntAngle_z - IntSetpoint) <= 3)
    {
        setCommand('S', Output);
    } 
    else
    {
        // TODO: поворот отрабатывается в одну из сторон неправильно.
        // возможное место ошибки здесь
        char command = (IntAngle_z > IntSetpoint) ? 'R' : 'L';
        setCommand(command, Output);
    }
}



void setCommand(char command, int V) // управление двигателями.
{
    analogWrite (pwm2, LOW);
    analogWrite (pwm3, LOW); 

    if (command == 'S')
        return;

    delay(10);

    switch (command) 
    {
        case 'F': //вперед             
            digitalWrite(dir2,HIGH);
            digitalWrite(dir3,HIGH);
            // Serial.println("Forward");
        break;

        case 'B': //назад
            digitalWrite(dir2,LOW);
            digitalWrite(dir3,LOW); 
            // Serial.println("Back");
        break;

        // TODO: поворот отрабатывается в одну из сторон неправильно.
        // возможное место ошибки здесь
        case 'L': // влево
            digitalWrite(dir2,HIGH);
            digitalWrite(dir3,LOW); 
        //   Serial.println("Left");
        break;

        case 'R':// вправо
            digitalWrite(dir3,HIGH);
            digitalWrite(dir2,LOW);
        //   Serial.println("Right");
        break;
    } // end switch

    analogWrite (pwm2, V);
    analogWrite (pwm3, V);
}