#include "Sensor.h"

#include "debug.h"


Sensor::Sensor(const NewPingMega & sonar) 
	: NewPingMega(sonar), m_pingRecieved(false) {
}


void Sensor::CheckEcho() {
    if (NewPingMega::check_timer()) {
        m_pingRecieved = true;
    }  
}

// возвращает флаг на то, совершён ли запуск сенсора
boolean Sensor::activate_trigger() {
    m_pingRecieved = false;
    return NewPingMega::activate_trigger();
}


boolean Sensor::PingRecieved() const {
    return m_pingRecieved;
}


void Sensor::ClearPingRecieved() {
    m_pingRecieved = false;
}