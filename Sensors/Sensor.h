#ifndef SENSOR_H
#define SENSOR_H 

#include "NewPingMega.h"

class Sensor : public NewPingMega {
    public:
        Sensor(const NewPingMega & sonar);

        boolean activate_trigger(); // типо перекрыли
        void CheckEcho();
        boolean PingRecieved() const;

        void ClearPingRecieved();

    private:
        boolean m_pingRecieved;
};

#endif
