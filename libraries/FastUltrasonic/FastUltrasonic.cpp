#include "FastUltrasonic.h"

#include <DirectIO.h>
#include <EnableInterrupt.h>

#include "pins.h"

// ----------------------- ISR --------------------------------

// создадим прототипы ISR
#define _make_isr_prototype(idx) \
    void pingStartClock_##idx(); \
    void pingStopClock_##idx();


#if defined SENSORS_NUM
    _make_isr_prototype(0)

    #if SENSORS_NUM == 1

        VoidFunc pingStartClock[] = {
            pingStartClock_0
        };

        VoidFunc pingStopClock[] = {
            pingStopClock_0
        };

    #elif SENSORS_NUM == 2

        _make_isr_prototype(1)

        VoidFunc pingStartClock[] = {
            pingStartClock_0,
            pingStartClock_1
        };

        VoidFunc pingStopClock[] = {
            pingStopClock_0,
            pingStopClock_1
        };

    #elif SENSORS_NUM == 3

        _make_isr_prototype(1)
        _make_isr_prototype(2)

        VoidFunc pingStartClock[] = {
            pingStartClock_0,
            pingStartClock_1,
            pingStartClock_2
        };

        VoidFunc pingStopClock[] = {
            pingStopClock_0,
            pingStopClock_1,
            pingStopClock_2
        };

    #elif SENSORS_NUM == 4

        _make_isr_prototype(1)
        _make_isr_prototype(2)
        _make_isr_prototype(3)

        VoidFunc pingStartClock[] = {
            pingStartClock_0,
            pingStartClock_1,
            pingStartClock_2,
            pingStartClock_3
        };

        VoidFunc pingStopClock[] = {
            pingStopClock_0,
            pingStopClock_1,
            pingStopClock_2,
            pingStopClock_3
        };

    #endif

#endif // defined SENSORS_NUM




// ----------------------- SENSORS --------------------------------

// базовый класс сенсора, хранит общие ништяки
BaseSensor::BaseSensor(uint8_t _trigPin, uint8_t _echoPin, uint8_t _sensorId) : 
    trigPin(_trigPin), echoPin(_echoPin), sensorId(_sensorId)
{};

void BaseSensor::ClearData() {
    pulseStart = 0;
    pulseStop = 0;
    pingDistance = 0;
}

bool BaseSensor::CheckPing() {
    if (pulseBusy) {
        // Serial.print("pulseBusy, id = ");
        // Serial.println(sensorId);

        return false; // do not trigger if in the middle of a pulse
    }

    if (GetEcho() == HIGH) {
        // Serial.print("Echo == HIGH, id = ");
        // Serial.println(sensorId);
        
        return false; // do not trigger if ECHO pin is high
    }

    return true;
}

// каркас функции инициализации пинга, перегружается в потомках
void BaseSensor::pingTrig() {
    pulseBusy = true;
    pulseTimeout = false;
};

// will start the pulse clock on the rising edge of ECHO pin
void BaseSensor::pingStart() {
    // Serial.print("pingStart(), sensorId = ");
    // Serial.print(sensorId);
    // Serial.print(", echoPin = ");
    // Serial.print(echoPin);
    // Serial.println();

    enableInterrupt(echoPin, pingStartClock[sensorId], RISING);
};

// ECHO became HIGH, starting count
void BaseSensor::isrStartClock() {
    pulseStart = micros();

    disableInterrupt(echoPin); 
    enableInterrupt(echoPin, pingStopClock[sensorId], FALLING);

    // Serial.print("isrStartClock, sensorId = ");
    // Serial.println(sensorId);

    if (enableExternalTimeout != NULL) {
        enableExternalTimeout(); 
    } 
};

// ECHO became LOW, eval distance
void BaseSensor::isrStopClock() {
    pulseStop = micros();

    disableInterrupt(echoPin); 

    if (pulseTimeout == false) {
        pingDistance = pulseStop - pulseStart;
    } else {
        pingDistance = 0;
    }

    pulseBusy = false;

    // Serial.print("isrStopClock, sensorId = ");
    // Serial.println(sensorId);

    if (disableExternalTimeout != NULL) { 
        disableExternalTimeout(); 
    } 
};



void ClearSensorsData() {
    for (uint8_t i = 0; i < SENSORS_NUM; i++) {
        sensors[i]->ClearData();
    }
}



// Магия макросов для создания сенсоров
// сделано по причине шаблонных классов из библиотеки DirectIO.h

#define _create_sensor(idx) \
    struct _Sensor_##idx : public BaseSensor { \
    \
        _Sensor_##idx() : \
            BaseSensor(SENSOR_##idx##_TRIG, SENSOR_##idx##_ECHO, idx) \
        { \
            /*pTrig = LOW; pEcho = LOW;*/ \
        }; \
        \
        Output<SENSOR_##idx##_TRIG> pTrig; \
        Input<SENSOR_##idx##_ECHO> pEcho; \
        \
        bool GetEcho() { \
            return pEcho == HIGH; \
        }; \
        \
        void pingTrig() { \
            BaseSensor::pingTrig(); \
            \
            pTrig = LOW; \
            delayMicroseconds(4); \
            pTrig = HIGH; \
            \
            delayMicroseconds(10); \
            pTrig = LOW; \
        }; \
    \
    } Sensor_##idx 



// создаём сенсоры

#if defined SENSORS_NUM
    _create_sensor(0);

    #if SENSORS_NUM == 1

        BaseSensor  *sensors[] = { &Sensor_0 };

    #elif SENSORS_NUM == 2

        _create_sensor(1);

        BaseSensor  *sensors[] = { &Sensor_0, &Sensor_1 };

    #elif SENSORS_NUM == 3

        _create_sensor(1);
        _create_sensor(2);

        BaseSensor  *sensors[] = { &Sensor_0, &Sensor_1, &Sensor_2 };

    #elif SENSORS_NUM == 4

        _create_sensor(1);
        _create_sensor(2);
        _create_sensor(3);

        BaseSensor  *sensors[] = { &Sensor_0, &Sensor_1, &Sensor_2, &Sensor_3 };

    #endif

#endif // defined SENSORS_NUM




// Определим функции для прерываний

#define _make_isr_func(idx) \
    \
    /* Start clock on the rising edge of the ultrasonic pulse */ \
    void pingStartClock_##idx() { \
        sensors[idx]->isrStartClock(); \
    } \
    \
    /* Stop clock on the falling edge of the ultrasonic pulse */ \
    void pingStopClock_##idx() { \
        sensors[idx]->isrStopClock(); \
    }


#if defined SENSORS_NUM
    _make_isr_func(0)

    #if SENSORS_NUM > 1
        _make_isr_func(1)
    #endif

    #if SENSORS_NUM > 2
        _make_isr_func(2)
    #endif

    #if SENSORS_NUM > 3
        _make_isr_func(3)
    #endif

#endif // defined SENSORS_NUM