#ifndef FAST_ULTRASONIC_H
#define FAST_ULTRASONIC_H 

#include <Arduino.h>

#define US_ROUNDTRIP_CM 57      // Microseconds (uS) it takes sound to travel round-trip 1cm (2cm total), uses integer to save compiled code space. Default=57


typedef void (*VoidFunc) ();

// базовый класс сенсора, хранит общие ништяки
struct BaseSensor {
    uint8_t trigPin;
    uint8_t echoPin;

    uint8_t sensorId;

    volatile bool pulseBusy = false;
    volatile bool pulseTimeout = false;
    volatile unsigned long pulseStart = 0;
    volatile unsigned long pulseStop = 0;
    volatile unsigned long pingDistance = 0;

    VoidFunc enableExternalTimeout = NULL;
    VoidFunc disableExternalTimeout = NULL;

    BaseSensor(uint8_t _trigPin, uint8_t _echoPin, uint8_t _sensorId);

    virtual bool GetEcho() = 0;

    void ClearData();

    bool CheckPing();

    // каркас функции инициализации пинга, перегружается в потомках
    virtual void pingTrig();

    // will start the pulse clock on the rising edge of ECHO pin
    void pingStart();

    // ECHO became HIGH, starting count
    void isrStartClock();

    // ECHO became LOW, eval distance
    void isrStopClock();
};

extern BaseSensor  *sensors[];

// очищаем данные массива выше
void ClearSensorsData();

#endif