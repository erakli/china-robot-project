#ifndef NEW_PING_MEGA_H
#define NEW_PING_MEGA_H 

#include <Arduino.h>

#include <avr/io.h>
#include <avr/interrupt.h>

// Shouldn't need to change these values unless you have a specific need to do so.
#define MAX_SENSOR_DISTANCE 500 // Maximum sensor distance can be as high as 500cm, no reason to wait for ping longer than sound takes to travel this distance and back. Default=500
#define US_ROUNDTRIP_CM 57      // Microseconds (uS) it takes sound to travel round-trip 1cm (2cm total), uses integer to save compiled code space. Default=57
#define ONE_PIN_ENABLED true    // Set to "false" to disable one pin mode which saves around 14-26 bytes of binary size. Default=true
#define ROUNDING_ENABLED false  // Set to "true" to enable distance rounding which also adds 64 bytes to binary size. Default=false

// Probably shouldn't change these values unless you really know what you're doing.
#define NO_ECHO 0               // Value returned if there's no ping echo within the specified MAX_SENSOR_DISTANCE or max_cm_distance. Default=0
#define MAX_SENSOR_DELAY 5800   // Maximum uS it takes for sensor to start the ping. Default=5800
#define ECHO_TIMER_FREQ 24      // Frequency to check for a ping echo (every 24uS is about 0.4cm accuracy). Default=24
#define PING_OVERHEAD 5         // Ping overhead in microseconds (uS). Default=5
#define PING_TIMER_OVERHEAD 13  // Ping timer overhead in microseconds (uS). Default=13

// Conversion from uS to distance (round result to nearest cm or inch).
#define NewPingConvert(echoTime, conversionFactor) (max(((unsigned int)echoTime + conversionFactor / 2) / conversionFactor, (echoTime ? 1 : 0)))


enum Timers {
	Timer0,
	Timer1,
	Timer2,
	Timer3,
	Timer4,
	Timer5
};

// signalize about stage of waiting ping start
enum StartPingResults {
	StartPingResult_Ok,
	StartPingResult_Pending,
	StartPingResult_Timeout
};


class NewPingMega {
	public:
		NewPingMega(uint8_t trigger_pin, uint8_t echo_pin, uint16_t max_cm_distance = MAX_SENSOR_DISTANCE);

		uint16_t ping(uint16_t max_cm_distance = 0);
		uint32_t ping_cm(uint16_t max_cm_distance = 0);

		static uint16_t convert_cm(uint16_t echoTime);

	// timer stuff
		uint32_t ping_timer(void(*userFunc)(void), boolean countElapsedTime = false, uint16_t max_cm_distance = 0);
		boolean check_timer();

		uint16_t ping_result;

		void timer_us(uint16_t frequency, void(*userFunc)(void));
		void timer_stop();

	// my realization
	public:
		uint32_t pingStartTime; // by ping start time we can get delta between 2 sensors echoes
		uint32_t pingInitDuration;

		void assign_timer(Timers timer_num);

	// functions for separete control of ping process
		boolean activate_trigger(); // returns false if there is pending echo
		void init_wait_ping();
		StartPingResults check_ping_start();	// returns status of ping start status
		uint32_t wait_ping_start(); // returns ping start time
		void start_ping_timer(void(*userFunc)(void));

	protected:
		boolean ping_trigger(boolean countElapsedTime = false);
		void set_max_distance(uint16_t max_cm_distance);

	// timer
		boolean ping_trigger_timer(uint16_t trigger_delay);
		void timer_setup();

	// use port registers or timer interrupts as required.
		uint8_t _triggerBit;
		uint8_t _echoBit;
		volatile uint8_t *_triggerOutput;
		volatile uint8_t *_echoInput;
		volatile uint8_t *_triggerMode;

		uint16_t _maxEchoTime;
		uint32_t _max_time;

	// my improvement
		Timers _timerNum; // select source timer
};

#endif