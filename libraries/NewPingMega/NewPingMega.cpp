#include "NewPingMega.h"

// ---------------------------------------------------------------------------
// NewPingMega constructor
// ---------------------------------------------------------------------------

NewPingMega::NewPingMega(uint8_t trigger_pin, uint8_t echo_pin, uint16_t max_cm_distance) {
    _triggerBit = digitalPinToBitMask(trigger_pin); // Get the port register bitmask for the trigger pin.
    _echoBit = digitalPinToBitMask(echo_pin);       // Get the port register bitmask for the echo pin.

    _triggerOutput = portOutputRegister(digitalPinToPort(trigger_pin)); // Get the output port register for the trigger pin.
    _echoInput = portInputRegister(digitalPinToPort(echo_pin));         // Get the input port register for the echo pin.

    _triggerMode = (uint8_t *) portModeRegister(digitalPinToPort(trigger_pin)); // Get the port mode register for the trigger pin.

    set_max_distance(max_cm_distance); // Call function to set the max sensor distance.

#if ONE_PIN_ENABLED != true
    *_triggerMode |= _triggerBit; // Set trigger pin to output.
#endif

    pingStartTime = 0;
    pingInitDuration = 0;
    _timerNum = Timer2; // Default timer is Timer2
}


// ---------------------------------------------------------------------------
// Standard ping methods
// ---------------------------------------------------------------------------

uint16_t NewPingMega::ping(uint16_t max_cm_distance) {
    if (max_cm_distance > 0) 
        set_max_distance(max_cm_distance); // Call function to set a new max sensor distance.

    if (!ping_trigger()) 
        return NO_ECHO; // Trigger a ping, if it returns false, return NO_ECHO to the calling function.

    // while (digitalRead() == HIGH)
    while (*_echoInput & _echoBit)                // Wait for the ping echo.
        if (micros() > _max_time) 
            return NO_ECHO; // Stop the loop and return NO_ECHO (false) if we're beyond the set maximum distance.

    return (micros() - (_max_time - _maxEchoTime) - PING_OVERHEAD); // Calculate ping time, include overhead.
}


uint32_t NewPingMega::ping_cm(uint16_t max_cm_distance) {
    uint32_t echoTime = NewPingMega::ping(max_cm_distance); // Calls the ping method and returns with the ping echo distance in uS.
#if ROUNDING_ENABLED == false
    return (echoTime / US_ROUNDTRIP_CM);              // Call the ping method and returns the distance in centimeters (no rounding).
#else
    return NewPingConvert(echoTime, US_ROUNDTRIP_CM); // Convert uS to centimeters.
#endif
}



// ---------------------------------------------------------------------------
// Standard and timer interrupt ping method support functions (not called directly)
// ---------------------------------------------------------------------------

uint32_t pingWaitMaxTime;
uint32_t waitStartTime; // save time when we start to wait ping

boolean NewPingMega::ping_trigger(boolean countElapsedTime) {
    pingStartTime = 0;

#if ONE_PIN_ENABLED == true
    *_triggerMode |= _triggerBit;  // Set trigger pin to output.
#endif

    *_triggerOutput &= ~_triggerBit;   // Set the trigger pin low, should already be low, but this will make sure it is.
    delayMicroseconds(4);              // Wait for pin to go low.
    *_triggerOutput |= _triggerBit;    // Set trigger pin high, this tells the sensor to send out a ping.
    delayMicroseconds(10);             // Wait long enough for the sensor to realize the trigger pin is high. Sensor specs say to wait 10uS.
    *_triggerOutput &= ~_triggerBit;   // Set trigger pin back to low.

#if ONE_PIN_ENABLED == true
    *_triggerMode &= ~_triggerBit; // Set trigger pin to input (when using one Arduino pin, this is technically setting the echo pin to input as both are tied to the same Arduino pin).
#endif

    // if (digitalRead() == HIGH)
    if (*_echoInput & _echoBit) 
        return false;               // Previous ping hasn't finished, abort.

    // Wait for ping to start.
    pingWaitMaxTime = micros() + _maxEchoTime + MAX_SENSOR_DELAY; // Maximum time we'll wait for ping to start (most sensors are <450uS, the SRF06 can take up to 34,300uS!)

    // store elapsed time on ping start
    // if we got timeout, then elapsedTime would be equal 0
    if (countElapsedTime == true) {
        pingInitDuration = 0;
        waitStartTime = micros();
    }

    // while (digitalRead() == LOW) 
    while (!(*_echoInput & _echoBit)) {
        if (micros() > pingWaitMaxTime)
            return false;             // Took too long to start, abort.
    }

    pingStartTime = micros();

    if (countElapsedTime == true) 
        pingInitDuration = pingStartTime - waitStartTime;

    _max_time = micros() + _maxEchoTime; // Ping started, set the time-out.
    return true;                         // Ping started successfully.
}


void NewPingMega::set_max_distance(uint16_t max_cm_distance) {
#if ROUNDING_ENABLED == false
    _maxEchoTime = min(max_cm_distance + 1, (uint16_t)MAX_SENSOR_DISTANCE + 1) * US_ROUNDTRIP_CM; // Calculate the maximum distance in uS (no rounding).
#else
    _maxEchoTime = min(max_cm_distance, (uint16_t)MAX_SENSOR_DISTANCE) * US_ROUNDTRIP_CM + (US_ROUNDTRIP_CM / 2); // Calculate the maximum distance in uS.
#endif
}



// ---------------------------------------------------------------------------
// functions for separete control of ping process (based on ping_timer())
// ---------------------------------------------------------------------------

boolean NewPingMega::activate_trigger() {
    // if (digitalRead() == HIGH)
    if (*_echoInput & _echoBit) 
        return false;               // Previous ping hasn't finished, abort.

#if ONE_PIN_ENABLED == true
    *_triggerMode |= _triggerBit;  // Set trigger pin to output.
#endif

    *_triggerOutput &= ~_triggerBit;   // Set the trigger pin low, should already be low, but this will make sure it is.
    delayMicroseconds(4);              // Wait for pin to go low.
    *_triggerOutput |= _triggerBit;    // Set trigger pin high, this tells the sensor to send out a ping.
    delayMicroseconds(10);             // Wait long enough for the sensor to realize the trigger pin is high. Sensor specs say to wait 10uS.
    *_triggerOutput &= ~_triggerBit;   // Set trigger pin back to low.

#if ONE_PIN_ENABLED == true
    *_triggerMode &= ~_triggerBit; // Set trigger pin to input (when using one Arduino pin, this is technically setting the echo pin to input as both are tied to the same Arduino pin).
#endif

    return true;
}



void NewPingMega::init_wait_ping() {
    pingStartTime = 0;
    // Maximum time we'll wait for ping to start (most sensors are <450uS, the SRF06 can take up to 34,300uS!)
    pingWaitMaxTime = micros() + _maxEchoTime + MAX_SENSOR_DELAY; 
}


// function for controlling proccess of ping's start outside the class (bad idea, but needed)
StartPingResults NewPingMega::check_ping_start() {
    if (*_echoInput & _echoBit) {
        pingStartTime = micros();
        _max_time = pingStartTime + _maxEchoTime; // Ping started, set the time-out.
        return StartPingResult_Ok;    // ping started

    } else if (micros() > pingWaitMaxTime) {
        return StartPingResult_Timeout; // wait too long
    }

    return StartPingResult_Pending; // still waiting
}



uint32_t NewPingMega::wait_ping_start() {
    init_wait_ping();

    // while (digitalRead() == LOW) 
    while (!(*_echoInput & _echoBit)) {
        if (micros() > pingWaitMaxTime)
            return 0;             // Took too long to start, abort.
    }

    pingStartTime = micros();
    _max_time = pingStartTime + _maxEchoTime; // Ping started, set the time-out.
    return pingStartTime;         // Ping started successfully.
}



void NewPingMega::start_ping_timer(void(*userFunc)(void)) {
    timer_us(ECHO_TIMER_FREQ, userFunc); // Set ping echo timer check every ECHO_TIMER_FREQ uS.
}


// ---------------------------------------------------------------------------
// Timer interrupt ping methods (won't work with non-AVR, ATmega128 and all ATtiny microcontrollers)
// ---------------------------------------------------------------------------

uint32_t NewPingMega::ping_timer(void(*userFunc)(void), boolean countElapsedTime, uint16_t max_cm_distance) {
    if (max_cm_distance > 0) 
        set_max_distance(max_cm_distance); // Call function to set a new max sensor distance.

    // Trigger a ping, if it returns false, return without starting the echo timer.
    if (!ping_trigger(countElapsedTime)) 
        return 0;    

    timer_us(ECHO_TIMER_FREQ, userFunc); // Set ping echo timer check every ECHO_TIMER_FREQ uS.

    return pingInitDuration;
}


boolean NewPingMega::check_timer() {
    if (micros() > _max_time) { // Outside the time-out limit.
        timer_stop();           // Disable timer interrupt
        return false;           // Cancel ping timer.
    }

    // if (digitalRead() == LOW)
    if (!(*_echoInput & _echoBit)) { // Ping echo received.
        timer_stop();                // Disable timer interrupt
        ping_result = (micros() - (_max_time - _maxEchoTime) - PING_TIMER_OVERHEAD); // Calculate ping time including overhead.
        return true;                 // Return ping echo true.
    }

    return false; // Return false because there's no ping echo yet.
}


// ---------------------------------------------------------------------------
// Timer interrupt methods (can be used for non-ultrasonic needs)
// ---------------------------------------------------------------------------

// Variables used for timer functions
// separate functions for every timer
void(*intFunc1)();
void(*intFunc2)();
#if defined (__AVR_ATmega2560__)
void(*intFunc3)();
void(*intFunc4)();
void(*intFunc5)();
#endif


void NewPingMega::timer_us(uint16_t frequency, void(*userFunc)(void)) {
    timer_setup();      // Configure the timer interrupt.

    switch (_timerNum) {
        case Timer1:
            intFunc1 = userFunc; 
            OCR1A = min((frequency >> 2) - 1, 255);
            TIMSK1 = bit(OCIE1A);
        break;

        case Timer2:
            intFunc2 = userFunc;      // User's function to call when there's a timer event.
            OCR2A = min((frequency >> 2) - 1, 255); // Every count is 4uS, so divide by 4 (bitwise shift right 2) subtract one, then make sure we don't go over 255 limit.
            TIMSK2 |= bit(OCIE2A);    // Enable Timer interrupt.
        break;

#if defined (__AVR_ATmega2560__)
        case Timer3:
            intFunc3 = userFunc; 
            OCR3A = min((frequency >> 2) - 1, 255);
            TIMSK3 = bit(OCIE3A);
        break;

        case Timer4:
            intFunc4 = userFunc; 
            OCR4A = min((frequency >> 2) - 1, 255);
            TIMSK4 = bit(OCIE4A);
        break;

        case Timer5:
            intFunc5 = userFunc; 
            OCR5A = min((frequency >> 2) - 1, 255);
            TIMSK5 = bit(OCIE5A);
        break;
#endif

        default:
            ;
    }
}


void NewPingMega::timer_stop() { 
    // Disable timer interrupt. Write LOW in OCIEnA bit
    switch (_timerNum) {
        case Timer1:
            TIMSK1 &= ~bit(OCIE1A);
        break;

        case Timer2:
            TIMSK2 &= ~bit(OCIE2A);
        break;

#if defined (__AVR_ATmega2560__)
        case Timer3:
            TIMSK3 &= ~bit(OCIE3A);
        break;

        case Timer4:
            TIMSK4 &= ~bit(OCIE4A);
        break;

        case Timer5:
            TIMSK5 &= ~bit(OCIE5A);
        break;
#endif

        default:
            ;
    }
}


// ---------------------------------------------------------------------------
// My improvement on various Timer support
// ---------------------------------------------------------------------------


void NewPingMega::assign_timer(Timers timer_num) {
    _timerNum = timer_num;
}



// ---------------------------------------------------------------------------
// Timer interrupt method support functions (not called directly)
// ---------------------------------------------------------------------------

void NewPingMega::timer_setup() {
    timer_stop();        // Disable Timer interrupt.

    // Set Timer to CTC mode.
    // Reset Timer counter.

    ASSR &= ~bit(AS2);   // Set clock, not pin.

    switch (_timerNum) {
        case Timer1:
            TCCR1B = bit(WGM12);
            TCCR1B |= bit(CS10);    // CLK (no prescaling    )
            // TCCR1B |= bit(CS11);   // CLK/8
            TCNT1 = 0;
        break;

        case Timer2:
            TCCR2A = bit(WGM21); 
            TCCR2B = bit(CS22);  // Set Timer prescaler to 64 (4uS/count, 4uS-1020uS range).
            TCNT2 = 0;  
        break;

#if defined (__AVR_ATmega2560__)
        case Timer3:
            TCCR3B = bit(WGM32);
            TCCR3B |= bit(CS30);    // CLK (no prescaling)
            TCNT3 = 0;
        break;

        case Timer4:
            TCCR4B = bit(WGM42);
            TCCR4B |= bit(CS40);    // CLK (no prescaling)
            TCNT4 = 0;
        break;

        case Timer5:
            TCCR5B = bit(WGM52);
            TCCR5B |= bit(CS50);    // CLK (no prescaling)
            TCNT5 = 0;
        break;
#endif

        default:
            ;
    }         
}


 // Call wrapped function.

ISR(TIMER1_COMPA_vect) {    
    intFunc1();
}

ISR(TIMER2_COMPA_vect) {
    intFunc2();
}

#if defined (__AVR_ATmega2560__)
ISR(TIMER3_COMPA_vect) {
    intFunc3();
}

ISR(TIMER4_COMPA_vect) {
    intFunc4();
}

ISR(TIMER5_COMPA_vect) {
    intFunc5();
}
#endif

// ---------------------------------------------------------------------------
// Conversion methods (rounds result to nearest cm or inch).
// ---------------------------------------------------------------------------

uint16_t NewPingMega::convert_cm(uint16_t echoTime) {
#if ROUNDING_ENABLED == false
    return (echoTime / US_ROUNDTRIP_CM);              // Convert uS to centimeters (no rounding).
#else
    return NewPingConvert(echoTime, US_ROUNDTRIP_CM); // Convert uS to centimeters.
#endif
}
