// NewPingMegaTest2

#include "NewPingMega.h"

#define SONAR_NUM     3 // Number of sensors.
#define MAX_DISTANCE 300 // Maximum distance (in cm) to ping.

uint16_t pingSpeed = 250; // How frequently are we going to send out a ping (in milliseconds). 50ms would be 20 times a second.
uint32_t pingTimer;     // Holds the next ping time.

uint16_t cm[SONAR_NUM];         // Where the ping distances are stored.
// uint32_t elapsedTime[SONAR_NUM]; 
uint32_t startTimes[SONAR_NUM]; 

NewPingMega sonar[SONAR_NUM] = {     // Sensor object array.
    NewPingMega(12, 13, MAX_DISTANCE), // Each sensor's trigger pin, echo pin, and max distance to ping.
    NewPingMega(43, 45, MAX_DISTANCE),
    NewPingMega(49, 51, MAX_DISTANCE)
};

void setup() {
    sonar[0].assign_timer(Timer1);
    sonar[1].assign_timer(Timer3);
    sonar[2].assign_timer(Timer4);

    Serial.begin(115200);
    pingTimer = millis(); // Start now.
}

void loop() {
    // Notice how there's no delays in this sketch to allow you to do other processing in-line while doing distance pings.
    if (millis() >= pingTimer) {   // pingSpeed milliseconds since last ping, do another ping.
        oneSensorCycle();
        pingTimer += pingSpeed;      // Set the next ping time.
        cm[0] = 0;                      // Make distance zero in case there's no ping echo for this sensor.
        cm[1] = 0;
        cm[2] = 0;

        // only if active sensor activated we start cycle
        // if (sonar[0].activate_trigger() == true) {
            sonar[0].activate_trigger();
            sonar[1].activate_trigger();
            sonar[2].activate_trigger();

            startTimes[0] = sonar[0].wait_ping_start();
            startTimes[1] = sonar[1].wait_ping_start();
            startTimes[2] = sonar[2].wait_ping_start();

            sonar[0].start_ping_timer(echoCheck0);
            sonar[1].start_ping_timer(echoCheck1);
            sonar[2].start_ping_timer(echoCheck2);
        // }

        // elapsedTime[0] = sonar[0].ping_timer(echoCheck0, true); // Send out the ping, calls "echoCheck" function every 24uS where you can check the ping status.
        // elapsedTime[1] = sonar[1].ping_timer(echoCheck1, true); // won't wait for ping
        // elapsedTime[2] = sonar[2].ping_timer(echoCheck2, true); // won't wait for ping
    }
    // Do other stuff here, really. Think of it as multi-tasking.
}

void echoCheck0() { // If ping received, set the sensor distance to array.
    if (sonar[0].check_timer()) {
        cm[0] = sonar[0].ping_result;
    }
}

void echoCheck1() { // If ping received, set the sensor distance to array.
    if (sonar[1].check_timer()) {
        // we started second, so add time elapsed on first sensor to start
        cm[1] = sonar[1].ping_result;
    }
}

void echoCheck2() { // If ping received, set the sensor distance to array.
    if (sonar[2].check_timer()) {
        // we started second, so add time elapsed on first sensor to start
        cm[2] = sonar[2].ping_result;
    }
}



void oneSensorCycle() { // Sensor ping cycle complete, do something with the results.
    // The following code would be replaced with your code that does something with the ping results.
    for (uint8_t i = 0; i < SONAR_NUM; i++) {
        Serial.print(i);
        Serial.print("=");
        Serial.print(cm[i] / US_ROUNDTRIP_CM);
        Serial.print("cm (");
        // Serial.print(elapsedTime[i]);
        // Serial.print(", ");
        // if ((cm[i] / US_ROUNDTRIP_CM) > 0) {
            // if it's not a first sensor, then we started later, take it in account
                Serial.print("delta = ");
                Serial.print(startTimes[i] - startTimes[0]);
                Serial.print(", ");
                Serial.print(
                    (cm[i] + (startTimes[i] - startTimes[0])) / 
                    US_ROUNDTRIP_CM
                );

            // switch (i) {
            //     case 1 :
            //         Serial.print(
            //             (cm[i] + elapsedTime[0] + elapsedTime[1]) / 
            //             US_ROUNDTRIP_CM
            //         );
            //     break;

            //     case 2 :
            //         Serial.print(
            //             (cm[i] + elapsedTime[0] + elapsedTime[1] + elapsedTime[2]) /
            //             US_ROUNDTRIP_CM
            //         );
            //     break;
            // }
        // } else {
            // Serial.print(0);
        // }
        Serial.print("cm) ");
    }
    Serial.println();
}
