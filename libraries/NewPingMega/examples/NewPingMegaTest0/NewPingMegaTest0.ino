#include "NewPingMega.h"

#define TRIGGER_PIN  49  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     51  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 300 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPingMega sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

void setup() {
    sonar.assign_timer(Timer4);
  
    Serial.begin(115200);
}

void loop() {
    delay(50);                     // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
    Serial.print("Ping: ");
    Serial.print(sonar.ping_cm()); // Send ping, get distance in cm and print result (0 = outside set distance range)
    Serial.println("cm");
}
