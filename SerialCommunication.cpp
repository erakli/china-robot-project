#include "SerialCommunication.h"

#include "debug.h"

void openPorts(unsigned long baudrate) {
    Serial.begin(baudrate);
#ifdef ENABLE_BLUETOOTH 
    BLUETOOTH_PORT.begin(baudrate);
#endif

#ifdef DEBUG
    DPRINTLN("Serial initialized");
#endif
}
