﻿#ifndef PINS_H
#define PINS_H 

#define SENSORS_NUM     4
#define STATE_VECTOR_SIZE		SENSORS_NUM * SENSORS_NUM

// настоящие пины
#define SENSOR_0_TRIG   30
#define SENSOR_0_ECHO   31

#define SENSOR_1_TRIG   32
#define SENSOR_1_ECHO   33

#define SENSOR_2_TRIG   34
#define SENSOR_2_ECHO   35

#define SENSOR_3_TRIG   50
#define SENSOR_3_ECHO   51


#define RELAY_0         38
#define RELAY_1         39
#define RELAY_2         40
#define RELAY_3         41


#define PWM_RIGHT       9   // (1; 3)
#define DIR_RIGHT       29
#define PWM_LEFT        10  // (2; 4)
#define DIR_LEFT        28


/*
    Arduino Mega2560/ADK

    Interrupt Type | Pins
    -------------- | --------------
    External       | 2 3 and 18-21
*/

#define MPU_INTERRUPT_PIN 18

#endif